import os
from datetime import datetime, timedelta

from airflow import DAG

from additional.helpers.ticketek.crawl_events import PullEvents
from additional.tools.browser_config_model import *

from additional.operators.start_operator import StartOperator
from additional.operators.create_new_cycle_postgres_operator \
    import CreateNewCyclePostgresOperator
from additional.operators.export_to_csv_operator \
    import ExportAndUpload
from additional.operators.anti_distill_operator \
    import AntiDistillInstall


B_CONFIGS = ConfigList(
    BrowserConfig(
        0, 90,
        None,
        ScreenConfig(
            ScreenSizeConfig(1920, 1080),
            24
        ),
        'en,en_US', True,
        ['http://lumtest.com/myip.json', 'https://coveryourtracks.eff.org/']
    )
)
# Default settings applied to all tasks
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'provide_context': True,
    'execution_timeout': None
}

# Using a DAG context manager, you don't have to specify the dag property of each task
with DAG('crawlers.ticketek.au',
         start_date=datetime(2020, 6, 1),
         max_active_runs=1,
         concurrency=64,
         schedule_interval=None,
         default_args=default_args,
         catchup=False  # enable if you don't want historical dag runs to run
         ) as dag:

    oStart = StartOperator(
        task_id='start',
        config_module_path='additional.helpers.axs.axs_config'
    )

    # step 1 - start new cycle, init tables
    operator_0_init_tables = CreateNewCyclePostgresOperator(
        task_id='create_cycle',
        is_test=False,
        is_run_last_cycle=False,
        manual_cycle_id=None,
        postgres_conn_id='crawlers_debug_postgres',
        step_tables={
            'ticketek_au': {
                'schema':
                    f'id bigserial not null constraint ticketek_au_pk primary key, '
                    f'cycle_id integer, '
                    f'data jsonb, '
                    f'created_at timestamp default now()',
                'insert_schema': [
                    'cycle_id', 'data'
                ],
                'indices': ['cycle_id', ]}
        },
        get_last_cycle_id_query=f'SELECT cycle_id from ticketek_au order by created_at desc limit 1',
        get_max_cycle_id_query=f'SELECT MAX(cycle_id) from ticketek_au',
    )

    operator_0_1_install_distill = AntiDistillInstall(
        task_id='prepare_browser',
        browser_configs=B_CONFIGS,
        prefix='ticketek',
        rewrite=False
    )

    operator_1_pull_events = PullEvents(
        task_id="pull_events",
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
        browser_config=B_CONFIGS.get_by_id(0),
        is_test=False,
        delay_between_pages=1,
        prefix='ticketek',
        user_data_dir=os.path.join(os.getcwd(), 'dags', 'additional', 'chrome_user_data', 'ticketek'),
        pplugin='p_axs_0.zip',
        no_images=True,
        clear_user_data=True
    )

    operator_2_export_and_upload = ExportAndUpload(
        task_id="export_to_csv",
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
        table_name='ticketek_au',
        filename_template='ticketek_au',
        records_threshold=100
    )
    oStart >> operator_0_init_tables >> operator_0_1_install_distill \
           >> operator_1_pull_events >> operator_2_export_and_upload
