from datetime import datetime, timedelta

from airflow import DAG

from additional.helpers.eventbrite_events.eventbrite_events_config import *
from additional.helpers.eventbrite_events.crawl_events import PullEvents

from additional.operators.start_operator \
    import StartOperator
from additional.operators.final_stats_operator \
    import LogStatOperator
from additional.operators.create_new_cycle_postgres_operator \
    import CreateNewCyclePostgresOperator
from additional.operators.dedup_data_operator \
    import DedupDataOperator
from additional.operators.export_to_csv_operator \
    import ExportAndUpload

# Default settings applied to all tasks
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'provide_context': True,
    'execution_timeout': None
}

with DAG('crawlers.eventbrite.main',
         start_date=datetime(2020, 6, 1),
         max_active_runs=1,
         concurrency=64,
         schedule_interval='0 9,19 * * *',
         default_args=default_args,
         catchup=False
         ) as dag:

    oStart = StartOperator(
        task_id='start',
        config_module_path='additional.helpers.eventbrite_events.eventbrite_events_config'
    )

    oEnd = LogStatOperator(
        task_id='end',
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
        tables=[EVENTS_RAW_TABLE, EVENTS_FINAL_TABLE, ]
    )

    # step 1 - start new cycle, init tables
    operator_0_init_tables = CreateNewCyclePostgresOperator(
        task_id='create_cycle',
        is_test=False,
        is_run_last_cycle=False,
        manual_cycle_id=MANUAL_CYCLE_ID,
        postgres_conn_id='crawlers_debug_postgres',
        step_tables={
            EVENTS_RAW_TABLE: {
                'schema':
                    f'id bigserial not null constraint {EVENTS_RAW_TABLE}_pk primary key, '
                    f'cycle_id integer, '
                    f'event_id varchar(100), '
                    f'data jsonb, '
                    f'created_at timestamp default now()',
                'insert_schema': [
                    'cycle_id', 'data', 'event_id'
                ],
                'indices': ['cycle_id', 'event_id']},
            EVENTS_FINAL_TABLE: {
                'schema':
                    f'id bigserial not null constraint {EVENTS_FINAL_TABLE}_pk primary key, '
                    f'cycle_id integer, '
                    f'event_id varchar(100), '
                    f'data jsonb, '
                    f'created_at timestamp default now()',
                'insert_schema': [
                    'cycle_id', 'data', 'event_id'
                ],
                'indices': ['cycle_id', 'event_id']},
        },
        get_last_cycle_id_query=f'SELECT cycle_id from {EVENTS_RAW_TABLE} order by created_at desc limit 1',
        get_max_cycle_id_query=f'SELECT MAX(cycle_id) from {EVENTS_RAW_TABLE}',
    )

    operator_1_pull_events = PullEvents(
        task_id="pull_events",
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}"
    )

    operator_2_dedup = DedupDataOperator(
        task_id="dedup_events",
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
        from_table=EVENTS_RAW_TABLE,
        into_table=EVENTS_FINAL_TABLE,
        insert_schema=('cycle_id', 'data', 'event_id'),
        group_by=('cycle_id', 'event_id'),
        trigger_rule='all_done'
    )
    oStart >> operator_0_init_tables >> operator_1_pull_events >> operator_2_dedup

    if EXPORT_TO_CSV:
        operator_3_export_and_upload = ExportAndUpload(
            task_id="export_to_csv",
            cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
            table_name=EVENTS_FINAL_TABLE,
            filename_template='eventbrite',
            records_threshold=EXPORT_RECORDS_THRESHOLD
        )
        operator_2_dedup >> operator_3_export_and_upload >> oEnd
    else:
        operator_2_dedup >> oEnd
