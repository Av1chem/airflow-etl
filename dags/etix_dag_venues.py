from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator

from additional.operators.start_operator import StartOperator

from additional.helpers.etix_venues.etix_config import *
from additional.helpers.etix_venues.make_configs import MakeConfigs
from additional.helpers.etix_venues.pull_searches import PullSearches
from additional.helpers.etix_venues.merge_venues import MergeVenues
from additional.helpers.etix_venues.pull_events import PullEvents
from additional.helpers.etix_venues.merge_events import MergeEvents

# Default settings applied to all tasks
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'retries': RETRIES,
    'retry_delay': timedelta(minutes=1),
    'provide_context': True,
    'execution_timeout': None
}


def pull_venues(**kwargs):
    if PULL_VENUES:
        return 'make_configs'
    else:
        return 'skip_venues'


def pull_events(**kwargs):
    if PULL_EVENTS:
        return 'before_events'
    else:
        return 'skip_events'


# set task colors
class DummyOperatorVenuesBlock(DummyOperator):
    pass


class BranchOperatorVenuesBlock(BranchPythonOperator):
    pass


class DummyOperatorEventsBlock(DummyOperator):
    pass


class BranchOperatorEventsBlock(BranchPythonOperator):
    pass


DEFAULT_COLOR = '#ededed'

DummyOperator.ui_color = DEFAULT_COLOR
StartOperator.ui_color = DEFAULT_COLOR

VENUES_BLOCK_COLOR_LIGHT = '#fff6f5'
VENUES_BLOCK_COLOR_INTENSE = '#f2d1ce'

MakeConfigs.ui_color = VENUES_BLOCK_COLOR_INTENSE
PullSearches.ui_color = VENUES_BLOCK_COLOR_INTENSE
MergeVenues.ui_color = VENUES_BLOCK_COLOR_LIGHT
BranchOperatorVenuesBlock.ui_color = VENUES_BLOCK_COLOR_LIGHT
DummyOperatorVenuesBlock.ui_color = VENUES_BLOCK_COLOR_LIGHT

EVENTS_BLOCK_COLOR_LIGHT = '#f0feff'
EVENTS_BLOCK_COLOR_INTENSIVE = '#c4fbff'

PullEvents.ui_color = EVENTS_BLOCK_COLOR_INTENSIVE
DummyOperatorEventsBlock.ui_color = EVENTS_BLOCK_COLOR_LIGHT
BranchOperatorEventsBlock.ui_color = EVENTS_BLOCK_COLOR_LIGHT
MergeEvents.ui_color = EVENTS_BLOCK_COLOR_LIGHT

# Using a DAG context manager, you don't have to specify the dag property of each task
with DAG('crawlers.etix.venues',
         start_date=datetime(2020, 6, 1),
         max_active_runs=1,
         concurrency=64,
         schedule_interval='@daily',
         default_args=default_args,
         catchup=False  # enable if you don't want historical dag runs to run
         ) as dag:

    oStart = StartOperator(
        task_id='start',
        config_module_path='additional.helpers.etix_venues.etix_config'
    )

    oSkipVenues = DummyOperatorVenuesBlock(
        task_id='skip_venues'
    )

    oEnd = DummyOperator(
        task_id='end'
    )

    operator_3_merge_venues = MergeVenues(
        task_id='merge_venues',
        trigger_rule='all_done'
    )

    operator_1_make_configs = MakeConfigs(
        task_id='make_configs'
    )

    oBranchingVenues = BranchOperatorVenuesBlock(
        task_id="pull_venues_branching",
        python_callable=pull_venues,
        provide_context=True
    )
    oStart >> oBranchingVenues

    oBranchingVenues >> operator_1_make_configs
    oBranchingVenues >> oSkipVenues >> operator_3_merge_venues

    for task in range(NUM_WORKERS_STEP_1):
        operator_2_pull_searches = PullSearches(
            pool=AIRFLOW_POOL_NAME,
            task_id=f'pull_venues_{task}',
            task_idx=task
        )
        operator_1_make_configs >> operator_2_pull_searches >> operator_3_merge_venues

    oBranchingEvents = BranchOperatorEventsBlock(
        task_id="pull_events_branching",
        python_callable=pull_events,
        provide_context=True
    )
    operator_3_merge_venues >> oBranchingEvents

    oSkipEvents = DummyOperatorEventsBlock(
        task_id='skip_events'
    )

    oBeforeEvents = DummyOperatorEventsBlock(
        task_id='before_events'
    )

    oAfterEvents = DummyOperatorEventsBlock(
        task_id='after_events',
        trigger_rule='none_failed_or_skipped'
    )
    oBranchingEvents >> oSkipEvents >> oAfterEvents
    oBranchingEvents >> oBeforeEvents

    operator_5_merge_events = MergeEvents(
        task_id="merge_events"
    )

    for task in range(NUM_WORKERS_STEP_2):
        operator_4_pull_events = PullEvents(
            task_id=f"pull_events_{task}",
            task_idx=task
        )
        oBeforeEvents >> operator_4_pull_events >> operator_5_merge_events

    operator_5_merge_events >> oAfterEvents
    oAfterEvents >> oEnd
