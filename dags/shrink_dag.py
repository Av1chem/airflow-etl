import re
import logging
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.hooks.S3_hook import S3Hook

import additional.helpers.maintenance.maintenance_config as cfg
from additional.tools.sql_util import select_records, delete_rows

from additional.operators.start_operator \
    import StartOperator

# Default settings applied to all tasks
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'provide_context': True,
    'execution_timeout': None
}

with DAG('auxiliary.shrink',
         start_date=datetime(2020, 6, 1),
         max_active_runs=1,
         concurrency=64,
         schedule_interval='@weekly',
         default_args=default_args,
         catchup=False
         ) as dag:

    oStart = StartOperator(
        task_id='start',
        config_module_path='additional.helpers.maintenance.maintenance_config'
    )

    def shrink_db(
            postgres_conn_id, tables, max_age,
            *args, **kwargs
    ):
        hook = PostgresHook(postgres_conn_id=postgres_conn_id)
        size_before = select_records(
            hook, logging,
            f"SELECT pg_size_pretty(pg_database_size('{PostgresHook.get_connection(postgres_conn_id).schema}'))"
        )
        if size_before:
            size_before = size_before[0][0]
        else:
            logging.warning("Database is empty!")
            return

        for t_idx, tbl in enumerate(tables):
            logging.info(f"Proceeding to table # {t_idx+1} of {len(tables)}. Tablename='{tbl}'")

            # check stats
            rows_total = select_records(hook, logging, f'select count(*) from {tbl}')
            if rows_total:
                rows_total = rows_total[0][0]
            else:
                continue
            logging.info(f"Rows total: {rows_total}")

            outdated_cycles = select_records(
                hook, logging,
                f"SELECT DISTINCT(cycle_id) FROM {tbl} WHERE EXTRACT(DAY FROM now() - created_at) > {max_age}"
            )
            if outdated_cycles:
                outdated_cycles = [str(r[0]) for r in sorted(outdated_cycles, key=lambda o: o[0])]
                logging.info("Outdated cycles: " + ", ".join(outdated_cycles))
                outdated_records_total = select_records(
                    hook, logging,
                    f"SELECT COUNT(*) FROM {tbl} WHERE cycle_id IN (" + ", ".join(c for c in outdated_cycles) + ")"
                )
                if outdated_records_total:
                    outdated_records_total = outdated_records_total[0][0]
                    logging.info(f"Outdated records total: {outdated_records_total}")
                    delete_rows(
                        hook, logging, tbl,
                        f"WHERE cycle_id IN (" + ", ".join(c for c in outdated_cycles) + ")"
                    )

            else:
                logging.info("Nothing to shrink in that table, continuing.")

        size_after = select_records(
            hook, logging,
            f"SELECT pg_size_pretty(pg_database_size('{PostgresHook.get_connection(postgres_conn_id).schema}'))"
        )
        if size_after:
            size_after = size_after[0][0]
        else:
            size_after = '0 B'

        logging.info(f"DB has been shrinked. DB Size: {size_before} before/ {size_after} after.")


    oShrinkDB = PythonOperator(task_id='shrink_db', python_callable=shrink_db, op_kwargs={
        'postgres_conn_id': 'crawlers_debug_postgres',
        'tables': cfg.DB_TABLES,
        'max_age': cfg.DB_MAX_AGE_DAYS
    })


    def shrink_s3(
            aws_conn_id, bucket, max_age,
            *args, **kwargs
    ):
        started = datetime.utcnow()
        def sizeof_fmt(num, suffix='B'):
            for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
                if abs(num) < 1024.0:
                    return "%3.1f%s%s" % (num, unit, suffix)
                num /= 1024.0
            return "%.1f%s%s" % (num, 'Yi', suffix)

        s3_hook = S3Hook(aws_conn_id)
        total_size_before = sizeof_fmt(sum(o.size for o in s3_hook.get_bucket(bucket).objects.all()))
        to_delete = []
        dirs_to_delete = set()
        for key in s3_hook.list_keys(
            bucket_name=bucket,
            page_size=999,
            max_items=999
        ):
            found = re.findall(r"\d{4}-\d{2}-\d{2}/", key)
            if found:
                found = found[0]
                created = datetime.strptime(found[:-1], "%Y-%m-%d")
                if (started - created).days > max_age:
                    to_delete.append(key)
                    dirs_to_delete.add(found)
        if not to_delete:
            logging.info("Nothing to delete from bucket!")
        dirs_to_delete = list(dirs_to_delete)
        logging.info(f"Deleting ({len(to_delete)}): \"" + '", "'.join(to_delete) + '"')
        logging.info(f"Deleting folders ({len(dirs_to_delete)}): \"" + '", "'.join(dirs_to_delete) + '"')
        s3_hook.delete_objects(
            bucket=bucket,
            keys=to_delete + dirs_to_delete
        )
        total_size_after = sizeof_fmt(sum(o.size for o in s3_hook.get_bucket(bucket).objects.all()))
        logging.info(f"Bucket size: {total_size_before} before / {total_size_after} after.")


    oShrinkS3 = PythonOperator(task_id='shrink_s3', python_callable=shrink_s3, op_kwargs={
        'aws_conn_id': 'aws_export_bucket',
        'bucket': 'ticket-crawlers-export',
        'max_age': cfg.S3_MAX_AGE_DAYS
    })
    oStart >> oShrinkDB >> oShrinkS3