import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator

from additional.helpers.ticketweb.pull_ids import PullIds
from additional.helpers.ticketweb.merge_data import MergeIds

from additional.operators.create_new_cycle_postgres_operator \
    import CreateNewCyclePostgresOperator
from additional.operators.export_to_csv_operator \
    import ExportAndUpload

# Default settings applied to all tasks
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'provide_context': True,
    'execution_timeout': None,
}

IS_TEST = False
DOMAINS = ['uk', 'com', 'ca', 'ie']
NUM_WORKERS = 4

# Using a DAG context manager, you don't have to specify the dag property of each task
with DAG('crawlers.ticketweb.soldout',
         start_date=datetime(2020, 6, 1),
         max_active_runs=1,
         concurrency=64,
         schedule_interval=None,
         default_args=default_args,
         catchup=False  # enable if you don't want historical dag runs to run
         ) as dag:

    oStart = DummyOperator(
        task_id='start'
    )

    # step 1 - start new cycle, init tables
    operator_0_init_tables = CreateNewCyclePostgresOperator(
        task_id='create_cycle',
        is_test=False,
        is_run_last_cycle=False,
        manual_cycle_id=None,
        postgres_conn_id='crawlers_debug_postgres',
        step_tables={
            'ticketweb': {
                'schema':
                    f'id bigserial not null constraint ticketweb_pk primary key, '
                    f'cycle_id integer, '
                    f'data jsonb, '
                    f'created_at timestamp default now()',
                'insert_schema': [
                    'cycle_id', 'data'
                ],
                'indices': ['cycle_id', ]}
        },
        get_last_cycle_id_query=f'SELECT cycle_id from ticketek_au order by created_at desc limit 1',
        get_max_cycle_id_query=f'SELECT MAX(cycle_id) from ticketek_au',
    )
    oStart >> operator_0_init_tables

    operator_2_merge_ids = MergeIds(
        task_id='merge_ids',
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
        domains=DOMAINS,
        num_workers=NUM_WORKERS
    )
    for domain in DOMAINS:
        operator_1_pull_ids = PullIds(
            task_id=f"pull_ids_from_{domain}",
            cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
            domain=domain,
            is_test=IS_TEST
        )
        operator_0_init_tables >> operator_1_pull_ids >> operator_2_merge_ids

    operator_4_export_and_upload = ExportAndUpload(
        task_id="export_to_csv",
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
        table_name='ticketweb',
        filename_template='ticketweb',
        records_threshold=100
    )
    operator_2_merge_ids >> operator_4_export_and_upload
