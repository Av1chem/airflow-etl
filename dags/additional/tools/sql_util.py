from psycopg2.extras import execute_values

"""
A module for using SQL in Bonya
"""


def delete_rows(hook, logger, table_name, clause):
    """
    @param hook: airflow PostgresHook
    @param logger: the log attribute from Airflow operator
    @param table_name: the name of the table
    """
    delete_query = f"DELETE FROM {table_name} {clause}"
    logger.info(f'Deleting records from {table_name}: {delete_query}')
    hook.run(delete_query, True)
    logger.info('Deleted records.')


def drop_table_if_exists(hook, logger, table_name):
    """
    Drop a table if it exists.

    @param hook: airflow PostgresHook
    @param logger: the log attribute from Airflow operator
    @param table_name: the name of the table to be dropped
    """
    drop_table_query = f'DROP TABLE IF EXISTS {table_name} CASCADE'
    logger.info(f'Executing DROP TABLE command : {drop_table_query}')
    hook.run(drop_table_query, True)
    logger.info("DROP TABLE command complete...")


def does_table_exist(hook, table_name):
    """
    Check if a table already exists

    @param hook: airflow PostgresHook
    @param table_name: the name of the table
    @return:
    """
    table_exists_query = \
        f"SELECT EXISTS(SELECT * FROM information_schema.tables " \
        f"WHERE table_name = '{table_name}')"
    if '.' in table_name:
        schema = table_name.split('.')[0]
        table = table_name.split('.')[1]
        table_exists_query = \
            f"SELECT EXISTS(SELECT * FROM information_schema.tables " \
            f"WHERE table_name = '{table}' and table_schema = '{schema}')"
    return hook.get_first(table_exists_query)[0]


def run_and_commit_query(hook, logger, query):
    """
    Run and commit a query

    @param hook: airflow PostgresHook
    @param logger: the log attribute from Airflow operator
    @param query: the raw SQL for the query
    @return:
    """
    logger.info(f"Running and committing query: {query}")
    hook.run(query, autocommit=True)


def create_table(hook, logger, table_name, create_table_query):
    """
    Run a create table query with baked-in logging

    # TODO we could maybe make this method a bit fancier but not sure
        if it's worth the effort

    @param hook: airflow PostgresHook
    @param logger: the log attribute from Airflow operator
    @param table_name: the name of the table to be created (note: this must be repeated in the CREATE TABLE query)
    @param create_table_query: a query to create the table
    """
    logger.info(f"CREATING TABLE {table_name}")
    run_and_commit_query(hook, logger, create_table_query)
    logger.info(f"TABLE created {table_name}")


def create_table_like(hook, logger, table_name, reference_table_name):
    """
    Run a create table like another table

    @param hook: airflow PostgresHook
    @param logger: the log attribute from Airflow operator
    @param table_name: the name of the table to be created (note: this must be repeated in the CREATE TABLE query)
    @param reference_table_name: the name of reference table
    """
    logger.info(f"CREATING TABLE {table_name} LIKE TABLE {reference_table_name}")
    run_and_commit_query(hook, logger,
                         f'CREATE TABLE IF NOT EXISTS {table_name} (LIKE {reference_table_name} INCLUDING ALL)')
    logger.info(f"TABLE created {table_name}")


def rename_table(hook, logger, table_name, rename_to_table_name):
    """
    Run a rename table

    @param hook: airflow PostgresHook
    @param logger: the log attribute from Airflow operator
    @param table_name: the name of the table to be created (note: this must be repeated in the CREATE TABLE query)
    @param rename_to_table_name: the name of renamed table
    """
    logger.info(f"RENAMING TABLE {table_name} LIKE TABLE {rename_to_table_name}")
    run_and_commit_query(hook, logger, f'alter table {table_name} rename to {rename_to_table_name}')


def insert(cursor, logger, table_name, columns, value_tuples, verbose=True):
    """

    Inserts values into a table but does not commit the query.

    Remember to call `.commit()` (and also `.close()`) on the connection
    the cursor derives from somewhere in your calling code.

    @param cursor: a PostgresHook cursor
    @param logger: the log attribute from Airflow operator
    @param table_name: the name of the table to insert the values into
    @param columns: the name of the columns to insert values into
    @param value_tuples: a list of tuples of values to be inserted into the specified columns;
        NOTE: the values must be in the same order as the columns
    @param verbose: True if logging all value_tuples, False if logging only first value_tuple
    """
    logger.info(f"running: {_insert_query_log_stmt(table_name, columns, value_tuples, verbose)}")
    columns_str = f"({', '.join([f'{value}' for value in columns])})"
    insert_query = f"INSERT INTO {table_name} {columns_str} VALUES %s"
    execute_values(cursor, insert_query, value_tuples)


def insert_and_commit(conn, logger, table_name, columns, value_tuples, verbose=True):
    """

    Inserts values into a table and commits the query.

    **Please** remember to close your connection in your calling code
    sometime after calling this method.
    eg. `conn.close()`

    @param conn: a PostgresHook connection
    @param logger: the log attribute from Airflow operator
    @param table_name: the name of the table to insert the values into
    @param columns: the name of the columns to insert values into
    @param value_tuples: a list of tuples of values to be inserted into the specified columns;
        NOTE: the values must be in the same order as the columns
    @param verbose: True if logging all value_tuples, False if logging only first value_tuple
    """
    cursor = conn.cursor()
    insert(cursor, logger, table_name, columns, value_tuples, verbose)
    logger.info(f"committing: {_insert_query_log_stmt(table_name, columns, value_tuples, verbose)}")
    conn.commit()


def select_records(hook, logger, select_query):
    """
    # TODO
    @param hook:
    @param logger:
    @param select_query:
    @return:
    """
    logger.info(f"running: {select_query}")
    return hook.get_records(select_query)


def insert_in_batches(conn, logger, table_name, columns, value_tuples, batch_size=1000, verbose=True):
    """
    Insert into a table in batches of records.

    @param conn: a PostgresHook connection
    @param logger: the log attribute from Airflow operator
    @param table_name: the name of the table to insert into
    @param columns: a tuple of strings of the columns to insert into
    @param value_tuples: the values to insert
    @param batch_size: the number of records to insert at once
    @param verbose: True if logging all value_tuples, False if logging only first value_tuple
    """

    if len(value_tuples) < batch_size:
        insert_and_commit(conn, logger, table_name, columns, value_tuples, verbose)
        return

    final_idx = 0
    for idx in range(0, len(value_tuples), batch_size):
        tuples_to_write = value_tuples[idx:idx + batch_size]
        insert_and_commit(conn, logger, table_name, columns, tuples_to_write, verbose)
        final_idx = idx + batch_size

    insert_and_commit(conn, logger, table_name, columns, value_tuples[final_idx:], verbose)


def _insert_query_log_stmt(table_name, columns, value_tuples, verbose=True):
    if verbose:
        return f"running: INSERT into {table_name} {columns} VALUES {value_tuples}"
    else:
        return f"running: INSERT into {table_name} {columns} VALUES {value_tuples[:1]} ..."


# we keep only 10 last cycles in step tables to keep them small enough
def delete_bogus_cycles(hook, logger, table_name, keep_cycles=10):
    logger.info(f'Removing bogus cycles from {table_name}, keeping last {keep_cycles} cycles')
    delete_rows(hook, logger, table_name, f'where cycle_id not in (select cycle_id from {table_name} group by 1 '
                                          f'order by 1 desc limit {keep_cycles})')
