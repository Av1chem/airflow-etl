from dill.source import getsource

from additional.tools.sql_util import select_records, delete_rows, insert_and_commit, run_and_commit_query


class DatabaseMixin:

    def _compile_clause(self):
        clause = ''
        if hasattr(self, 'cycle_id'):
            clause += f'AND cycle_id={self.cycle_id} '
        if hasattr(self, 'task_idx'):
            clause += f"AND task_id={self.task_idx} "
        if hasattr(self, 'step_idx'):
            clause += f"AND step_idx={self.step_idx} "
        return clause

    def cleanup_results(self, step_tablename, hook=None):
        self.log.info("Starting cleanup.")

        clause = self._compile_clause()
        if hook is None:
            hook = self.hook

        rows_to_delete = select_records(
            hook, self.log,
            f"SELECT count(*) FROM {step_tablename} WHERE TRUE {clause}"
        )
        if not rows_to_delete or not rows_to_delete[0][0]:
            self.log.info(f"Nothing to delete for {clause}.")
            return
        rows_to_delete = rows_to_delete[0][0]
        self.log.info(
            f"Total {rows_to_delete} rows deleting for {clause}..."
        )
        delete_rows(hook, self.log, step_tablename, f'WHERE TRUE {clause}')
        self.log.info("Cleanup success.")

    def get_configs_count(self, step_tablename, hook=None):
        clause = self._compile_clause()
        if hook is None:
            hook = self.hook
        configs_count = select_records(
            hook, self.log,
            f"select count(*) from {step_tablename} "
            f"WHERE TRUE {clause} and status is null"
        )
        if configs_count:
            return configs_count[0][0]
        else:
            return 0

    def get_configs_to_parse(self, step_tablename, columns, limit=100, order_by="id asc", hook=None):

        clause = self._compile_clause()
        if hook is None:
            hook = self.hook

        try:
            select_query = "select " + ", ".join(columns) + f" from {step_tablename} " \
                                                            f"where true {clause} and status is null " + f"order by {order_by} " + \
                           f"limit {limit}"
            records = select_records(
                hook, self.log, select_query
            )
            configs = [{k: v for k, v in zip(columns, r)} for r in records]
        except Exception as ex:
            print(ex)
            self.log.error(f'Exception while read configs from database', exc_info=True)
            raise

        return configs

    def update_configs(self, ids, result, step_tablename, hook=None):
        self.log.info(f"Updating status={result} for ids={ids}.")
        if hook is None:
            hook = self.hook
        run_and_commit_query(
            hook, self.log,
            f"update {step_tablename} set updated_at = now(), status = {str(result.value)} "
            f"where id in ({', '.join(str(i) for i in ids)})"
        )

    def write_to_db(
            self,
            step_tablename,
            insert_schema,
            records,
            to_tuple=None,
            batch_size=1000,
            hook=None):

        self.log.info("Starting DB inserts")
        batch_count = len(records) // batch_size + (1 if len(records) % batch_size > 0 else 0)
        if hook is None:
            hook = self.hook

        if to_tuple is not None:
            self.log.info(f"DB insert: to_tuple={getsource(to_tuple).split('to_tuple=')[-1]}")
            for i in range(len(records)):
                records[i] = to_tuple(records[i])

        with hook.get_conn() as conn:
            for b_idx, b_start_idx in enumerate(range(0, len(records), batch_size)):
                b_end_idx = b_start_idx + batch_size
                self.log.info(f"Writing batch # {b_idx + 1} of {batch_count}, "
                              f"size={len(records[b_start_idx:b_end_idx])}")
                insert_and_commit(
                    conn, self.log, step_tablename, insert_schema,
                    records[b_start_idx:b_end_idx], verbose=False
                )

        self.log.info("Wrote successfully")
