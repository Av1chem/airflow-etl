import json
import os

from airflow.models.variable import Variable


class ConfigReader(object):

    def __init__(self, env_var):

        self.env_var = env_var
        try:
            self.config = json.loads(os.environ[self.env_var])
        except KeyError:
            self.config = {}

        # config stored in airflow variable is prioritized over ENV variable
        try:
            self.airflow_var = Variable.get(self.env_var, deserialize_json=True)
            self.config.update(self.airflow_var)
        except KeyError:
            pass

    def read_or_default(self, key, default=None):
        return self.config[key] if key in self.config else default

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self
