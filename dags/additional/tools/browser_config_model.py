from collections import namedtuple

ProxyConfig = \
namedtuple(
    'Proxy', [
        'hst', 'prt', 'usr', 'pwd'
    ]
)
ScreenSizeConfig = \
namedtuple(
    'Size', [
        'width', 'height'
    ]
)
ScreenConfig = \
namedtuple(
    'Screen', [
        'size', 'depth'
    ]
)

BrowserConfig = \
namedtuple(
    'BrowserConfig', [
        'config_id',
        'major_version',
        'proxy',
        'screen',
        'lang',
        'disable_webgl',
        'init_pages'
    ]
)


class ConfigList(object):

    def __init__(self, *args, **kwargs):
        self.configs = [*args]

    def get_by_id(self, id):
        return next(b for b in self.configs if b.config_id == id)
