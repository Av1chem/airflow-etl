from enum import Enum

ALPHABET = 'abcdefghijklmnopqrstuvwxyz'


class CollectingStatus(Enum):

    SUCCESS = 0
    ERROR = 1
    UP_TO_RESCHEDULE = 2
    TOO_MUCH_RESULTS = 3
    NO_RESULTS = 4


class AvailabilityStatus(Enum):

    AVAILABLE = 0
    NOT_ON_SALE = 10
    SOLD_OUT = 20
    UNKNOWN = 30
    ONLINE_SALES_ENDED = 40
    CANCELLED = 50

    @property
    def status_name(self):
        if self == self.AVAILABLE:
            return 'available'
        elif self == self.NOT_ON_SALE:
            return 'not_on_sale'
        elif self == self.SOLD_OUT:
            return 'sold_out'
        elif self == self.UNKNOWN:
            return 'unknown'
        elif self == self.ONLINE_SALES_ENDED:
            return 'online_sales_have_ended'
        elif self == self.CANCELLED:
            return 'cancelled'
