import json
import random
from datetime import datetime as dt, timedelta

from requests import get
from bs4 import BeautifulSoup as bs

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.hooks.postgres_hook import PostgresHook

from additional.tools import sql_util
from additional.helpers.fgt.fgt_config import *
from additional.tools.database_mixin import DatabaseMixin


class PullEvents(BaseOperator, DatabaseMixin):
    """
    """

    template_fields = ('cycle_id', )
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.cycle_id = cycle_id
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)

    def execute(self, context):

        starting_time = dt.utcnow()

        self.cleanup_results(TABLE)

        sites = SITES
        self.log.info('got {0} sites from sheet'.format(len(sites)))

        # parse each site
        parsed_events = []
        for site in sites:
            self.log.info('parsing site: {}'.format(site))
            user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
            index_page = bs(get(site, headers={'User-agent': user_agent}).content, 'html.parser')

            # parse each event
            max_idx = len(index_page.select('.details > h2 > a')) - 1
            for i, event in enumerate([href.attrs['href'] for href in index_page.select('.details > h2 > a')]):
                event_page = bs(get(event, headers={'User-agent': user_agent}).content, 'html.parser')
                self.log.info('event {0} of {1}: {2}'.format(i + 1, max_idx + 1, event))

                min_price, max_price = 9999999999, 0
                ticket_details, on_sale_at = '', ''
                sold_out = True
                # extract tickets
                if event_page.select('.onSaleDate'):
                    on_sale_at = event_page.select_one('.onSaleDate').get_text().replace('On Sale', '').strip()
                    sold_out = False
                    min_price = 0
                    max_price = 0
                else:
                    for tix, price, quan in zip(
                            [e.get_text().split('\n')[1].strip() for e in
                             event_page.select('.ticket-price-section > .row > .xtix-select')],
                            [e.get_text().split('\n')[1].strip() for e in
                             event_page.select('.ticket-price-section > .row > .xtix-price')],
                            [e.get_text().split('\n')[1].strip() for e in
                             event_page.select('.ticket-price-section > .row > .quantity')]
                    ):
                        min_price = min_price if min_price < float(price[1:]) else float(price[1:])
                        max_price = max_price if max_price > float(price[1:]) else float(price[1:])
                        ticket_details += '{0}: {1} ({2}) /'.format(tix, price, quan if quan else 'AVAILABLE')
                        if 'SOLD OUT' not in quan:
                            sold_out = False
                    if ticket_details:
                        ticket_details = ticket_details[:-2]
                    else:
                        sold_out = 'Tickets unavailable at this time' in event_page.text
                        min_price = 0
                        max_price = 0

                parsed_events.append({
                    'name': event_page.select_one('h2.event-name').get_text().strip(),
                    'url': event,
                    'date': event_page.select_one('.date').get_text().strip(),
                    'time': event_page.select_one('div.time').get_text().strip() if len(
                        event_page.select('div.time')) else event_page.select_one('div.doors').get_text().strip(),
                    'age_restrictions': event_page.select_one('.age').get_text().strip(),
                    'venue_name': event_page.select_one('.venue').get_text().strip(),
                    'address': event_page.select_one('.address').get_text().strip(),
                    'min_price': min_price,
                    'max_price': max_price,
                    'ticket_details': ticket_details,
                    'sold_out': sold_out,
                    'on_sale_at': on_sale_at
                })

        self.log.info(
            'parsing finished. Execution time: {0:.2f} sec'.format((dt.utcnow() - starting_time).total_seconds()))

        self._write_events(parsed_events)

    def _write_events(self, events):

        for s_i in range(0, len(events), SQL_BATCH_SIZE):
            self.log.info(f"DB: writing batch of size {len(events[s_i:s_i+SQL_BATCH_SIZE])}.")
            tuple_to_write = [
                (self.cycle_id, json.dumps(e)) for e in events[s_i:s_i+SQL_BATCH_SIZE]
            ]
            if tuple_to_write:
                with self.hook.get_conn() as conn:
                    self.log.info(f"DB: inserting {len(tuple_to_write)} events.")
                    sql_util.insert_and_commit(
                        conn, self.log, TABLE, ('cycle_id', 'data'), tuple_to_write,
                        verbose=False
                    )
                    self.log.info(f"DB: inserted successfully.")
