import json
import random
import time

import requests
from time import sleep
from datetime import datetime as dt, timedelta

from requests import get
from bs4 import BeautifulSoup as bs

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from twocaptcha import TwoCaptcha

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.hooks.postgres_hook import PostgresHook
from airflow.exceptions import AirflowException

from additional.tools import sql_util
from additional.tools.database_mixin import DatabaseMixin
from additional.operators.anti_distill_operator import AntiDistillBrowser

TABLE = 'ticketek_au'
SQL_BATCH_SIZE = 1000
TWOCAP_API_KEY = 'SECRET'


class PullEvents(AntiDistillBrowser, DatabaseMixin):
    """
    """

    template_fields = ('cycle_id', )
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.cycle_id = cycle_id
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)

    def token_expired(self):
        return (dt.utcnow() - self._tokentime).seconds > 600

    def get_token(self):
        if not hasattr(self, '_token') or self.token_expired():
            url = "https://api-ignition.ticketek.com.au/v2/oauth2/token"

            payload='grant_type=client_credentials'
            headers = {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': 'Basic SECRET'
            }
            self._token = requests.request("POST", url, headers=headers, data=payload).json()['access_token']
            self._tokentime = dt.utcnow()
            self.log.info(f"Got token: {self._token}")
        return self._token

    def periods(self):

        if self._is_test:
            s_d = dt.utcnow() + timedelta(days=1)
            p_limit = 5
        else:
            s_d = dt.utcnow()
            p_limit = 100

        for i in range(p_limit):
            e_d = s_d + timedelta(days=7)
            yield s_d.strftime('%Y-%m-%d') + 'T00:01:01Z', e_d.strftime('%Y-%m-%d') + 'T00:01:01Z'

            s_d = e_d

    def execute(self, context):

        starting_time = dt.utcnow()

        self.cleanup_results(TABLE)
        all_events = []
        all_e_ids = set()
        for w_i, dates in enumerate(self.periods()):
            s_d, e_d = dates[0], dates[1]
            tkn = self.get_token()
            url = 'https://api-ignition.ticketek.com.au/v1/products/search?q=matchall&q.parser=structured&size=5000&sort=date%20asc&fq=(and%20date:[%27' + s_d + '%27,%27' + e_d + '%27})&cursor=initial'
            evs = requests.get(url,
                                headers={
                                    'User-Agent': 'Android',
                                    'Authorization': f'Bearer {tkn}'
                                },
                                ).json()['hits']['hit']
            for e in evs:
                if e['id'] not in all_e_ids:
                    all_e_ids.add(e['id'])
                    all_events.append(e)
            self.log.info(f"w_i={w_i}, e_total={len(all_events)}, that_week={len(evs)}")

        # get Incapsula cookie
        self._ticketek_cookie = self._get_cookie()
        full_events = []
        for idx, e in enumerate(all_events):
            # time.sleep(10)
            for cookie_attempt in range(5):
                for attempt in range(5):
                    try:
                        headers = {
                            'authority': 'premier.ticketek.com.au',
                            'pragma': 'no-cache',
                            'cache-control': 'no-cache',
                            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
                            'sec-ch-ua-mobile': '?0',
                            'upgrade-insecure-requests': '1',
                            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
                            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                            'sec-fetch-site': 'none',
                            'sec-fetch-mode': 'navigate',
                            'sec-fetch-user': '?1',
                            'sec-fetch-dest': 'document',
                            'accept-language': 'ru-RU,ru;q=0.9',
                            # 'cookie': 'ticketek.com.au+cookies=true; visid_incap_2399871=aWPx1nRsQhmAoWdi32FYaKKasGAAAAAAQUIPAAAAAAChjC/IHTLrV4iUlEJw0ySY; nlbi_2399871=gS8ML1uxOXJhwuu7ROFu0QAAAACOUa0pi84hw4ibK0awrygh; incap_ses_582_2399871=IZNSZLXSC1jLURP3iK4TCKSasGAAAAAAzuGI/FEv7FiEqs3MVQNcNA==; server-group=B; dtCookie=v_4_srv_3_sn_3C4E2AF8FBC51CCC791B608DC8E9EC01_perc_100000_ol_0_mul_1; visid_incap_2408402=niHJ4dWQRh2VIEmL9bQAb6SasGAAAAAAQUIPAAAAAADBburk6TdNK4dp7gbC6Rls; nlbi_2408402=PcTRTcTrxHlWbiVlIjE9VQAAAABm6FdR4SOntWIbhRgvQ0lb; incap_ses_582_2408402=vgkfOHbuKilXUxP3iK4TCKWasGAAAAAACkxh+ihlIJntN/bvvAKHUA==; AWSALBTG=c83XGFPwLp+5QzGbP7gmmVVzuUW31yGLFyGnbWDt6fms9hfQOZBB9K5qKCgbdouEP+gRE8CLcsouXxRrD0JjH1j7BCSaLuJw2y4EGzDSBdTYoim12z3AKgNTO6rn2heqvN9cUHt8dshPBb6lXp1AdS5J+i6ddsJHywGqExzQoVW11yIDihY=; AWSALBTGCORS=c83XGFPwLp+5QzGbP7gmmVVzuUW31yGLFyGnbWDt6fms9hfQOZBB9K5qKCgbdouEP+gRE8CLcsouXxRrD0JjH1j7BCSaLuJw2y4EGzDSBdTYoim12z3AKgNTO6rn2heqvN9cUHt8dshPBb6lXp1AdS5J+i6ddsJHywGqExzQoVW11yIDihY=; ticketek.com.au+cp.id=84b3645e-639d-4ace-84f3-ea11cb96cf4b; ticketek.com.au+cp.ex=2021-05-28+17%3a27%3a52; ticketek.com.au+cp.st=2021-05-28+17%3a24%3a22; __RequestVerificationToken=MZkOG1OAHLrdOSM5OfLBmfS5xjOFMkGP0NYVIuecCJxcOrPqVJiIiYfvdTynVYfJ0bpNh1QmPdMLw-QvEKzxRU8v2Qo1; nlbi_2399871_2147483646=RmavVV6Rdw9swPD9ROFu0QAAAADIGvIgmuRMK/8O2QPTpKqL; __session:0.31107096998018235:=https:; softix.affiliate=Ticketek%20Au%20Website',
                            # 'cookie': 'myCookie=; ticketek.com.au+cookies=true; visid_incap_2399871=q/kF3F0NT3arbtezBYDvRY6QsGAAAAAAQkIPAAAAAACA8oqcAVlxjRVVgh42akUrutKXPg8d+prd; nlbi_2399871=V9UXF7pJ3xsu1XumROFu0QAAAAA66tC/xbrMAL88xDcMJHji; server-group=B; dtCookie=v_4_srv_3_sn_23AD7D54E30289176B7BC498835FF379_perc_100000_ol_0_mul_1; visid_incap_2408402=Y/5SxIEgTZi5OpcbJx3T/LOPsGAAAAAAQUIPAAAAAACRKcoP4pbK6LD4ZMYCa/f5; nlbi_2408402=kfXnKPgeVnAdDhjvIjE9VQAAAABsjjVL7+F92SuDm1jdUqoQ; incap_ses_586_2408402=V22ZUZvvn1uhiRJAeuQhCL+QsGAAAAAAc0HTsj72CDYprk911QJx2w==; softix.affiliate=Ticketek%20Au%20Website; myCookie=; ab.storage.deviceId.35e9ff84-9413-4d1a-853b-5cdeba1dfdef=%7B%22g%22%3A%2243891d79-8de4-6c7f-77e7-5773872dd0fa%22%2C%22c%22%3A1622184130273%2C%22l%22%3A1622184130273%7D; _ga=GA1.3.646355680.1622184134; _gid=GA1.3.168740890.1622184134; _fbp=fb.2.1622184134980.150745433; _hjTLDTest=1; _hjid=b6909aec-d257-40e6-bf37-f522186adb89; __gads=ID=1dea7f033ce2118e-2231fdbf2ec80009:T=1622184135:S=ALNI_MapKTzTRuYWonvTcQl-DtXUL1s1dQ; _gcl_au=1.1.1401318864.1622184138; _ga=GA1.4.646355680.1622184134; _gid=GA1.4.168740890.1622184134; __session:0.40624355976231397:roktWidgetType=; __session:0.40624355976231397:vehicleNumRequired=False; __session:0.40624355976231397:viewBasketJSFile=; __session:0.40624355976231397:visaPresaleEnabled=False; __session:0.40624355976231397:gpayoption=False; __session:0.40624355976231397:showCategory=null; __session:0.40624355976231397:ameBtnText=; __session:0.40624355976231397:ameBtnURL=; __RequestVerificationToken=BySF0WNtQUGxGxp6iAH9Slt9usBy3ykQz5KggEK402lxGUXNu6dAFb7CoV-FGO1xIrCSJhtf2gKtjHGIXoqf14a4sQc1; __session:0.40624355976231397:accessibleSeatingVenues=SSD; __session:0.4066207794330725:roktWidgetType=; __session:0.4066207794330725:vehicleNumRequired=False; __session:0.4066207794330725:viewBasketJSFile=; __session:0.4066207794330725:visaPresaleEnabled=False; __session:0.4066207794330725:gpayoption=False; __session:0.4066207794330725:showCategory=null; __session:0.4066207794330725:ameBtnText=; __session:0.4066207794330725:ameBtnURL=; __session:0.4066207794330725:accessibleSeatingVenues=SSD; incap_ses_582_2399871=iqNjffcGVgKsaT/3iK4TCJHMsGAAAAAAm8Ef/zig9YlWglWiVmKCFg==; incap_ses_770_2408402=KAOcIzf903zaVyTbOpevCi/QsGAAAAAAbxlheZj5D8EWzTj0a6NpRw==; ticketek.com.au+cp.id=19b4f81a-8434-4b17-b636-fb3c8eda7ea9; ticketek.com.au+cp.ex=2021-05-28+21%3a16%3a18; ticketek.com.au+cp.st=2021-05-28+21%3a12%3a48; incap_ses_586_2399871=G7dbM5BiSiZaZHtAeuQhCDHQsGAAAAAA3bcKMPr3Fj+7BSOyWsJjow==; __session:0.781119054538213:=https:; AWSALBTG=yxVG8tEhMg7Ncv5ZlEBT08iu6mDSdi2LZe8Ercknf2jRW6RguhXNHoHEdwHcr6w1qYXTUBIRc+DnYXrcTcOkJ36LSml7jy4xQFG0taaezkAtLDFUY4Vpa3cBrSXD7Ke6ecG0+4XLSZAYS7zCXpkzsl/OtSudp8niEe1H5mPFGAALy3Vv8yc=; AWSALBTGCORS=yxVG8tEhMg7Ncv5ZlEBT08iu6mDSdi2LZe8Ercknf2jRW6RguhXNHoHEdwHcr6w1qYXTUBIRc+DnYXrcTcOkJ36LSml7jy4xQFG0taaezkAtLDFUY4Vpa3cBrSXD7Ke6ecG0+4XLSZAYS7zCXpkzsl/OtSudp8niEe1H5mPFGAALy3Vv8yc=; nlbi_2399871_2147483646=4GHGORh6zlRS7v8tROFu0QAAAAAt5yISbL+25K1b0PbG4tZX; reese84=3:4tOpOhX8Z8eIMa0DaqXZQg==:jbJU7qMFn9FiWf2my9hu31aKy9ZG5KRxAPaAkVQvnP8do3ybZRGYDpcysWo2cS8vLY3NuTqWZWQAENWqZ6SRCignSDLwIVkP8Ieb4dcXz16Nig3TdMq0/tx98U/vajL7BIlV+frN8sh6gGAI/0SHojm/3ESHuvpqGVfre3RYeL09wTgkO6P+XhV8irsaiSg73ai3neF6GyJaDteS06YoyxCjhuDeyeZBU0QgyQU9WCjlrgiim+EtyJ2jiowggWJ2mdtL85NC8H6R3cSiRDdyCHCjGPGP+r+db6rAHxeVyBsJi/l3a78tIfjzn3B2B5ndrrJIBhFV3PUz5HSuixtscZT5smzctizKI3oJLg1eWW92mrNwSQoQLVQPjVY0EsHxmmdu+nreyUcZ1wlpiXzNotRcU2xAF4o4iAAY6lLglpQ=:0A/BUUHVeXka3GwcUo0H9q7pNQrc/1uhttfDZQm86Rs=; ab.storage.sessionId.35e9ff84-9413-4d1a-853b-5cdeba1dfdef=%7B%22g%22%3A%22802b9191-6090-56d2-85a1-e8caae6ece96%22%2C%22e%22%3A1622202177380%2C%22c%22%3A1622200377340%2C%22l%22%3A1622200377380%7D; _gat=1; _gat_UA-63445827-1=1; _dc_gtm_UA-43862537-2=1; ins-storage-version=8',
                            'cookie': self._ticketek_cookie
                        }
                        res = requests.get(
                            f"{e['fields']['purchase']}/tickets/performanceinfo",
                            headers=headers,
                            proxies={
                                "http": "http://lum-customer-c_154c57cc-zone-ticketek_au-ip-178.171.15.234:qbod1kbqv9i8@zproxy.lum-superproxy.io:22225",
                                "https": "http://lum-customer-c_154c57cc-zone-ticketek_au-ip-178.171.15.234:qbod1kbqv9i8@zproxy.lum-superproxy.io:22225",
                            }
                        )
                        resp = res.json()
                        break
                    except:
                        self.log.error(f"Attempt # {attempt+1}: Error while parsing event {idx+1} of {len(all_events)}: {e['fields']['purchase']}")
                        resp = None
                        sleep(5)
                        continue
                if resp is not None:
                    break
                else:
                    self.log.error(
                        f"Cookie attempt # {cookie_attempt+1}: "
                        f"Error while parsing event {idx + 1} of {len(all_events)}: {e['fields']['purchase']}")
                    self._ticketek_cookie = self._get_cookie()

            if resp is None:
                raise AirflowException("Seems we got blocked :( ...")

            e['details'] = resp
            full_events.append(e)
            self.log.info(f"Parsed event {idx+1} of {len(all_events)}: {e['fields']['purchase']}")
            if len(full_events) >= SQL_BATCH_SIZE:
                self._write_events(full_events)
                full_events = []
        self.log.info(
            'parsing finished. Execution time: {0:.2f} sec'.format((dt.utcnow() - starting_time).total_seconds()))

        if full_events:
            self._write_events(full_events)

    def _get_cookie(self):
        self.get_browser()
        self.parse_page('https://premier.ticketek.com.au/')
        got_recap = False
        try:
            recap = self.dr_find_element('.g-recaptcha', 10, silent=True)
            if recap is None:
                frame = self.dr_find_element('iframe[src*="/_Incaps"]', 10)
                self.dr.switch_to.frame(frame)
            got_recap = True
        except TimeoutException:
            event_link = self.dr_find_element('.featuredEventsSection a').get_attribute('href')
            self.parse_page(event_link)
            try:
                recap = self.dr_find_element('.g-recaptcha', 10, silent=True)
                if recap is None:
                    frame = self.dr_find_element('iframe[src*="/_Incaps"]', 10)
                    self.dr.switch_to.frame(frame)
                got_recap = True
            except TimeoutException:
                pass
        if got_recap:
            self.log.info(f"Solving captcha...")
            recap = self.dr_find_element('.g-recaptcha')
            site_key = recap.get_attribute('data-sitekey')
            re_callback = recap.get_attribute('data-callback')
            solver = TwoCaptcha(TWOCAP_API_KEY)
            result = solver.recaptcha(sitekey=site_key,
                                      url=self.dr.current_url)
            self.dr.execute_script(
                'var element=document.getElementById("g-recaptcha-response"); element.style.display="";')
            self.dr.execute_script("""
                      document.getElementById("g-recaptcha-response").innerHTML = arguments[0]
                    """, result['code'])
            self.dr.execute_script(f'this.onCaptchaFinished(\'{result["code"]}\')')
            # self.dr.execute_script(f'{re_callback}(\'{result["code"]}\');')
        else:
            self.log.info(f"Lucky day, no recaptcha!")
        WebDriverWait(
            self.dr, 320
        ).until(
            EC.title_contains('Ticketek')
        )
        for _ in range(3):
            WebDriverWait(
                self.dr, 60
            ).until(
                lambda dr: len(dr.get_cookies()) > 40
            )
            sleep(3)

        cookies_list = self.dr.get_cookies()
        cookieString = ""
        for cookie in cookies_list[:-1]:
            cookieString = cookieString + cookie["name"] + "=" + cookie["value"] + "; "
        cookieString = cookieString + cookies_list[-1]["name"] + "=" + cookies_list[-1]["value"]
        self.log.info(f"Cookie ({len(cookies_list)}): {cookieString}")

        self.shutdown_all()

        return cookieString

    def _write_events(self, events):

        for s_i in range(0, len(events), SQL_BATCH_SIZE):
            self.log.info(f"DB: writing batch of size {len(events[s_i:s_i+SQL_BATCH_SIZE])}.")
            tuple_to_write = [
                (self.cycle_id, json.dumps(e)) for e in events[s_i:s_i+SQL_BATCH_SIZE]
            ]
            if tuple_to_write:
                with self.hook.get_conn() as conn:
                    self.log.info(f"DB: inserting {len(tuple_to_write)} events.")
                    sql_util.insert_and_commit(
                        conn, self.log, TABLE, ('cycle_id', 'data'), tuple_to_write,
                        verbose=False
                    )
                    self.log.info(f"DB: inserted successfully.")
