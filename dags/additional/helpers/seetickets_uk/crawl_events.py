import json
import re
import time
from datetime import datetime as dt

import hjson
import requests
from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from bs4 import BeautifulSoup as bs

from additional.helpers.seetickets_uk.seetickets_uk_config import *
from additional.tools.database_mixin import DatabaseMixin


class PullEvents(BaseOperator, DatabaseMixin):
    """
    """

    template_fields = ('cycle_id',)
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.cycle_id = cycle_id
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)
        self.starting_time = dt.utcnow()
        self.events_parsed = 0

    def execute(self, context):

        self.cleanup_results(TABLE)

        self.log.info('SEARCH: Start parsing list of events in UK. Exec time {:.2f}'.format(self.exec_time()))
        events = []
        load_script_attempts = 0
        blanc_pages_in_a_row = 0
        for i in range(1, 999999999):
            resp = self._get_resp(URL_TEMPLATE.format(i))
            obj = bs(resp.text, 'lxml')
            try:
                ld_json = obj.select_one('script[type="application/ld+json"]')

                if ld_json is None:
                    if load_script_attempts < GET_SCRIPT_RETRIES:
                        load_script_attempts += 1
                        self.log.warning(
                            "SEARCH: Error while parsing page #{}. No ld+json on page. Repeat. {:.2f}pps. Exec time: "
                            "{:.2f} ".format(i, i / et, et)
                        )
                        i -= 1
                        continue
                    elif blanc_pages_in_a_row < BLANC_PAGES_LIMIT:
                        blanc_pages_in_a_row += 1
                        load_script_attempts = 0
                        self.log.warning(
                            "SEARCH: page #{}, blanc_pages_in_a_row={} (limit={}). Seems like empty page Repeat. "
                            "{:.2f}pps. Exec time: {:.2f} ".format(
                                i, blanc_pages_in_a_row, BLANC_PAGES_LIMIT, i / et, et)
                        )
                        continue
                    else:
                        self.log.warning(f"All retries passed, no new events. Finishing.")
                        break

                ld = json.loads(ld_json.string)
            except json.JSONDecodeError:
                # no more events, or an error loading page
                if load_script_attempts < GET_SCRIPT_RETRIES:
                    load_script_attempts += 1
                    self.log.warning(
                        "SEARCH: Error parsing script on page #{}, retry#{} of {}. Repeat. {:.2f}pps. Exec time: "
                        "{:.2f} ".format(i, load_script_attempts, GET_SCRIPT_RETRIES, i / et, et)
                    )
                    i -= 1
                    continue
                elif blanc_pages_in_a_row < BLANC_PAGES_LIMIT:
                    blanc_pages_in_a_row += 1
                    load_script_attempts = 0
                    self.log.warning(
                        "SEARCH: page #{}, blanc_pages_in_a_row={} (limit={}). Seems like empty page Repeat. "
                        "{:.2f}pps. Exec time: {:.2f} ".format(
                            i, blanc_pages_in_a_row, BLANC_PAGES_LIMIT, i / et, et)
                    )
                else:
                    self.log.warning(f"All retries passed, no new events. Finishing.")
                    break

            et = self.exec_time()
            self.log.info('SEARCH: Page #{} parsed. {:.2f}pps. Exec time: {:.2f}'
                          .format(i, i / et, et))

            for l_i, l in enumerate(ld):
                l['id'] = l['url'].split('/')[-1]
                l['url'] = 'https://www.seetickets.com' + l['url']
                event = self.parse_event(l)
                if event:
                    self.log.info('EVENT: page {}, parsed event {} of {}'.format(i, l_i+1, len(ld)))
                    events.append(event)
                else:
                    self.log.error(
                        'EVENT: page {}, event {} of {}, can\'t parse this: {}'.format(i, l_i+1, len(ld), l))
                    events.append(l)

                if len(events) >= SQL_BATCH_SIZE:
                    self.write_to_db(
                        step_tablename=TABLE,
                        insert_schema=('cycle_id', 'data'),
                        records=events,
                        to_tuple=lambda rec: (self.cycle_id, json.dumps(rec))
                    )
                    events = []

            load_script_attempts = 0
            blanc_pages_in_a_row = 0

        if events:
            self.write_to_db(
                step_tablename=TABLE,
                insert_schema=('cycle_id', 'data'),
                records=events,
                to_tuple=lambda rec: (self.cycle_id, json.dumps(rec))
            )

        self.log.info('SEARCH: Finished parsing. Events total: {}. Exec time {:.2f}'
                      .format(self.events_parsed, self.exec_time()))

    def _get_resp(self, url):
        try_number = 0
        while True:
            try:
                resp = requests.get(url)
                return resp
            except Exception as ex:
                if try_number >= GET_RETRIES:
                    self.log.error(f"Too much retries for url={url}, raising error.", exc_info=True)
                    raise ex
                else:
                    self.log.error(f"Retry #{try_number+1} of {GET_RETRIES}: Error getting url={url}.")
                    try_number += 1
                self.log.error(f"Sleep for {RETRY_INTERVAL}")
                time.sleep(RETRY_INTERVAL)

    def parse_event(self, event):
        try:
            resp = self._get_resp(event['url'])
        except requests.exceptions.TooManyRedirects:
            return None

        if resp.status_code != 200:
            return None
        else:
            obj = bs(resp.text, 'lxml')
            # NotOnSale = ('On sale on' in resp.text or 'Currently unavailable' in resp.text)
            script = obj.select_one('script[type="application/ld+json"]')
            if script is not None:
                try:
                    result = json.loads(script.string)
                except json.JSONDecodeError:
                    return None
            else:
                result = dict()
            if isinstance(result, list):
                result = result[0]
            if 'offers' not in result.keys():
                # try to parse tickets info from javascript code
                currency = ''
                prices = []
                for script in obj.select('script'):
                    cur_match = re.findall(r"'currencyCode': '(.+)'", script.text)
                    if len(cur_match):
                        currency = cur_match[0]
                    if 'initialiseSeatSelector()' in script.text:
                        prices_matches = re.findall(r"prices.push\(([\s\S]*?)\);", script.text, re.MULTILINE)
                        for price_txt in prices_matches:
                            try:
                                price_obj = hjson.loads(price_txt)
                            except json.JSONDecodeError:
                                return None
                            if 'offers' not in result.keys():
                                result['offers'] = []
                            try:
                                p_name = price_obj['seatingBlocks'][0]['name']
                            except KeyError:
                                try:
                                    p_name = price_obj['label']
                                except KeyError:
                                    p_name = ''
                            p_price = price_obj['ticketPrice']
                            p_availability = 'InStock' if price_obj['maximumQuantity'] > 0 else 'SoldOut'
                            result['offers'].append({
                                'name': p_name,
                                'price': p_price,
                                'availability': p_availability
                            })
                if 'offers' not in result.keys():
                    for script in obj.select('script'):
                        prices_matches = re.findall(r"window.dataLayer.push\(([\s\S]*?)\);", script.text, re.MULTILINE)
                        if prices_matches:
                            if 'offers' not in result.keys():
                                result['offers'] = []
                            try:
                                price_obj = hjson.loads(prices_matches[0])
                            except json.JSONDecodeError:
                                return None
                            try:
                                currency = price_obj['currencyCode']
                                for p in price_obj['ecommerce']['detail']['products']:
                                    result['offers'].append({
                                        'name': p['name'],
                                        'price': p['price'],
                                        'availability': 'InStock',
                                    })
                            except KeyError:
                                pass

                if 'offers' in result.keys():
                    for o in result['offers']:
                        o['priceCurrency'] = currency
            if 'offers' in result.keys() and result.get('eventStatus') == 'EventScheduled':
                sold_out = True
                for o in result['offers']:
                    if o.get('availability') != 'SoldOut':
                        sold_out = False
                        break
                result['soldOut'] = sold_out
            else:
                result['soldOut'] = False
            # result['NotOnSale'] = NotOnSale
            self.events_parsed += 1
            return result

    def exec_time(self):
        return (dt.utcnow() - self.starting_time).total_seconds()
