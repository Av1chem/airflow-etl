from airflow.models.variable import Variable
from additional.tools.config_helper import ConfigReader

try:
    config_name = Variable.get('SEETICKETS_UK_CONFIG_NAME')
except KeyError:
    config_name = 'SEETICKETS_UK'

URL_TEMPLATE = 'https://www.seetickets.com/search/{}?BrowseOrder=relevance&q=&se=on&SearchRange=AnyTime&dst=&dend=&l='

with ConfigReader(config_name) as cr:

    CONFIG_NAME = config_name
    MANUAL_CYCLE_ID = cr.read_or_default('manual_cycle_id', None)

    TABLE = cr.read_or_default('table', 'seetickets_uk')
    SQL_BATCH_SIZE = cr.read_or_default('sql_batch_size', 100)

    GET_RETRIES = cr.read_or_default('get_retries', 5)
    GET_SCRIPT_RETRIES = cr.read_or_default('get_script_retries', 3)
    BLANC_PAGES_LIMIT = cr.read_or_default('blanc_pages_limit', 3)
    RETRY_INTERVAL = cr.read_or_default('retry_interval', 5)

    EXPORT_TO_CSV = cr.read_or_default('export_to_csv', True)
    EXPORT_RECORDS_THRESHOLD = cr.read_or_default('export_records_threshold', 10)
