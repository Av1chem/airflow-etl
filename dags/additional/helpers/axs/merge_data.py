import json
from random import shuffle

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.hooks.postgres_hook import PostgresHook

from additional.tools import sql_util
from additional.tools.database_mixin import DatabaseMixin

from additional.helpers.axs.axs_config import *


class MergeData(BaseOperator, DatabaseMixin):
    """

    """

    template_fields = ('cycle_id', )
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cycle_id = cycle_id
        if isinstance(self.cycle_id, tuple):
            self.cycle_id = self.cycle_id[0]
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)

    def execute(self, context):

        self.cleanup_results(VENUES_TABLE)
        self.cleanup_results(EVENTS_TABLE)

        # pull events from XCom
        all_events = []
        all_venues = []
        for i in range(NUM_WORKERS_STEP_1):
            task_id = f'pull_venues_{i}'
            try:
                those_events = json.loads(context['task_instance'].xcom_pull(task_ids=task_id, key=f"events"))
                all_events += those_events
                those_venues = json.loads(context['task_instance'].xcom_pull(task_ids=task_id, key=f"venues"))
                all_venues += those_venues
            except:
                continue

        # dedup data - just in case
        self.log.info(f"Venues raw: {len(all_venues)}")
        all_venues = [
            next(v for v in all_venues if v['id'] == v_id) for v_id in set(e['id'] for e in all_venues)
        ]
        self.log.info(f"Venues unique: {len(all_venues)}")

        self.log.info(f"Events raw: {len(all_events)}")
        all_events = [
            next(v for v in all_events if v['event_data']['eventId'] == v_id)
            for v_id in set(e['event_data']['eventId'] for e in all_events)
        ]
        self.log.info(f"Events unique: {len(all_events)}")

        shuffle(all_events)
        shuffle(all_venues)

        # store results to DB
        if SAVE_TO_DB:
            for table, data in zip([VENUES_TABLE, EVENTS_TABLE], [all_venues, all_events]):
                for start_idx in range(0, len(data), SQL_BATCH_SIZE):
                    cur_chunk = data[start_idx:start_idx+SQL_BATCH_SIZE]
                    tuples_to_write = []
                    for p in cur_chunk:
                        tuples_to_write.append((
                            self.cycle_id, json.dumps(p)
                        ))
                    if tuples_to_write:
                        with self.hook.get_conn() as conn:
                            self.log.info(f"DB: inserting {len(tuples_to_write)} {table}.")
                            sql_util.insert_and_commit(
                                conn, self.log, table, ('cycle_id', 'data'), tuples_to_write,
                                verbose=False
                            )
                            self.log.info(f"DB: inserted successfully.")
