import json
import random
from airflow.utils.decorators import apply_defaults

from additional.helpers.axs.axs_config import *
from additional.operators.anti_distill_operator import AntiDistillBrowser


class GetVenues(AntiDistillBrowser):
    """

    """

    template_fields = ()
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            *args, **kwargs):
        super().__init__(*args, **kwargs)

    def execute(self, context):

        # get venues
        self.get_browser()
        parsed = self.parse_page('https://www.axs.com/venues#')
        venues_to_parse = []
        for l in parsed.select('.main-content-panel a[href*="https://www.axs.com/"]'):
            events_num = l.select_one('span').text.replace(')', '').replace('(', '').strip()
            if events_num:
                events_num = int(events_num)
                if events_num >= MIN_EVENTS:
                    venues_to_parse.append((l.attrs['href'], events_num))
        self.log.info(f"Got {len(venues_to_parse)} venues with events >= {MIN_EVENTS}.")

        random.shuffle(venues_to_parse)

        # split venues between workers
        venues_count = len(venues_to_parse)
        if venues_count <= NUM_WORKERS_STEP_1:
            partitions = {
                i: [i, i] for i in range(venues_count)
            }
            partitions.update({
                i: None for i in range(venues_count, NUM_WORKERS_STEP_1)
            })
        else:
            step = venues_count / NUM_WORKERS_STEP_1
            start_idx = 0
            end_idx = 0
            partitions = {}
            for i in range(NUM_WORKERS_STEP_1):
                end_idx = min(start_idx + step, venues_count)
                partitions[i] = [int(start_idx // 1), int(end_idx // 1)]
                start_idx = end_idx
            if partitions:
                partitions[NUM_WORKERS_STEP_1 - 1][-1] = venues_count
        self.log.info(f"Broke configs into following partitions: {partitions}")

        venues_partitions = {}
        for task in partitions:
            if partitions[task] is None:
                venues_partitions[task] = "[]"
            else:
                venues_partitions[task] = json.dumps(venues_to_parse[partitions[task][0]: partitions[task][1] + 1])
        self.log.info(f'Pushing partitions to the context')

        context['task_instance'].xcom_push(key="venues_partition", value=venues_partitions)
