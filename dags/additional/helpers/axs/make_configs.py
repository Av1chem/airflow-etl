import json
from random import shuffle
from itertools import product
from collections import OrderedDict
import requests
from bs4 import BeautifulSoup as Bs

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from additional.helpers.axs.axs_config import NUM_WORKERS_STEP_1


class MakeConfigs(BaseOperator):
    """
    """

    template_fields = ()
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            *args, **kwargs):
        super().__init__(*args, **kwargs)

    def execute(self, context):
        url = "https://www.axs.com/venues"

        payload = {}
        headers = OrderedDict([
            ('Connection', 'keep-alive'),
            ('Pragma', 'no-cache'),
            ('Cache-Control', 'no-cache'),
            ('Upgrade-Insecure-Requests', '1'),
            ('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36'),
            ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'),
            ('Sec-Fetch-Site', 'same-origin'),
            ('Sec-Fetch-Mode', 'navigate'),
            ('Sec-Fetch-Dest', 'document'),
            ('sec-ch-ua', '"Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"'),
            ('sec-ch-ua-mobile', '?0'),
            ('Accept-Language', 'ru-RU,ru;q=0.9'),
            ('Cookie', 'DG_ZID=06AAF9EC-0600-3949-991F-7E02C40F36E2; DG_ZUID=957093B6-54E0-3263-9110-11596B22131D; DG_HID=99280A24-45BB-37F7-AD44-F47D37728101; PHPSESSID=nmbrqkivg6s6ifh8trrtcog7ba; preferred_locale=en-US; device_view=full; at_check=true; AMCVS_B7B972315A1341150A495EFE%40AdobeOrg=1; axs_consent_to_write_cookies=false; s_promotor=unknown; s_cc=true; _ga=GA1.2.2126605194.1614698135; _gid=GA1.2.1300785153.1614698135; AMCV_B7B972315A1341150A495EFE%40AdobeOrg=-432600572%7CMCIDTS%7C18689%7CMCMID%7C22896677352281372910604213198145243951%7CMCAAMLH-1615302931%7C9%7CMCAAMB-1615302931%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1614705331s%7CNONE%7CMCSYNCSOP%7C411-18696%7CvVersion%7C4.5.2; _ctpuid=3a90307d-789f-4c01-88f3-5ffd76d874c4; _fbp=fb.1.1614698135452.1439939732; __qca=P0-572496454-1614698139033; __gads=ID=b0d405c87aa7c21a-22097dbdbfc600b9:T=1614698145:S=ALNI_MaU_RhZDBojcnYqCwveeRJW7toaTg; wfx_unq=HAYGPOVh9eVInYZ3; axs_geo_static_v1=CA%3A%3AToronto%3A%3A0%3A43.6547%3A-79.3623%3AAmerica%2FLos_Angeles%3A198; axs_geo_v1=CA%3A%3AToronto%3A%3A0%3A43.6547%3A-79.3623%3AAmerica%2FLos_Angeles%3A198; _admrla=2.0-b1787764-2bd7-3240-c60d-96cb04d21974; DG_SID=143.110.219.120:5QLgoctQRDoB6DY9GgCNpHBC6S9Brxxhjki9qZuA3VM; mbox=session#79962f94c09045be92f93ebeed52337a#1614699990|PC#79962f94c09045be92f93ebeed52337a.35_0#1677943805; gpv_c7=no%20value; _awl=2.1614699011.0.4-7d1885de-b17877642bd73240c60d96cb04d21974-6763652d75732d63656e7472616c31-603e5a03-0; gpv_pn=venues; s_gnr7=1614699691741-New; s_gnr30=1614699691741-New; s_sq=aegaxsprod%252Caegaxsonly%252Caegaxsukglobal%3D%2526c.%2526a.%2526activitymap.%2526page%253Dvenues%2526link%253D1015%252520Folsom%2526region%253Dpage-relative-block%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dvenues%2526pidt%253D1%2526oid%253Dhttps%25253A%25252F%25252Fwww.axs.com%25252Fvenues%25252F125088%25252F1015-folsom-san-francisco-tickets%2526ot%253DA; DG_IID=9F7010BA-008B-3FB5-8DFE-ECF6B08A7A88; DG_UID=3D0CBF49-4244-3225-A4BB-A2EB5DB842DB; PHPSESSID=nmbrqkivg6s6ifh8trrtcog7ba')
        ])

        response = requests.request("GET", url, headers=headers, data=payload)
        a = 1
        venue_page = requests.get(
            'https://www.axs.com/venues#'
        )
        obj = Bs(venue_page.content, 'lxml')

        links = [
            a.attrs['href'] for a in obj.select('.main-content-panel a[href*="venues"]')
        ]
        a = 1

        # split configs among workers
        # configs_count = len(cities_dedup)
        # if configs_count <= NUM_WORKERS_STEP_1:
        #     partitions = {
        #         i: [i, i] for i in range(configs_count)
        #     }
        #     partitions.update({
        #         i: None for i in range(configs_count, NUM_WORKERS_STEP_1)
        #     })
        # else:
        #     step = configs_count / NUM_WORKERS_STEP_1
        #     start_idx = 0
        #     end_idx = 0
        #     partitions = {}
        #     for i in range(NUM_WORKERS_STEP_1):
        #         end_idx = min(start_idx + step, configs_count)
        #         partitions[i] = [int(start_idx // 1), int(end_idx // 1)]
        #         start_idx = end_idx
        #     if partitions:
        #         partitions[NUM_WORKERS_STEP_1 - 1][-1] = configs_count
        # self.log.info(f"Broke configs into following partitions: {partitions}")
        #
        # state_partitions = {}
        # for task in partitions:
        #     if partitions[task] is None:
        #         state_partitions[task] = "[]"
        #     else:
        #         state_partitions[task] = json.dumps(cities_dedup[partitions[task][0]: partitions[task][1] + 1])
        # self.log.info(f'Pushing partitions to the context')
        #
        # context['task_instance'].xcom_push(key="configs", value=state_partitions)
y