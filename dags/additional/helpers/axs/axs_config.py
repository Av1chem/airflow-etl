from airflow.models.variable import Variable
from additional.tools.config_helper import ConfigReader
from additional.tools.browser_config_model import *

try:
    config_name = Variable.get('AXS_CONFIG_NAME')
except KeyError:
    config_name = 'AXS_CONFIG'

B_CONFIGS = ConfigList(
    BrowserConfig(
                0, 90,
                None,
                ScreenConfig(
                    ScreenSizeConfig(1920, 1080),
                    24
                ),
                'en,en_US', True,
                ['http://lumtest.com/myip.json', 'https://coveryourtracks.eff.org/']
    ),

API_URL_TEMPLATE = "https://www.axs.com/venueEvents?id={venue_id}&rangeIndex=&fromIndex=0&page=1&rows=1000"

with ConfigReader(config_name) as cr:

    CONFIG_NAME = config_name

    AIRFLOW_POOL_NAME = cr.read_or_default('airflow_pool', 'default_pool')
    MANUAL_CYCLE_ID = cr.read_or_default('manual_cycle_id', None)
    IS_TEST_MODE = cr.read_or_default('is_test_mode', False)

    RETRIES = cr.read_or_default('retries', 0)
    NUM_WORKERS_STEP_1 = cr.read_or_default('num_workers_step_1', 1)

    DELAY_BETWEEN_REQUESTS = cr.read_or_default('delay_between_requests', 30)
    MIN_EVENTS = cr.read_or_default('min_events', 1)
    NEW_IDENTITY_AFTER = cr.read_or_default('new_identity_after', 50)
    PUSHING_DATA_TO_XCOM_INTERVAL = cr.read_or_default('pushing_data_to_xcom_interval', 5)
    GET_SESSION_RETRIES = cr.read_or_default('get_session_retries', 3)
    GET_SESSION_RETRY_DELAY = cr.read_or_default('get_session_retry_delay', 120)

    VENUES_TABLE = cr.read_or_default('venues_table', 'axs_venues')
    EVENTS_TABLE = cr.read_or_default('events_table', 'axs_events')
    SQL_BATCH_SIZE = cr.read_or_default('sql_batch_size', 1000)
    SAVE_TO_DB = cr.read_or_default('save_to_db', True)
    PREFIX = cr.read_or_default('prefix', 'axs')

    VENUES_EXPORT_TO_CSV = cr.read_or_default('venues_export_to_csv', True)
    EVENTS_EXPORT_TO_CSV = cr.read_or_default('events_export_to_csv', True)

    EVENT_RECORDS_THRESHOLD = cr.read_or_default('event_records_threshold', 1000)
    VENUE_RECORDS_THRESHOLD = cr.read_or_default('venue_records_threshold', 500)
    PROXY = cr.read_or_default(
        'proxy',
        {
        	"secret": "sec"
        }
    )
