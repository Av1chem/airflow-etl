import ast
import json
import time
import random
import requests
from bs4 import BeautifulSoup as Bs
import re
import csv
from datetime import datetime as dt, timedelta
from airflow.utils.decorators import apply_defaults

from additional.helpers.axs.axs_config import *
from additional.operators.anti_distill_operator import AntiDistillBrowser


class PullVenues(AntiDistillBrowser):
    """
    """

    template_fields = ()
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            task_idx,
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.task_idx = task_idx

    def execute(self, context):

        v_partitions = context['task_instance'].xcom_pull(task_ids="get_venues", key=f"venues_partition")
        venues = v_partitions[self.task_idx if self.task_idx in v_partitions else str(self.task_idx)]
        if not venues:
            self.log.error("No venues found. Exiting...")
            return

        venues = ast.literal_eval(venues)
        random.shuffle(venues)

        venues_count = len(venues)
        self.log.info(f"Got venues type={type(venues)}, count={venues_count}")

        these_events = []
        these_venues = []

        # pull data from venue pages
        for v_idx, venue in enumerate(venues):
            if v_idx % NEW_IDENTITY_AFTER == 0:
                self.get_browser()
                self.parse_page(venue[0])

            venue_id = venue[0].split('venues/')[1].split('/')[0]
            api_url = API_URL_TEMPLATE.format(venue_id=venue_id)
            for attempt in range(10):
                try:
                    response = self.parse_page(api_url, as_json=True)
                    break
                except:
                    self.log.error(f"Attempt # {attempt+1}: error when pulling data from {api_url}")
                    time.sleep(1)
                    continue
            if 'pageVars' in response:
                these_venues.append(response['pageVars'])
            if 'pageVars' in response and 'allEvents' in response:
                venue_events = [
                    {'event_data': e, 'venue_data': response['pageVars']} for e in response['allEvents']
                ]

                # pulling onSale data
                event_count = len(venue_events)
                # for e_idx, e in enumerate(venue_events):
                #     e['event_data']['isExternallyTicketed'] = True
                #     if 'ticketCTAHref' in e['event_data']:
                #         msg_tmp = f"Venue # {v_idx+1} of {venues_count}, event # {e_idx + 1} of {event_count}:"
                #         if 'tix.axs.com' in e['event_data']['ticketCTAHref']:
                #             # North America API - 1st variant
                #             on_sale_id = e['event_data']['ticketCTAHref']\
                #                           [e['event_data']['ticketCTAHref'].rfind('/') + 1:]
                #             if not hasattr(self, '_onsale_id_us'):
                #                 self._onsale_id_us = on_sale_id
                #             resp = requests.get(
                #                 f'https://unifiedapicommerce.us-prod0.axs.com/veritix/onsale/v2/{on_sale_id}'
                #                 f'?sessionID={self.get_session_us()}&locale=en-GB'
                #             )
                #         elif 'shop.axs.com' in e['event_data']['ticketCTAHref']:
                #             # North America API - 2nd variant
                #             resp = requests.get(e['event_data']['ticketCTAHref'])
                #             r_idx = resp.url.rfind('?')
                #             if r_idx <= 0:
                #                 r_idx = None
                #             on_sale_id = resp.url[resp.url.rfind('/') + 1:r_idx]
                #             if not hasattr(self, '_onsale_id_us'):
                #                 self._onsale_id_us = on_sale_id
                #             resp = requests.get(
                #                 f'https://unifiedapicommerce.us-prod0.axs.com/veritix/onsale/v2/{on_sale_id}'
                #                 f'?sessionID={self.get_session_us()}&locale=en-GB'
                #             )
                #         elif 'q.axs.co.uk' in e['event_data']['ticketCTAHref']:
                #             # UK API - 1st variant
                #             resp = requests.get(e['event_data']['ticketCTAHref'])
                #             r_idx =resp.url.rfind('?')
                #             if r_idx <= 0:
                #                 r_idx = None
                #             on_sale_id = resp.url[resp.url.rfind('/') + 1:r_idx]
                #             if not hasattr(self, '_onsale_id_uk'):
                #                 self._onsale_id_uk = on_sale_id
                #             resp = requests.get(
                #                 f'https://unifiedapicommerce.axs.co.uk/veritix/onsale/v2/{on_sale_id}'
                #                 f'?sessionID={self.get_session_uk()}&locale=en-GB'
                #             )
                #         elif 'shop.axs.co.uk' in e['event_data']['ticketCTAHref']:
                #             # UK API - 2st variant
                #             on_sale_id = e['event_data']['ticketCTAHref'][
                #                          e['event_data']['ticketCTAHref'].rfind('/') + 1:]
                #             if not hasattr(self, '_onsale_id_uk'):
                #                 self._onsale_id_uk = on_sale_id
                #             resp = requests.get(
                #                 f'https://unifiedapicommerce.axs.co.uk/veritix/onsale/v2/{on_sale_id}'
                #                 f'?sessionID={self.get_session_uk()}&locale=en-GB'
                #             )
                #         else:
                #             self.log.warning(
                #                 f"{msg_tmp} Can't find appropriate target for url={e['event_data']['ticketCTAHref']}")
                #             continue
                #     try:
                #         parsed = resp.json()
                #         if 'onsaleInformation' not in parsed:
                #             self.log.error(
                #                 f"{msg_tmp} Unknown response model parsed={parsed}")
                #             continue
                #         o_date = {
                #             'start_date': parsed['onsaleInformation'].get('startDate'),
                #             'end_date': parsed['onsaleInformation'].get('endDate')
                #         }
                #
                #         if o_date['start_date'] or o_date['end_date']:
                #             e['event_data']['on_sale_date'] = o_date
                #             self.log.info(f"{msg_tmp} got on_sale={o_date}")
                #             e['event_data']['isExternallyTicketed'] = False
                #         else:
                #             self.log.warning(f"{msg_tmp} no onsale in response: {parsed}")
                #
                #     except json.JSONDecodeError:
                #         self.log.error(f"{msg_tmp} no JSON in response", exc_info=True)
                #         continue

                these_events += venue_events

            self.log.info(f"Venue # {v_idx+1} of {venues_count} parsed. "
                          f"Events total: {len(these_events)}. Venue: {venue}.")
            if v_idx > 0 and v_idx % PUSHING_DATA_TO_XCOM_INTERVAL == 0:
                self.log.info(f"Autosave to XCom, {len(these_events)} events, {len(these_venues)} venues.")
                context['task_instance'].xcom_push(key="events", value=json.dumps(these_events))
                context['task_instance'].xcom_push(key="venues", value=json.dumps(these_venues))

        self.log.info(f"Pulling finished, now pushing data to XCom, "
                      f"{len(these_events)} events, {len(these_venues)} venues.")
        context['task_instance'].xcom_push(key="events", value=json.dumps(these_events))
        context['task_instance'].xcom_push(key="venues", value=json.dumps(these_venues))

    def get_session_us(self):
        if not hasattr(self, '_session_id_us') or dt.utcnow() > self._session_expire_us:
            exc = None
            for ret in range(GET_SESSION_RETRIES):
                try:
                    onsale_id_hardcoded = 'txWKDwAAAAB3u1J0AAAAAAAH%2fv%2f%2f%2fwD%2f%2f%2f%2f%2fBXRoZW1lAP%2f%2f%2f%2f%2f%2f%2f%2f%2f%2f'
                    resp = requests.post(
                        f'https://unifiedapicommerce.us-prod0.axs.com/veritix/session/v2/{onsale_id_hardcoded}',
                        proxies={
                            'http':'http://lum-customer-c_154c57cc-zone-ticketek_au-ip-191.101.143.204:qbod1kbqv9i8@zproxy.lum-superproxy.io:22225',
                            'https': 'http://lum-customer-c_154c57cc-zone-ticketek_au-ip-191.101.143.204:qbod1kbqv9i8@zproxy.lum-superproxy.io:22225'
                        }
                    )
                    self._session_id_us = resp.json()['sessionID']
                    self._session_expire_us = dt.utcnow() + timedelta(seconds=600)
                    self.log.info(f"Got new US sessionId={self._session_id_us}")
                    return
                except Exception as ex:
                    exc = ex
                    self.log.warning(f"Failed at getting US session, retry # {ret + 1} of {GET_SESSION_RETRIES}, "
                                     f"now sleeping for {GET_SESSION_RETRY_DELAY}s.", exc_info=True)
                    self.log.warning(f"resp={resp.text}")
                    time.sleep(GET_SESSION_RETRY_DELAY)

            if exc is not None:
                raise exc

    def get_session_uk(self):
        if not hasattr(self, '_session_id_uk') or dt.utcnow() > self._session_expire_uk:
            exc = None
            for ret in range(GET_SESSION_RETRIES):
                try:
                    resp = requests.post(
                        f'https://unifiedapicommerce.axs.co.uk/veritix/session/v2/{self._onsale_id_uk}')
                    self._session_id_uk = resp.json()['sessionID']
                    self._session_expire_uk = dt.utcnow() + timedelta(seconds=600)
                    self.log.info(f"Got new UK sessionId={self._session_id_uk}")
                    return
                except Exception as ex:
                    exc = ex
                    self.log.warning(f"Failed at getting UK session, retry # {ret+1} of {GET_SESSION_RETRIES}, "
                                     f"now sleeping for {GET_SESSION_RETRY_DELAY}s.", exc_info=True)
                    time.sleep(GET_SESSION_RETRY_DELAY)

            if exc is not None:
                raise exc
