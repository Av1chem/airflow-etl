from airflow.models.variable import Variable
from additional.tools.config_helper import ConfigReader

try:
    config_name = Variable.get('HOLDMYTICKET_CONFIG_NAME')
except KeyError:
    config_name = 'HOLDMYTICKET'


with ConfigReader(config_name) as cr:

    CONFIG_NAME = config_name
    MANUAL_CYCLE_ID = cr.read_or_default('manual_cycle_id', None)

    TABLE = cr.read_or_default('table', 'holdmyticket')
    SQL_BATCH_SIZE = cr.read_or_default('sql_batch_size', 1000)

    EXPORT_TO_CSV = cr.read_or_default('export_to_csv', True)
    EXPORT_RECORDS_THRESHOLD = cr.read_or_default('export_records_threshold', 10)
