from json import loads
import json
from time import sleep
import random
from datetime import datetime as dt, timedelta

from requests import get
from bs4 import BeautifulSoup as bs

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.hooks.postgres_hook import PostgresHook

from additional.tools import sql_util
from additional.helpers.holdmyticket.holdmyticket_config import *
from additional.tools.database_mixin import DatabaseMixin


class PullEvents(BaseOperator, DatabaseMixin):
    """
    """

    template_fields = ('cycle_id', )
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.cycle_id = cycle_id
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)

    def execute(self, context):

        starting_time = dt.utcnow()
        self.cleanup_results(TABLE)

        LIST = 'https://holdmyticket.com/api/public/events/nearby/location/Albuquerque%2C%20NM' \
               '/accuracy/Any/api_key/anon/page/{}/type/event'
        SHOWS = 'https://holdmyticket.com/api/public/events/nearby/location/Los%20Angeles%2C%20CA' \
                '/accuracy/Any/api_key/anon/page/{}/type/multi'
        REPEATING = 'https://holdmyticket.com/api/public/events/repeating/id/{}'
        EVENT = 'https://holdmyticket.com/api/shop/events/get/{}'
        LATENCY = timedelta(seconds=1)

        # pull events list from API
        events = []
        for i in range(999999):
            self.log.info('Get page {}'.format(i))
            resp = loads(get(LIST.format(i)).text)
            if not len(resp['events']):
                break
            else:
                for e in resp['events']:
                    e['type'] = 'event'
                events += resp['events']

        # pull shows list from API
        shows = []
        for i in range(999999):
            self.log.info('Get shows page {}'.format(i))
            resp = loads(get(SHOWS.format(i)).text)
            if not len(resp['events']):
                break
            else:
                for s in resp['events']:
                    self.log.info('Get show details {0}'.format(s['id']))
                    show = loads(get(REPEATING.format(s['id'])).text)
                    for se in show['events']:
                        a = 1
                        se['venue_address1'] = s['venue_address1']
                        se['venue_address2'] = s['venue_address2']
                        se['venue_city'] = s['venue_city']
                        se['venue_state'] = s['venue_state']
                        se['type'] = 'show'
                        events.append(se)

        # get ticket details for each event
        for e in events:
            last_request_at = dt.utcnow()
            e['tickets'] = loads(get(EVENT.format(e['id'])).text)['event']['tickets']
            self.log.info('Details for event: {}'.format(e['id']))
            e['ticket_details'] = ''
            e['min_price'] = 100000000
            e['max_price'] = 0
            e['sold_out'] = True
            e['has_unavailable_tickets'] = ''
            for t in e['tickets']:
                if float(t['price']) < e['min_price']:
                    e['min_price'] = float(t['price'])
                if float(t['price']) > e['max_price']:
                    e['max_price'] = float(t['price'])
                if not len(t['sold_out']):
                    if t['max_qty']:
                        availability = 'AVAILABLE'
                        e['sold_out'] = False
                    else:
                        availability = 'UNAVAILABLE'
                        e['has_unavailable_tickets'] = 'Yes'
                    e['ticket_details'] += '{0} {1} ({2}) / '.format(t['description'], '$' + str(t['price']),
                                                                     availability)
                else:
                    e['ticket_details'] += '{0} {1} ({2}) / '.format(t['description'], '$' + str(t['price']),
                                                                     'SOLD OUT')
            if len(e['ticket_details']):
                e['ticket_details'] = e['ticket_details'][:-3]
            else:
                e['min_price'] = 0
                e['max_price'] = 0
                e['sold_out'] = False

            # avoid blocking
            time_from_last_request = dt.utcnow() - last_request_at
            to_sleep = (LATENCY - time_from_last_request).total_seconds()
            if to_sleep > 0:
                sleep(to_sleep)

        self.log.info(
            'parsing finished. Execution time: {0:.2f} sec'.format((dt.utcnow() - starting_time).total_seconds()))

        self._write_events(events)

    def _write_events(self, events):

        for s_i in range(0, len(events), SQL_BATCH_SIZE):
            self.log.info(f"DB: writing batch of size {len(events[s_i:s_i+SQL_BATCH_SIZE])}.")
            tuple_to_write = [
                (self.cycle_id, json.dumps(e)) for e in events[s_i:s_i+SQL_BATCH_SIZE]
            ]
            if tuple_to_write:
                with self.hook.get_conn() as conn:
                    self.log.info(f"DB: inserting {len(tuple_to_write)} events.")
                    sql_util.insert_and_commit(
                        conn, self.log, TABLE, ('cycle_id', 'data'), tuple_to_write,
                        verbose=False
                    )
                    self.log.info(f"DB: inserted successfully.")
