from airflow.models.variable import Variable
from additional.tools.config_helper import ConfigReader

try:
    config_name = Variable.get('MAINTENANCE_CONFIG_NAME')
except KeyError:
    config_name = 'MAINTENANCE_CONFIG'

with ConfigReader(config_name) as cr:

    CONFIG_NAME = config_name
    DB_MAX_AGE_DAYS = cr.read_or_default('db_max_age_days', 7)
    DB_TABLES = cr.read_or_default(
        'db_tables',
        [
            'axs_events',
            'axs_venues',
            'etix_final',
            'eventbrite_events_final',
            'eventbrite_events_raw',
            'fgt',
            'holdmyticket',
            'seetickets_us',
            'seetickets_uk',
            'ticketek_au',
            'ticketweb'
        ]
    )
    S3_MAX_AGE_DAYS = cr.read_or_default('s3_max_age_days', 7)
