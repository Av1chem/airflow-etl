from airflow.models.variable import Variable
from additional.tools.config_helper import ConfigReader

try:
    config_name = Variable.get('CUSTOM_EXPORT_CONFIG_NAME')
except KeyError:
    config_name = 'CUSTOM_EXPORT_CONFIG'

with ConfigReader(config_name) as cr:

    CONFIG_NAME = config_name
    TABLE = cr.read_or_default('table', '')
    FILENAME = cr.read_or_default('filename', 'custom_export')
    CLAUSE = cr.read_or_default('clause', 'true')
