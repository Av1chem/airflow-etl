import json
import random
from copy import deepcopy
from datetime import datetime as dt, timedelta

import requests

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.hooks.postgres_hook import PostgresHook

from additional.tools import sql_util
from additional.helpers.eventbrite_events.eventbrite_events_config import *
from additional.tools.database_mixin import DatabaseMixin


class PullEvents(BaseOperator, DatabaseMixin):
    """
    """

    template_fields = ('cycle_id', )
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.cycle_id = cycle_id
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)
        self._events_inserted = 0
        self._events_in_area = 0
        self._api_url = "https://www.eventbrite.com/api/v3/destination/search/"
        self._payload = deepcopy(PAYLOAD)
        self._categories = [c.strip().lower() for c in CATEGORIES] if CATEGORIES else None

    def execute(self, context):

        self.cleanup_results(EVENTS_RAW_TABLE)

        quadrants = []
        sd = dt.utcnow().date() - timedelta(days=1)
        ed = sd + timedelta(days=PERIOD_DAYS + 1)

        for b_idx, bbox in enumerate(BBOXES):

            self.log.info(f"Checking bbox # {b_idx+1} of {len(BBOXES)}.")

            # check if any events in these areas
            q_all = (bbox[2] - bbox[0]) * (bbox[3] - bbox[1])
            i = 1
            for lon in range(bbox[0], bbox[2]):
                for lat in range(bbox[1], bbox[3]):

                    self._get_csrf()

                    cur_bbox = '.0,'.join([str(lon), str(lat), str(lon + 1), str(lat + 1)]) + '.0'
                    cur_payload = self._payload.replace('{bbox}', cur_bbox) \
                        .replace('{from}', dt.strftime(sd, '%Y-%m-%d')) \
                        .replace('{to}', dt.strftime(ed, '%Y-%m-%d')) \
                        .replace('{page}', '1')
                    resp = requests.post(self._api_url, headers=self._get_headers(), data=cur_payload)
                    if resp.status_code == 200:
                        if json.loads(resp.text)['events']['pagination']['object_count']:
                            quadrants.append([lon, lat])
                    else:
                        self.log.error('SOMETHING WENT WRONG!!!')
                    self.log.info(
                        'Checking quadrants: {0} of {1}. Valuable: {2}'.format(i, q_all, len(quadrants))
                    )
                    i += 1

        random.shuffle(quadrants)

        # pull events in ranges
        cur_q_idx = 0
        for cur_q_idx, q in enumerate(quadrants):
            self._write_events(
                self._pull_events_in_range(0, q, cur_q_idx, len(quadrants), (sd, ed)), True
            )
            self.log.info(f"q_# {cur_q_idx+1} of {len(quadrants)}\t\t Events total: {self._events_inserted}")

    def _pull_events_in_range(self, r_l, quadrant, q_idx, q_count, date_range):

        self._get_csrf()

        lon = quadrant[0]
        lat = quadrant[1]
        sd = date_range[0]
        ed = date_range[1]
        period = (ed - sd).days
        cur_bbox = '.0,'.join([str(lon), str(lat), str(lon + 1), str(lat + 1)]) + '.0'

        # get count
        cur_payload = self._payload.replace('{bbox}', cur_bbox) \
            .replace('{from}', dt.strftime(sd, '%Y-%m-%d')) \
            .replace('{to}', dt.strftime(ed, '%Y-%m-%d')) \
            .replace('{page}', '1')
        resp = requests.request("POST", self._api_url, headers=self._get_headers(), data=cur_payload)

        if resp.status_code == 200:
            cont = json.loads(resp.text)

            split_period = False

            if cont['events']['pagination']['object_count'] > 1000:
                log_message = \
                    '{0}\t{1} to {2}\tr_l {3}\tq_# {4} of {5}\t\tToo much ({6}) events! Splitting range...'.format(
                        cur_bbox,
                        dt.strftime(sd, '%Y-%m-%d'),
                        dt.strftime(ed, '%Y-%m-%d'),
                        r_l,
                        q_idx + 1,
                        q_count,
                        cont['events']['pagination']['object_count']
                    )
                self.log.warning(log_message)
                split_period = True

            else:

                log_message = '{0}\t{1} to {2}\tr_l {3}\tq_# {4} of {5}\t\t{6} events.'.format(
                    cur_bbox,
                    dt.strftime(sd, '%Y-%m-%d'),
                    dt.strftime(ed, '%Y-%m-%d'),
                    r_l,
                    q_idx + 1,
                    q_count,
                    cont['events']['pagination']['object_count']
                )
                self.log.info(log_message)

            if split_period and period >= 2:
                ed0 = sd + timedelta(days=period // 2)
                sd1 = ed0 + timedelta(days=1)
                events = self._pull_events_in_range(r_l + 1, quadrant, q_idx, q_count, (sd, ed0))
                events += self._pull_events_in_range(r_l + 1, quadrant, q_idx, q_count, (sd1, ed))
            else:
                if split_period:
                    self.log.error(f"Too much ({cont['events']['pagination']['object_count']}) events on date={sd}")

                if not cont.get('events') or not cont['events'].get('results'):

                    self.log.error(f"Got unexpected response: {cont}")
                    if 'events' not in locals():
                        events = []

                else:

                    events = self._filter_by_categories(cont['events']['results'])
                    for page_idx in range(2, 21):
                        cur_payload = self._payload.replace('{bbox}', cur_bbox) \
                            .replace('{from}', dt.strftime(sd, '%Y-%m-%d')) \
                            .replace('{to}', dt.strftime(ed, '%Y-%m-%d')) \
                            .replace('{page}', str(page_idx))
                        resp = requests.request("POST", self._api_url, headers=self._get_headers(), data=cur_payload)
                        cont = json.loads(resp.text)

                        if not cont.get('events'):
                            # error
                            self.log.error(f"Got unexpected response: {cont}")
                            break

                        if not cont['events'].get('results'):
                            # last page
                            break

                        events += self._filter_by_categories(cont['events']['results'])

            if r_l == 0:
                self.log.info(f"Events in this area = {self._events_in_area + len(events)}")

            if len(events) >= SQL_BATCH_SIZE:
                self._write_events(events)
                return []
            else:
                return events

    def _write_events(self, events, end_of_area=False):

        for e in events:
            if e.get('tags'):
                e['tags'] = sorted(e['tags'], key=lambda k: f"{k.get('prefix')} {k.get('tag')}")

        for s_i in range(0, len(events), SQL_BATCH_SIZE):
            self.log.info(f"DB: writing batch of size {len(events[s_i:s_i+SQL_BATCH_SIZE])}.")
            tuple_to_write = [
                (self.cycle_id, json.dumps(e), str(e.get('id'))) for e in events[s_i:s_i+SQL_BATCH_SIZE]
            ]
            if tuple_to_write:
                with self.hook.get_conn() as conn:
                    self.log.info(f"DB: inserting {len(tuple_to_write)} events.")
                    sql_util.insert_and_commit(
                        conn, self.log, EVENTS_RAW_TABLE, ('cycle_id', 'data', 'event_id'), tuple_to_write,
                        verbose=False
                    )
                    self.log.info(f"DB: inserted successfully.")
        self._events_inserted += len(events)

        if end_of_area:
            self._events_in_area = 0
        else:
            self._events_in_area += len(events)
        del events

    def _get_csrf(self):
        if not hasattr(self, '_refresh_date') \
                or (dt.utcnow() - self._refresh_date).total_seconds() > CSRF_REFRESH_INTERVAL:
            resp = requests.get(CSRF_SOURCE)
            self._token = resp.text.split("name='csrfmiddlewaretoken' value='")[1].split("' ")[0]
            self._refresh_date = dt.utcnow() + timedelta(seconds=CSRF_REFRESH_INTERVAL)
            self.log.info(f"got token=\"{self._token}\".")

    def _get_headers(self):
        return {
            'X-CSRFToken': self._token,
            'Content-Type': 'application/json',
            'Referer': 'https://www.eventbrite.com/d/al--adamsville/music--events/',
            'Cookie': '''csrftoken={}'''.format(self._token)
        }

    def _filter_by_categories(self, events):
        if not self._categories:
            return events
        return list(filter(
            lambda e: tuple(
                t for t in e['tags']
                if t['prefix'] == 'EventbriteCategory' and t['display_name'].strip().lower() in self._categories
            ),
            events
        ))
