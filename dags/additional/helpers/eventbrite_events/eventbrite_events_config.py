from airflow.models.variable import Variable
from additional.tools.config_helper import ConfigReader

try:
    config_name = Variable.get('EVENTBRITE_CONFIG_NAME')
except KeyError:
    config_name = 'EVENTBRITE'

# # US
#
# 19.056951, -127.753536
# 52.560759, -51.071126

DEFAULT_BBOXES = [
    [-128, 18, -50, 53],
]

DEFAULT_PAYLOAD = \
'''{"event_search":{"date_range": {"to":"{to}","from":"{from}"},
"page":{page},"page_size":50,
"bbox":"{bbox}"},
"expand.destination_event": ["ticket_availability","primary_venue","music_properties","event_sales_status","has_digital_content","refund_policy"]}'''

with ConfigReader(config_name) as cr:

    CONFIG_NAME = config_name
    MANUAL_CYCLE_ID = cr.read_or_default('manual_cycle_id', None)

    CSRF_SOURCE = cr.read_or_default('csrf_source', 'https://www.eventbrite.com/d/al--adamsville/music--events/')
    CSRF_REFRESH_INTERVAL = cr.read_or_default('csrf_refresh_interval', 300)

    BBOXES = cr.read_or_default('bboxes', DEFAULT_BBOXES)
    PAYLOAD = cr.read_or_default('payload', DEFAULT_PAYLOAD)
    CATEGORIES = cr.read_or_default('categories', ['Music'])
    PERIOD_DAYS = cr.read_or_default('period_days', 180)

    EVENTS_RAW_TABLE = cr.read_or_default('venues_raw_table', 'eventbrite_events_raw')
    EVENTS_FINAL_TABLE = cr.read_or_default('venues_raw_table', 'eventbrite_events_final')
    SQL_BATCH_SIZE = cr.read_or_default('sql_batch_size', 1000)

    EXPORT_TO_CSV = cr.read_or_default('export_to_csv', True)
    EXPORT_RECORDS_THRESHOLD = cr.read_or_default('export_records_threshold', 100000)
