import ast
import json
import asyncio
from random import shuffle
from re import findall
from collections import namedtuple
import requests
from bs4 import BeautifulSoup as Bs

from airflow.models.variable import Variable
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from additional.helpers.etix_venues.etix_config import \
    NUM_WORKERS_STEP_1, NUM_WORKERS_STEP_2, PULL_VENUES, VENUES_VAR_NAME, VENUES_BACKUP_VAR_NAME


class MergeVenues(BaseOperator):
    """

    """

    template_fields = ()
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            *args, **kwargs):
        super().__init__(*args, **kwargs)

    def execute(self, context):

        if PULL_VENUES:

            all_venues = set()

            for i in range(NUM_WORKERS_STEP_1):
                task_id = f'pull_venues_{i}'
                try:
                    those_venues = context['task_instance'].xcom_pull(task_ids=task_id, key=f"venues")
                    all_venues.update(those_venues)
                except:
                    continue

            all_venues = list(all_venues)

            try:
                previous_venues = Variable.get(VENUES_VAR_NAME, deserialize_json=True)
            except KeyError:
                previous_venues = []

            if previous_venues:
                self.log.info(f"previous_venues_count={len(previous_venues)}")
            Variable.set(VENUES_BACKUP_VAR_NAME, previous_venues)
            all_venues = list(set(all_venues + previous_venues))
            Variable.set(VENUES_VAR_NAME, all_venues, True)

        else:
            all_venues = Variable.get(VENUES_VAR_NAME, deserialize_json=True)

        shuffle(all_venues)

        # split configs among workers
        venues_count = len(all_venues)
        self.log.info(f"unique_venues_count={venues_count}")

        if venues_count <= NUM_WORKERS_STEP_2:
            partitions = {
                i: [i, i] for i in range(venues_count)
            }
            partitions.update({
                i: None for i in range(venues_count, NUM_WORKERS_STEP_1)
            })
        else:
            step = venues_count / NUM_WORKERS_STEP_1
            start_idx = 0
            end_idx = 0
            partitions = {}
            for i in range(NUM_WORKERS_STEP_1):
                end_idx = min(start_idx + step, venues_count)
                partitions[i] = [int(start_idx // 1), int(end_idx // 1)]
                start_idx = end_idx
            if partitions:
                partitions[NUM_WORKERS_STEP_1 - 1][-1] = venues_count
        self.log.info(f"Broke configs into following partitions: {partitions}")

        venues_partitions = {}
        for task in partitions:
            if partitions[task] is None:
                venues_partitions[task] = "[]"
            else:
                venues_partitions[task] = json.dumps(all_venues[partitions[task][0]: partitions[task][1] + 1])
        self.log.info(f'Pushing partitions to the context')

        context['task_instance'].xcom_push(key="venues_partition", value=venues_partitions)
