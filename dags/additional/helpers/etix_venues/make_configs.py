import json
from random import shuffle
from itertools import product
import requests

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from additional.tools.const import ALPHABET
from additional.helpers.etix_venues.etix_config import COUNTRIES, NUM_WORKERS_STEP_1, IS_TEST_MODE


class MakeConfigs(BaseOperator):
    """

    """

    template_fields = ()
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            *args, **kwargs):
        super().__init__(*args, **kwargs)

    def execute(self, context):

        cities_raw = []
        url = "https://www.etix.com/ticket/online/newHomePage.do?method=getStateCityByString"
        headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

        for char, country in product(ALPHABET, COUNTRIES):

            payload = f"query_string={char}&country={country}"

            try:

                cities_raw += requests.request("POST", url, headers=headers, data=payload).json()
                self.log.info(f"{payload} : {len(cities_raw)}")

            except Exception as ex:
                self.log.error(f"Error for char={char} & country={country}", exc_info=True)

            # if IS_TEST_MODE:
            #     self.log.info("Enough for test mode!")
            #     break

        hashes_raw = []
        for cfg in cities_raw:
            cfg['hash'] = f"{cfg['country']}{cfg['city']}{cfg['state']}"
            hashes_raw.append(cfg['hash'])
        hashes_dedup = set(hashes_raw)
        cities_dedup = [next(c for c in cities_raw if c['hash'] == hs) for hs in hashes_dedup]
        shuffle(cities_dedup)
        self.log.info(f"cities_dedup={len(cities_dedup)}")

        # split configs among workers
        configs_count = len(cities_dedup)
        if configs_count <= NUM_WORKERS_STEP_1:
            partitions = {
               i: [i, i] for i in range(configs_count)
            }
            partitions.update({
                i: None for i in range(configs_count, NUM_WORKERS_STEP_1)
            })
        else:
            step = configs_count / NUM_WORKERS_STEP_1
            start_idx = 0
            end_idx = 0
            partitions = {}
            for i in range(NUM_WORKERS_STEP_1):
                end_idx = min(start_idx + step, configs_count)
                partitions[i] = [int(start_idx // 1), int(end_idx // 1)]
                start_idx = end_idx
            if partitions:
                partitions[NUM_WORKERS_STEP_1 - 1][-1] = configs_count
        self.log.info(f"Broke configs into following partitions: {partitions}")

        state_partitions = {}
        for task in partitions:
            if partitions[task] is None:
                state_partitions[task] = "[]"
            else:
                state_partitions[task] = json.dumps(cities_dedup[partitions[task][0]: partitions[task][1] + 1])
        self.log.info(f'Pushing partitions to the context')

        context['task_instance'].xcom_push(key="configs", value=state_partitions)
