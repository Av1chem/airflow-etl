from random import shuffle

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from additional.helpers.etix_venues.etix_config import \
    NUM_WORKERS_STEP_2


class MergeEvents(BaseOperator):
    """

    """

    template_fields = ()
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            *args, **kwargs):
        super().__init__(*args, **kwargs)

    def execute(self, context):

        # pull events from XCom
        all_events = []
        for i in range(NUM_WORKERS_STEP_2):
            task_id = f'pull_events_{i}'
            try:
                those_events = context['task_instance'].xcom_pull(task_ids=task_id, key=f"events")
                all_events += those_events
            except:
                continue

        # dedup events - just in case
        for ev in all_events:
            ev['hash'] = f"{ev['venue'].get('name')}{ev.get('name')}{ev.get('date')}{ev.get('time')}"

        unique_hashes = set(e['hash'] for e in all_events)
        events_dedup = [
            next(ev for ev in all_events if ev['hash'] == cur_hash) for cur_hash in unique_hashes
        ]

        self.log.info(f"Events: raw={len(all_events)}, dedup={len(events_dedup)}.")

        del all_events, unique_hashes
        for ev in events_dedup:
            del ev['hash']

        shuffle(events_dedup)

        # store results to DB
