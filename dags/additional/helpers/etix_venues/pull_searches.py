import ast
import asyncio
from time import sleep
from random import shuffle
from urllib.parse import quote
import requests
from bs4 import BeautifulSoup as Bs

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from additional.helpers.etix_venues.etix_config import \
    IS_TEST_MODE, PAGE_LIMIT, DUP_PAGES_LIMIT, NUM_WORKERS_STEP_1, ASYNC_REQUESTS, \
    REQUEST_RETRIES, PULLING_VENUES_ERROR_TOLERANCE, WITHOUT_NEW_VENUES_FINISH_THRESHOLD, SLEEP_AFTER_ERROR, \
    TEST_MODE_CONFIGS_LIMIT, VENUES_SYNCHRONIZE_DATA_AMONG_THREADS, VENUES_SAVE_TO_XCOM_EVERY_X_CONFIGS


class PullSearches(BaseOperator):
    """
    """

    template_fields = ()
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            task_idx,
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.task_idx = task_idx
        # async related
        self._resp = None
        self._send_request_tasks = set()

    def execute(self, context):

        try:
            configs = context['task_instance'].xcom_pull(task_ids="make_configs", key=f"configs")[str(self.task_idx)]
        except KeyError:
            configs = context['task_instance'].xcom_pull(task_ids="make_configs", key=f"configs")[int(self.task_idx)]

        if not configs:
            self.log.error("No configs found. Exiting...")
            return

        configs = ast.literal_eval(configs)

        if IS_TEST_MODE:
            self.log.info(f"Running in test mode. Will complete only {TEST_MODE_CONFIGS_LIMIT} searches.")
            configs = configs[:TEST_MODE_CONFIGS_LIMIT]

        shuffle(configs)

        configs_count = len(configs)
        self.log.info(f"Got configs type={type(configs)}, count={configs_count}")

        URL_TEMPLATE = 'https://www.etix.com/ticket/online/newHomePage.do?method=createHomePage' \
                       '&country={country}&language=en&city={city}&state={state}&find_by_address=true'

        these_venues = set()
        all_venues = set()
        errors_in_a_row = 0

        if ASYNC_REQUESTS:
            loop = asyncio.get_event_loop()
            self.run_loop(loop)

        for cfg_idx, cfg in enumerate(configs):

            pages_without_new_venues = 0

            # scrape config
            self.log.info(f"Proceeding to config # {cfg_idx + 1} of {configs_count}, config={cfg}")

            with requests.session() as s:

                try:

                    # do search
                    url = URL_TEMPLATE.format(
                        country=quote(cfg['country'][:2]),
                        city=quote(cfg['city']),
                        state=quote(cfg['state'])
                    )

                    NEXT_PAGE_URL_TEMPLATE = 'https://www.etix.com/ticket/online/newHomePage.do?method=createHomePage' \
                                             '&pageNumber={page_idx}&sortBy='

                    events_prev_page = []
                    events_cur_page = []
                    dup_pages = 0

                    for cur_page_idx in range(1, PAGE_LIMIT):

                        new_venues_on_found = False

                        self.log.info(f"Config # {cfg_idx + 1} of {configs_count}, page # {cur_page_idx}. "
                                      f"{'dup_pages_count={}, '.format(dup_pages) if dup_pages else ''}"
                                      f"""{'pages_without_new_venues={}, '.format(pages_without_new_venues) 
                                            if pages_without_new_venues else ''}"""
                                      f"unique_venues_count={len(these_venues)}.")

                        if cur_page_idx == 1 or not ASYNC_REQUESTS:
                            # # Sync requests
                            exc = None
                            for _ in range(REQUEST_RETRIES):
                                try:
                                    self._resp = s.get(url)
                                    exc = None
                                    break
                                except Exception as ex:
                                    exc = ex
                                    continue
                            if exc is not None:
                                raise exc

                            obj = Bs(self._resp.content, 'lxml')
                            url = NEXT_PAGE_URL_TEMPLATE.format(page_idx=cur_page_idx+1)
                        else:
                            # # Async requests

                            # wait till previous request finish
                            pending = asyncio.Task.all_tasks()
                            loop.run_until_complete(asyncio.gather(*pending))

                            # parse response
                            obj = Bs(self._resp.content, 'lxml')

                            # start next request (async)
                            url = NEXT_PAGE_URL_TEMPLATE.format(page_idx=cur_page_idx + 1)
                            loop.create_task(self.http_get(s, url))

                        events = obj.select('div.panel:not(.perf-mobile)')
                        events_prev_page = events_cur_page
                        events_cur_page = []
                        for e in events:

                            info_panel = e.select_one('.perf-info > h3 > a')
                            event_href = f'https://www.etix.com{info_panel["href"]}'
                            events_cur_page.append(event_href)

                            venue = e.select_one('.venue > a')
                            venue_href = f'https://www.etix.com{venue["href"]}'

                            if venue_href not in all_venues:
                                these_venues.add(venue_href)
                                all_venues.update(these_venues)
                                new_venues_on_found = True
                                pages_without_new_venues = 0

                        try:
                            max_visibile_page = max(int(e.text) for e in obj.select('.pagination li') if e.text.isdigit())
                        except:
                            max_visibile_page = 0

                        if max_visibile_page <= cur_page_idx:
                            self.log.info(f"That was the last page.")
                            break

                        events_cur_page.sort()
                        events_prev_page.sort()

                        if len(events_cur_page) == len(events_prev_page) and len(events_cur_page) == sum(
                                [1 for i, j in zip(events_cur_page, events_prev_page) if i == j]):
                            dup_pages += 1
                        else:
                            dup_pages = 0

                        if dup_pages >= DUP_PAGES_LIMIT:
                            self.log.info(
                                f"Got {DUP_PAGES_LIMIT} duplicated pages in a row. Proceeding to next config."
                            )
                            break

                        if not new_venues_on_found:
                            pages_without_new_venues += 1
                        if pages_without_new_venues >= WITHOUT_NEW_VENUES_FINISH_THRESHOLD:
                            self.log.info(f"Got {pages_without_new_venues} pages without new data in a row. "
                                          f"Proceeding to next config")
                            break

                    if cfg_idx % VENUES_SAVE_TO_XCOM_EVERY_X_CONFIGS == 0:
                        # share collected data among other workers, and load their data
                        context['task_instance'].xcom_push(key="venues", value=list(these_venues))

                        if VENUES_SYNCHRONIZE_DATA_AMONG_THREADS:
                            for i in range(NUM_WORKERS_STEP_1):
                                task_id = f'pull_venues_{i}'
                                try:
                                    those_venues = context['task_instance'].xcom_pull(task_ids=task_id, key=f"venues")
                                    all_venues.update(those_venues)
                                except:
                                    continue

                            self.log.info(f"Total {len(all_venues)} unique collected among all workers.")

                    errors_in_a_row = 0

                except Exception as ex:

                    self.log.error(f"Error pulling config={cfg}", exc_info=True)
                    errors_in_a_row += 1
                    if errors_in_a_row < PULLING_VENUES_ERROR_TOLERANCE:
                        sleep(SLEEP_AFTER_ERROR)

            if errors_in_a_row >= PULLING_VENUES_ERROR_TOLERANCE:
                if ASYNC_REQUESTS:
                    loop.close()
                self.log.error(f"Too much errors in a row: {errors_in_a_row}")
                raise ex

        if ASYNC_REQUESTS:
            loop.close()

    async def http_get(self, session, url):
        for _ in range(REQUEST_RETRIES):
            try:
                self._resp = session.get(url)
                ex = None
                break
            except Exception as ex:
                continue
        if ex is not None:
            raise ex

    @staticmethod
    async def run_loop(loop):
        loop.run_forever()
