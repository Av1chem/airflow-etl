import json
from random import shuffle

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.hooks.postgres_hook import PostgresHook

from additional.tools import sql_util
from additional.tools.database_mixin import DatabaseMixin

TABLE = 'ticketweb'
SQL_BATCH_SIZE = 1000


class MergeIds(BaseOperator, DatabaseMixin):
    """

    """

    template_fields = ('cycle_id', )
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            domains,
            num_workers,
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.cycle_id = cycle_id
        if isinstance(self.cycle_id, tuple):
            self.cycle_id = self.cycle_id[0]
        
        self.domains = domains
        self.num_workers = num_workers

        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)

    def execute(self, context):

        self.cleanup_results(TABLE)

        # pull events from XCom
        all_events = []
        for d in self.domains:
            task_id = f'pull_ids_from_{d}'
            try:
                those_events = context['task_instance'].xcom_pull(task_ids=task_id, key=f"e_ids")
                all_events += those_events
            except:
                continue

        # dedup data - just in case
        self.log.info(f"Events raw: {len(all_events)}")
        all_events = [
            next(v for v in all_events if v['id'] == v_id)
            for v_id in set(e['id'] for e in all_events)
        ]
        self.log.info(f"Events unique: {len(all_events)}")

        for e in all_events:
            if 'is-soldOut' in e['ui_status']['tags']:
                e['is_sold_out'] = True
            else:
                e['is_sold_out'] = False

        # writing to database
        for s_i in range(0, len(all_events), SQL_BATCH_SIZE):
            self.log.info(f"DB: writing batch of size {len(all_events[s_i:s_i + SQL_BATCH_SIZE])}.")
            tuple_to_write = [
                (self.cycle_id, json.dumps(e)) for e in all_events[s_i:s_i + SQL_BATCH_SIZE]
            ]
            if tuple_to_write:
                with self.hook.get_conn() as conn:
                    self.log.info(f"DB: inserting {len(tuple_to_write)} all_events.")
                    sql_util.insert_and_commit(
                        conn, self.log, TABLE, ('cycle_id', 'data'), tuple_to_write,
                        verbose=False
                    )
        self.log.info(f"DB: inserted successfully.")

        # shuffle(all_events)
        #
        #
        # # split configs among workers
        # configs_count = len(all_events)
        # if configs_count <= self.num_workers:
        #     partitions = {
        #         i: [i, i] for i in range(configs_count)
        #     }
        #     partitions.update({
        #         i: None for i in range(configs_count, self.num_workers)
        #     })
        # else:
        #     step = configs_count / self.num_workers
        #     start_idx = 0
        #     end_idx = 0
        #     partitions = {}
        #     for i in range(self.num_workers):
        #         end_idx = min(start_idx + step, configs_count)
        #         partitions[i] = [int(start_idx // 1), int(end_idx // 1)]
        #         start_idx = end_idx
        #     if partitions:
        #         partitions[self.num_workers - 1][-1] = configs_count
        # self.log.info(f"Broke configs into following partitions: {partitions}")
        #
        # e_partitions = {}
        # for task in partitions:
        #     if partitions[task] is None:
        #         e_partitions[task] = []
        #     else:
        #         e_partitions[task] = all_events[partitions[task][0]: partitions[task][1] + 1]
        # self.log.info(f'Pushing partitions to the context')
        #
        # context['task_instance'].xcom_push(key="e_partitions", value=e_partitions)
