import copy
import json
import re
import requests
from time import sleep
from datetime import datetime as dt, timedelta

from bs4 import BeautifulSoup as Bs

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults


class PullIds(BaseOperator):
    """
    """

    template_fields = ('cycle_id', )
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            domain,
            is_test,
            *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.cycle_id = cycle_id
        self.domain = domain
        self.is_test = is_test

    def periods(self):

        if self.is_test:
            s_d = dt.utcnow() + timedelta(days=1)
            p_limit = 1
        else:
            s_d = dt.utcnow()
            p_limit = 20

        for i in range(p_limit):
            e_d = s_d + timedelta(days=30)
            yield s_d.strftime('%Y-%m-%d'), e_d.strftime('%Y-%m-%d')

            s_d = e_d

    def execute(self, context):

        headers = {
            'authority': 'www.ticketweb.uk',
            'pragma': 'no-cache',
            'cache-control': 'no-cache',
            'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
            'sec-ch-ua-mobile': '?0',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'sec-fetch-site': 'none',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-user': '?1',
            'sec-fetch-dest': 'document',
            'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
            # 'cookie': 'correlationId=ecm48b72db8-b329-46fd-83eb-c8a91fd7aa19; ECM_JSESSIONID=5b124f2e-fb24-4440-b037-497547621b92.ecmnode1; initialSignIn=1; regionId=39; ecm_zipCode=1011%20; ecm_location=Manhattan%2C%20NY; _gcl_au=1.1.1718596120.1625115421; _ga=GA1.2.414692391.1625115422; _gid=GA1.2.1318741609.1625115422; correlationId=ecm79926e61-68d1-4d9b-a648-5309f0052076; ECM_JSESSIONID=b7396524-7019-4af7-9f80-78b1246c4078.ecmnode1'
        }

        all_results = []
        events_expected_total = 0
        for p_idx, period in enumerate(self.periods()):
            events_expected = 0
            results = []
            url = f'https://www.ticketweb.{self.domain}/search?startdate={period[0]}&enddate={period[1]}&page=' \
                  + '{page_idx}'
            for i in range(99999):
                resp = requests.get(url.format(page_idx=i), headers=headers)
                obj = Bs(resp.text, 'lxml')
                if events_expected == 0:
                    try:
                        events_expected=int(re.findall(r"\d*,*\d+", obj.select_one('.title').text)[0].replace(',', ''))
                        self.log.info(f"\n\n\nProceeding to period # {p_idx+1}:from {period[0]} to {period[1]}\n\n\n"
                                      f"Expecting {events_expected} events this dates \n\n")
                    except AttributeError:
                        self.log.warning(f"Can't parse results count for \"{url}\"")
                try:
                    ld_json = obj.select_one('script[type="application/ld+json"]')

                    if ld_json is None:
                        self.log.warning(f'Page #{i}. No ld+json on page. Repeat.')
                        i -= 1
                        continue
                    ld = json.loads(ld_json.decode_contents())
                except json.JSONDecodeError:
                    # no more events
                    self.log.info(f'Page #{i}. JSONDecodeError (No more events).')
                    break
                if not len(ld):
                    # no more events
                    self.log.info(f'Page #{i}. "len(ld) == 0" (No more events).')
                    break

                for l in ld:
                    l['domain'] = self.domain
                    l['id'] = l['url'].split('/')[-1]

                for a in obj.select('.list-group-item .event-status a'):
                    if 'data-ng-href' in a.attrs or 'href' in a.attrs:
                        l = next(
                            (l for l in ld
                             if str(l.get('url')) in str(a.attrs.get('data-ng-href'))
                             or str(l.get('url')) in str(a.attrs.get('href'))),
                            None
                        )
                        if l is None:
                            self.log.warning(f"No matching ld+json entry for \"{a.attrs.get('data-ng-href')}\"!")
                        else:
                            l['ui_status'] = {
                                'title': copy.deepcopy(a.attrs.get('title')),
                                'tags': copy.deepcopy(a.attrs.get('class'))
                            }
                results += ld

                self.log.info(f'Page #{i+1} parsed.')
            self.log.info(f'Finished parsing "{url}".\n'
                          f'Links grabbed: {len(results)} (expected={events_expected}).')
            all_results += results
            events_expected_total += events_expected
        self.log.info(f'Finished parsing all of the events in "{self.domain}".\n'
                      f'Links grabbed total: {len(all_results)} (expected_total={events_expected_total}).')
        context['task_instance'].xcom_push(key="e_ids", value=all_results)
