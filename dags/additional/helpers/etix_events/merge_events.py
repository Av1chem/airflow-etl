import json
from random import shuffle

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.hooks.postgres_hook import PostgresHook

from additional.tools import sql_util
from additional.tools.database_mixin import DatabaseMixin

from additional.helpers.etix_events.etix_config import \
    NUM_WORKERS_STEP_2, SAVE_TO_DB, TABLE_NAME, SQL_BATCH_SIZE


class MergeEvents(BaseOperator, DatabaseMixin):
    """

    """

    template_fields = ('cycle_id', )
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cycle_id = cycle_id
        if isinstance(self.cycle_id, tuple):
            self.cycle_id = self.cycle_id[0]
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)

    def execute(self, context):

        self.cleanup_results(TABLE_NAME)

        # pull events from XCom
        all_events = []
        for i in range(NUM_WORKERS_STEP_2):
            task_id = f'pull_events_{i}'
            try:
                those_events = context['task_instance'].xcom_pull(task_ids=task_id, key=f"events")
                all_events += those_events
            except:
                continue

        # dedup events - just in case
        for ev in all_events:
            ev['hash'] = f"{ev['venue'].get('name')}{ev.get('name')}{ev.get('date')}{ev.get('time')}"

        unique_hashes = set(e['hash'] for e in all_events)
        events_dedup = [
            next(ev for ev in all_events if ev['hash'] == cur_hash) for cur_hash in unique_hashes
        ]

        self.log.info(f"Events: raw={len(all_events)}, dedup={len(events_dedup)}.")

        del all_events, unique_hashes
        for ev in events_dedup:
            del ev['hash']

        shuffle(events_dedup)

        # store results to DB
        if SAVE_TO_DB:
            for start_idx in range(0, len(events_dedup), SQL_BATCH_SIZE):
                cur_chunk = events_dedup[start_idx:start_idx+SQL_BATCH_SIZE]
                tuples_to_write = []
                for p in cur_chunk:
                    # 'insert_schema': [
                    #     'cycle_id', 'task_id', 'provider_id', 'location_id', 'data', 'search_payload', 'step_0_id',
                    #     'provider_type', 'step_idx'
                    # ]
                    tuples_to_write.append((
                        self.cycle_id, json.dumps(p)
                    ))
                if tuples_to_write:
                    with self.hook.get_conn() as conn:
                        self.log.info(f"DB: inserting {len(tuples_to_write)} events.")
                        sql_util.insert_and_commit(
                            conn, self.log, TABLE_NAME, ['cycle_id', 'data'], tuples_to_write,
                            verbose=False
                        )
                        self.log.info(f"DB: inserted successfully.")
