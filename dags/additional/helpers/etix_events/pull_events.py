import ast
import calendar
from re import findall
from copy import deepcopy
from time import sleep
from random import shuffle
from datetime import datetime
from dateutil.relativedelta import relativedelta

import requests
from bs4 import BeautifulSoup as Bs

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from additional.helpers.etix_events.etix_config import \
    REQUEST_RETRIES, PULLING_EVENTS_ERROR_TOLERANCE, SLEEP_AFTER_ERROR, SEARCH_FOR_MONTH_AHEAD, \
    EVENTS_PULL_MORE_INFO, PAGE_LIMIT, EVENTS_SAVE_TO_XCOM_EVERY_X_CONFIGS
from additional.tools.const import AvailabilityStatus


class PullEvents(BaseOperator):
    """

    """

    template_fields = ()
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            task_idx,
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.task_idx = task_idx

    def execute(self, context):

        # # init const
        today = datetime.today()
        ENDPOINT_URL_TEMPLATE = 'https://www.etix.com/ticket/json/calendar/venue/{venue_id}/{year}/{month}'
        API_PAYLOAD_TEMPLATE = \
            "queryString=language%3Den%26country%3DUS%26venue_id%3D{venue_id}%26specifiedYearMonth%3Dtrue"
        API_HEADERS = {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
        MORE_INFO_URL_TEMPLATE = 'https://www.etix.com/ticket/online/newHomePage.do?method=getMoreInfo' \
                                 '&event_id={event_id}&event_type={event_type}'
        NEXT_PAGE_URL_TEMPLATE = 'https://www.etix.com/ticket/online/upcomingEventSearch.do?method=venueSearch' \
                                 '&venue_id={venue_id}&orderBy=1&pageNumber={page_idx}'

        try:
            venues = context['task_instance'].xcom_pull(
                task_ids="merge_venues", key=f"venues_partition")[str(self.task_idx)]
        except KeyError:
            venues = context['task_instance'].xcom_pull(
                task_ids="merge_venues", key=f"venues_partition")[int(self.task_idx)]

        if not venues:
            self.log.error("No venues found. Exiting...")
            return

        venues = ast.literal_eval(venues)
        shuffle(venues)

        venues_count = len(venues)
        self.log.info(f"Got venues type={type(venues)}, count={venues_count}")

        these_events = []
        errors_in_a_row = 0

        for ven_idx, ven in enumerate(venues):

            try:
                # scrape config
                self.log.info(f"Proceeding to venue # {ven_idx + 1} of {venues_count}, venue_url={ven}")

                with requests.session() as s:

                    # # Get venue page
                    for _ in range(REQUEST_RETRIES):
                        try:
                            resp = s.get(ven)
                            obj = Bs(resp.content, 'lxml')
                            ex = None
                            break
                        except Exception as ex:
                            continue
                    if ex is not None:
                        raise ex

                    # # Parsing venue details
                    details_container = obj.select_one('#venue-details, #venueDetails')
                    if details_container is None:
                        self.log.warning(f"Venue without details: {ven}")
                        continue
                    venue = {'etix_url': ven}
                    name = details_container.select_one('.venue-name')
                    if name:
                        url = name.select_one('a[itemprop="url"]')
                        if url:
                            url = url.attrs['href']
                            venue['venue_website'] = url
                        name = name.text.strip()
                        venue['name'] = name

                    address_container = details_container.select_one('.venue-address')
                    if address_container:
                        venue['address'] = {}
                        add_rows = details_container.select(
                            '.venue-address > div, .venue-address > meta, '
                            '.venue-address a[href*="maps.google.com/maps"] > span, '
                            '.venue-address a[href*="maps.google.com/maps"] > meta'
                        )
                        for a_r in add_rows:
                            el_class = a_r.attrs.get('class')
                            if el_class:
                                el_class = el_class[0].replace('venue-', '').strip().lower()
                                if 'phone' in el_class:
                                    venue[el_class] = a_r.text.strip()
                                else:
                                    if el_class == 'state':
                                        el_class = 'country'
                                    venue['address'][el_class] = ' '.join(s for s in a_r.stripped_strings if s)
                            else:
                                itemprop = a_r.attrs.get('itemprop')
                                if not itemprop:
                                    continue
                                itemprop = itemprop.strip().lower()
                                venue['address'][itemprop] = a_r.attrs.get('content').strip()

                    add_info = details_container.select_one('.venue-info')
                    if add_info:
                        venue['additional_info'] = add_info.text.strip()

                    social = details_container.select_one('.email-and-social')
                    if social:
                        venue['email_and_social'] = {}
                        for link in social.select('a[title]'):
                            venue['email_and_social'][link.attrs.get('title').strip().lower()] = link.attrs.get('href')
                        email = social.select_one('.emailButton')
                        if email:
                            email = email.attrs.get('onclick')
                            if email:
                                email = email.split('mailto:')
                                if len(email) > 1:
                                    email = email[1].replace("'", '').strip()
                                    venue['email_and_social']['email'] = email

                    # # Parsing events
                    venue_id = findall(r'\/[v,o]\/([0-9]+)', ven)[0]
                    if obj.select_one('#calendarframe'):

                        # calendar-based view: parsing through API
                        self.log.info("Got calendar.")

                        date_for_loop = deepcopy(today)

                        for _ in range(SEARCH_FOR_MONTH_AHEAD):
                            # API call
                            url = ENDPOINT_URL_TEMPLATE.format(
                                venue_id=venue_id,
                                year=date_for_loop.year,
                                month=date_for_loop.month
                            )
                            payload = API_PAYLOAD_TEMPLATE.format(venue_id=venue_id)
                            for _ in range(REQUEST_RETRIES):
                                try:
                                    resp = s.post(
                                        url=url,
                                        headers=API_HEADERS,
                                        data=payload
                                    ).json()
                                    ex = None
                                    break
                                except Exception as ex:
                                    continue
                            if ex is not None:
                                raise ex

                            # response parsing
                            for e_date in resp['dates']:

                                if today.year == resp['year'] and today.month == resp['month'] \
                                        and today.day > e_date['date']:
                                    # passed event
                                    continue

                                for act in e_date['activities']:

                                    # calculating status
                                    status_raw = act.get('status')
                                    status_final = {
                                        'id': AvailabilityStatus.UNKNOWN.value,
                                        'name': AvailabilityStatus.UNKNOWN.status_name
                                    }
                                    if status_raw and 'soldOut' in status_raw:
                                        status_final = {
                                            'id': AvailabilityStatus.SOLD_OUT.value,
                                            'name': AvailabilityStatus.SOLD_OUT.status_name
                                        }
                                    elif status_raw and 'notOnSale' in status_raw:
                                        status_final = {
                                            'id': AvailabilityStatus.NOT_ON_SALE.value,
                                            'name': AvailabilityStatus.NOT_ON_SALE.status_name
                                        }
                                    elif act.get('buyUrl') is not None \
                                            and (act.get('minPrice') > 0 or act.get('maxPrice') > 0):
                                        status_final = {
                                            'id': AvailabilityStatus.AVAILABLE.value,
                                            'name': AvailabilityStatus.AVAILABLE.status_name
                                        }

                                    event_url = act['buyUrl'].split('?language')[0] if act.get('buyUrl') else None
                                    more_info = None
                                    onsale_date, onsale_time = None, None
                                    if event_url:
                                        if EVENTS_PULL_MORE_INFO:
                                            event_id = findall(r'e\/[0-9]+|p\/[0-9]+|k\/[0-9]+', event_url)[0]
                                            if event_id:
                                                # request more info
                                                more_info_url = MORE_INFO_URL_TEMPLATE.format(
                                                    event_id=event_id,
                                                    event_type=act.get('performanceType')
                                                )

                                                for _ in range(REQUEST_RETRIES):
                                                    try:
                                                        more_info_resp = s.get(more_info_url)
                                                        ex = None
                                                        break
                                                    except Exception as ex:
                                                        continue
                                                if ex is not None:
                                                    raise ex

                                                more_info = more_info_resp.text.strip()

                                        # check on sale date
                                        if status_final['id'] == AvailabilityStatus.NOT_ON_SALE.value:
                                            # get event's page
                                            for _ in range(REQUEST_RETRIES):
                                                try:
                                                    event_resp = s.get(event_url)
                                                    ex = None
                                                    break
                                                except Exception as ex:
                                                    continue
                                            if ex is not None:
                                                raise ex

                                            # check  public onsale date
                                            event_obj = Bs(event_resp.content, 'lxml')
                                            page_strs = [s for s in event_obj.stripped_strings]
                                            try:
                                                onsale_idx = next(i for i, s in enumerate(page_strs) if
                                                                  'public onsale begins' in s.lower())
                                                date_str = page_strs[onsale_idx + 1]
                                                date_str = [s.replace(',', '').strip() for s in date_str.split(' ')]
                                                date_str = date_str[1:]
                                                month = next(
                                                    index for index, month in enumerate(calendar.month_abbr)
                                                    if month.lower() in date_str[0].lower() and index > 0
                                                )
                                                day = int(date_str[1])
                                                year = int(date_str[2])
                                                pm = 'PM' in date_str[-1]
                                                time = date_str[-2].split(':')
                                                hour = int(time[0])
                                                if pm and hour != 12:
                                                    hour += 12
                                                minute = int(time[1])
                                                onsale_datetime = datetime(
                                                    year=year,
                                                    month=month,
                                                    day=day,
                                                    hour=hour,
                                                    minute=minute
                                                )
                                                onsale_date = onsale_datetime.strftime("%Y-%m-%d")
                                                onsale_time = onsale_datetime.strftime('%I:%M %p').lstrip("0")

                                            except (StopIteration, IndexError):
                                                pass
                                    cur_event = {
                                        'name': act.get('name'),
                                        'event_url': event_url,
                                        'price': {
                                            'min': act.get('minPrice'),
                                            'max': act.get('maxPrice'),
                                            'currency': act.get('currency'),
                                        },
                                        'date': datetime(
                                            year=resp['year'], month=resp['month'], day=e_date['date']
                                        ).strftime("%Y-%m-%d"),
                                        'time': act['time'],
                                        'tickets_availability': status_final,
                                        'venue': deepcopy(venue)
                                    }
                                    if more_info is not None:
                                        cur_event['more_info'] = more_info

                                    if onsale_date is not None:
                                        cur_event['onsale_date'] = onsale_date
                                    if onsale_time is not None:
                                        cur_event['onsale_time'] = onsale_time

                                    if cur_event.get('tickets_availability').get('id') == \
                                            AvailabilityStatus.SOLD_OUT.value:
                                        cur_event['is_sold_out'] = 1
                                    else:
                                        cur_event['is_sold_out'] = 0

                                    # check for cancelled events
                                    if cur_event.get('tickets_availability').get('id') in [
                                                AvailabilityStatus.NOT_ON_SALE.value,
                                                AvailabilityStatus.UNKNOWN.value
                                            ]:
                                        if (findall(r"cancel+ed", cur_event['name'].lower())
                                             or findall(r"postponed", cur_event['name'].lower())
                                             or findall(r"postponing", cur_event['name'].lower())):
                                            cur_event['tickets_availability'] = {
                                                'id': AvailabilityStatus.CANCELLED.value,
                                                'name': AvailabilityStatus.CANCELLED.status_name
                                            }

                                    # check if it is hosted in DE
                                    if cur_event['venue'].get('address') and \
                                            (not cur_event['venue']['address'].get('country') \
                                             or not cur_event['venue']['address'].get('country').strip()) and \
                                            (cur_event.get('price') and cur_event['price'].get('currency')
                                             and 'eur' in cur_event['price'].get('currency').lower()):
                                        if cur_event['venue'].get('address'):
                                            cur_event['venue']['address']['country'] = 'Germany'

                                    these_events.append(cur_event)

                            self.log.info(f"Venue # {ven_idx + 1} of {venues_count}. "
                                          f"Parsed events for year={date_for_loop.year} and month={date_for_loop.month}. "
                                          f"Events total: {len(these_events)}.")

                            # generate new date
                            date_for_loop = date_for_loop + relativedelta(months=1)

                    else:

                        # pagination-based view
                        self.log.info("Got list of results.")

                        # loop through all the pages of results
                        for page_idx in range(1, PAGE_LIMIT):

                            # parse table
                            for row in obj.select('#view .row'):
                                cur_event = {'venue': deepcopy(venue)}

                                startdate = row.select_one('[itemprop="startDate"]')
                                if startdate:
                                    startdate = startdate.attrs.get('content')
                                    if startdate:
                                        startdate = startdate.strip().split('T')
                                        date = startdate[0]
                                        time = startdate[1].split('-')[0].split('+')[0]
                                        time = datetime.strptime(time, '%H:%M:%S')
                                        time = time.strftime('%I:%M %p').lstrip("0")
                                        cur_event['date'] = date
                                        cur_event['time'] = time
                                else:
                                    date = row.select_one('.datetime')
                                    if date:
                                        date = [s for s in date.stripped_strings]
                                        try:

                                            if len(date) >= 2:
                                                month = next(
                                                    index for index, month in enumerate(calendar.month_abbr)
                                                    if month.lower() in date[0].lower() and index > 0
                                                )
                                                day = int(date[1])
                                                hour = 0,
                                                minute = 0
                                                time = row.select_one('.performance-datetime')
                                                if time:
                                                    time = time.text
                                                    pm = 'PM' in time
                                                    time = findall(r"\d+:\d+", time)
                                                    if time:
                                                        time = time[0].split(':')
                                                        hour = int(time[0])
                                                        if pm and hour != 12:
                                                            hour += 12
                                                        minute = int(time[1])

                                                event_date = datetime(
                                                    year=today.year + 1 if today.month > month else today.year,
                                                    month=month,
                                                    day=day,
                                                    hour=hour,
                                                    minute=minute
                                                )

                                                cur_event['date'] = event_date.strftime("%Y-%m-%d")
                                                cur_event['time'] = event_date.strftime('%I:%M %p').lstrip("0")

                                        except Exception as ex:
                                            self.log.info(f"Can't parse date", exc_info=True)

                                event_details = row.select_one(' .details')
                                if event_details:
                                    name = event_details.select_one('.performance-name')
                                    if name:
                                        cur_event['name'] = name.text.strip()
                                        href = name.select_one('a[href]')
                                        if href:
                                            cur_event['event_url'] = f'https://www.etix.com{href.attrs.get("href")}'

                                if EVENTS_PULL_MORE_INFO:
                                    more_info = event_details.select_one('.event_details')
                                    if more_info:
                                        cur_event['more_info'] = more_info.text.strip()

                                # calculating status
                                buy_col = row.select_one('.buy')
                                if buy_col:
                                    ticket_avail = buy_col.text.strip().lower()

                                    price_tmp = {
                                        'min': 0.0,
                                        'max': 0.0,
                                        'currency': "Unknown",
                                    }
                                    status_final = {
                                        'id': AvailabilityStatus.UNKNOWN.value,
                                        'name': AvailabilityStatus.UNKNOWN.status_name
                                    }
                                    if 'public onsale:' in ticket_avail:
                                        ticket_avail = ticket_avail.split('public onsale:')[-1]
                                        ticket_avail = [
                                            s.strip() for s in ticket_avail.split('\n')
                                            if s.strip()
                                        ]
                                        year, month, day, hour, minute = 1, 1, 1, 0, 0
                                        for s in ticket_avail:
                                            time = findall('\d+:\d+', s)
                                            if time:
                                                pm = 'PM' in s
                                                time = time[0].split(':')
                                                hour = int(time[0])
                                                if pm and hour != 12:
                                                    hour += 12
                                                minute = int(time[1])
                                            else:
                                                o_date = [
                                                    t.replace(',', '').strip() for t in s.split(' ')
                                                ]
                                                if len(o_date) >= 3:
                                                    try:
                                                        month = next(
                                                            index for index, month in enumerate(calendar.month_abbr)
                                                            if month.lower() in o_date[0].lower() and index > 0
                                                        )
                                                    except StopIteration:
                                                        pass
                                                    day = int(o_date[1])
                                                    year = int(o_date[2])
                                        onsale_date = datetime(
                                            year=year,
                                            month=month,
                                            day=day,
                                            hour=hour,
                                            minute=minute
                                        )
                                        cur_event['onsale_date'] = onsale_date.strftime("%Y-%m-%d")
                                        cur_event['onsale_time'] = onsale_date.strftime('%I:%M %p').lstrip("0")
                                        status_final = {
                                            'id': AvailabilityStatus.NOT_ON_SALE.value,
                                            'name': AvailabilityStatus.NOT_ON_SALE.status_name
                                        }
                                    elif 'sold out' in ticket_avail:
                                        status_final = {
                                            'id': AvailabilityStatus.SOLD_OUT.value,
                                            'name': AvailabilityStatus.SOLD_OUT.status_name
                                        }
                                    elif 'online sales have ended' in ticket_avail:
                                        status_final = {
                                            'id': AvailabilityStatus.ONLINE_SALES_ENDED.value,
                                            'name': AvailabilityStatus.ONLINE_SALES_ENDED.status_name
                                        }
                                    elif 'get ticket' in ticket_avail or 'get vouchers' in ticket_avail \
                                            or 'register' in ticket_avail:
                                        status_final = {
                                            'id': AvailabilityStatus.AVAILABLE.value,
                                            'name': AvailabilityStatus.AVAILABLE.status_name
                                        }

                                        # extract prices
                                        price = buy_col.select_one('[itemprop="price"]')
                                        if price:
                                            value = price.attrs.get('content').strip()
                                            try:
                                                value = float(value)
                                            except:
                                                pass
                                            price_tmp['min'] = value
                                            price_tmp['max'] = value

                                        low_price = buy_col.select_one('[itemprop="lowPrice"]')
                                        if low_price:
                                            value = low_price.attrs.get('content').strip()
                                            try:
                                                value = float(value)
                                            except:
                                                pass
                                            price_tmp['min'] = value

                                        high_price = buy_col.select_one('[itemprop="highPrice"]')
                                        if high_price:
                                            value = high_price.attrs.get('content').strip()
                                            try:
                                                value = float(value)
                                            except:
                                                pass
                                            price_tmp['max'] = value

                                        currency = buy_col.select_one('[itemprop="priceCurrency"]')
                                        if currency:
                                            value = currency.attrs.get('content').strip()
                                            price_tmp['currency'] = value
                                    elif cur_event.get('name') and \
                                            (findall(r"cancel+ed", cur_event['name'].lower())
                                             or findall(r"postponed", cur_event['name'].lower())
                                             or findall(r"postponing", cur_event['name'].lower())):
                                        status_final = {
                                            'id': AvailabilityStatus.CANCELLED.value,
                                            'name': AvailabilityStatus.CANCELLED.status_name
                                        }
                                    elif 'not on sale' in ticket_avail \
                                            or row.select_one('.homeNotOnSale') is not None:
                                        status_final = {
                                            'id': AvailabilityStatus.NOT_ON_SALE.value,
                                            'name': AvailabilityStatus.NOT_ON_SALE.status_name
                                        }

                                cur_event['price'] = price_tmp
                                cur_event['tickets_availability'] = status_final
                                if cur_event.get('tickets_availability').get('id') == AvailabilityStatus.SOLD_OUT.value:
                                    cur_event['is_sold_out'] = 1
                                else:
                                    cur_event['is_sold_out'] = 0

                                # check if it is hosted in DE
                                if cur_event['venue'].get('address') and \
                                        (not cur_event['venue']['address'].get('country') \
                                         or not cur_event['venue']['address'].get('country').strip()) and \
                                        (cur_event.get('price') and cur_event['price'].get('currency')
                                         and 'eur' in cur_event['price'].get('currency').lower()):
                                    if cur_event['venue'].get('address'):
                                        cur_event['venue']['address']['country'] = 'Germany'

                                these_events.append(cur_event)

                            self.log.info(f"Venue # {ven_idx + 1} of {venues_count}. "
                                          f"Page # {page_idx} of results parsed. "
                                          f"Events total: {len(these_events)}.")

                            # check stats
                            try:
                                max_visibile_page = max(int(e.text) for e in obj.select('.pages a') if e.text.isdigit())
                            except:
                                max_visibile_page = 0

                            if max_visibile_page <= page_idx:
                                self.log.info(f"That was the last page.")
                                break

                            # get new page
                            np_url = NEXT_PAGE_URL_TEMPLATE.format(
                                venue_id=venue_id,
                                page_idx=page_idx + 1
                            )

                            for _ in range(REQUEST_RETRIES):
                                try:
                                    next_page_resp = s.get(np_url)
                                    ex = None
                                    break
                                except Exception as ex:
                                    continue
                            if ex is not None:
                                raise ex

                            obj = Bs(next_page_resp.content, 'lxml')

                self.log.info(f"Venue # {ven_idx + 1} of {venues_count}: finished. events_total={len(these_events)}.")
                errors_in_a_row = 0

            except Exception as ex:

                self.log.error(f"Error pulling venue={ven}", exc_info=True)
                errors_in_a_row += 1
                if errors_in_a_row < PULLING_EVENTS_ERROR_TOLERANCE:
                    sleep(SLEEP_AFTER_ERROR)

            if errors_in_a_row >= PULLING_EVENTS_ERROR_TOLERANCE:
                self.log.error(f"Too much errors in a row: {errors_in_a_row}")
                raise ex

            if ven_idx and ven_idx % EVENTS_SAVE_TO_XCOM_EVERY_X_CONFIGS == 0:

                self.log.info(f"Saving {len(these_events)} events to XCom...")
                context['task_instance'].xcom_push(key="events", value=these_events)

        self.log.info(f"Finished. Got {len(these_events)} events finally.")
        context['task_instance'].xcom_push(key="events", value=these_events)
