from airflow.models.variable import Variable
from additional.tools.config_helper import ConfigReader

try:
    config_name = Variable.get('ETIX_CONFIG_NAME_EVENTS')
except KeyError:
    config_name = 'ETIX_CONFIG_EVENTS'

with ConfigReader(config_name) as cr:

    CONFIG_NAME = config_name

    AIRFLOW_POOL_NAME = cr.read_or_default('airflow_pool', 'default_pool')
    MANUAL_CYCLE_ID = cr.read_or_default('manual_cycle_id', None)

    RETRIES = cr.read_or_default('retries', 0)
    NUM_WORKERS_STEP_1 = cr.read_or_default('num_workers_step_1', 5)
    NUM_WORKERS_STEP_2 = cr.read_or_default('num_workers_step_2', 2)
    COUNTRIES = cr.read_or_default('countries', ['US', 'CA', 'DE'])
    PAGE_LIMIT = cr.read_or_default('page_limit', 4000)
    DUP_PAGES_LIMIT = cr.read_or_default('dup_pages_limit', 2)
    IS_TEST_MODE = cr.read_or_default('is_test_mode', False)
    TEST_MODE_CONFIGS_LIMIT = cr.read_or_default('test_mode_configs_limit', 30)
    ASYNC_REQUESTS = cr.read_or_default('async_requests', True)
    PULL_VENUES = cr.read_or_default('pull_venues', False)
    PULL_EVENTS = cr.read_or_default('pull_events', True)
    VENUES_VAR_NAME = cr.read_or_default('venues_var_name', 'ETIX_VENUES_LIST')
    VENUES_SYNCHRONIZE_DATA_AMONG_THREADS = cr.read_or_default('venues_synchronize_data_among_threads', True)
    VENUES_SAVE_TO_XCOM_EVERY_X_CONFIGS = cr.read_or_default('venues_save_to_xcom_every_x_configs', 5)

    REQUEST_RETRIES = cr.read_or_default('request_retries', 2)
    PULLING_VENUES_ERROR_TOLERANCE = cr.read_or_default('pulling_venues_error_tolerance', 5)
    PULLING_EVENTS_ERROR_TOLERANCE = cr.read_or_default('pulling_events_error_tolerance', 5)
    WITHOUT_NEW_VENUES_FINISH_THRESHOLD = cr.read_or_default('without_new_venues_finish_threshold', 5)
    SLEEP_AFTER_ERROR = cr.read_or_default('sleep_after_error', 60)
    SEARCH_FOR_MONTH_AHEAD = cr.read_or_default('search_for_month_ahead', 9)
    EVENTS_PULL_MORE_INFO = cr.read_or_default('events_pull_more_info', True)
    EVENTS_SAVE_TO_XCOM_EVERY_X_CONFIGS = cr.read_or_default('events_save_to_xcom_every_x_configs', 5)
    TABLE_NAME = cr.read_or_default('step_2_tablename', 'etix_final')
    SQL_BATCH_SIZE = cr.read_or_default('sql_batch_size', 1000)
    SAVE_TO_DB = cr.read_or_default('save_to_db', True)

    EXPORT_TO_CSV = cr.read_or_default('export_to_csv', True)
    EXPORT_RECORDS_THRESHOLD = cr.read_or_default('export_records_threshold', 1000)
