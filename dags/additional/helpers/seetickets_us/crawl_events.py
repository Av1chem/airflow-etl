import json
import re
import string
from datetime import datetime as dt

import hjson
import requests
from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from bs4 import BeautifulSoup as bs

from additional.helpers.seetickets_us.seetickets_us_config import *
from additional.tools.database_mixin import DatabaseMixin


class PullEvents(BaseOperator, DatabaseMixin):
    """
    """

    template_fields = ('cycle_id',)
    template_ext = ()

    ui_color = '#f5dedc'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.cycle_id = cycle_id
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)
        self.starting_time = dt.utcnow()
        self.events_parsed = 0

    def execute(self, context):

        self.cleanup_results(TABLE)

        with requests.session() as s:
            self.log.info(f'SEARCH: Start parsing list of events in USA. Exec time {self.exec_time():.2f}')
            events = []
            for i in range(1, 999999999):

                http_ex = None
                for ret in range(REQUEST_RETRIES):
                    try:
                        resp = s.get(URL_TEMPLATE.format(str(i * 15)))
                        http_ex = None
                        break
                    except Exception as ex:
                        http_ex = ex
                if http_ex is not None:
                    raise http_ex

                obj = bs(resp.text, 'lxml')
                e_containers = obj.select('.search-event')
                if e_containers:
                    events_on_page = len(e_containers)
                    for e_idx, e in enumerate(e_containers):
                        try:
                            e_date = e.select_one('div > div.event-date').text.split('-')
                            s_date = dt.strftime(
                                dt.strptime(dt.utcnow().strftime('%Y') + ' ' + e_date[0].split(',')[-1].strip(),
                                            '%Y %b %d')
                                , '%Y-%m-%d'
                            )
                            if len(e_date) > 1:
                                e_date = dt.strftime(
                                    dt.strptime(dt.utcnow().strftime('%Y') + ' ' + e_date[-1].split(',')[-1].strip(),
                                                '%Y %b %d')
                                    , '%Y-%m-%d'
                                )
                            else:
                                e_date = s_date
                            cur_event = {
                                'url': 'https://www.seetickets.us' + e.select_one('div > div> div> a').attrs['href'],
                                'name': e.select_one('div > div> p').text.strip(),
                                # 'l_address': e.select_one('.event-info > .event-location').text,
                                'id': int(e.select_one('div > div> div> a').attrs['href'].split('/')[-1]),
                                'startDate': s_date,
                                'endDate': e_date
                            }
                            e_details = self.parse_event(cur_event)
                            if e_details is not None:
                                events.append(e_details)
                            else:
                                self.log.error(f"EVENTS: can't pull details for event={cur_event}")

                            self.log.info(f"EVENTS: page #{i}, event #{e_idx+1} of {events_on_page} parsed.")

                        except Exception as ex:
                            self.log.error(f"SEARCH: error parsing initial data from: {e}", exc_info=True)
                else:
                    # no more events
                    break

                et = self.exec_time()
                if len(events) >= SQL_BATCH_SIZE:
                    self.write_to_db(
                        step_tablename=TABLE,
                        insert_schema=('cycle_id', 'data'),
                        records=events,
                        to_tuple=lambda rec: (self.cycle_id, json.dumps(rec))
                    )
                    events = []
                self.log.info(f'SEARCH: Page #{i} parsed, events_total={self.events_parsed}. {i / et:.2f}pps. Exec time: {et:.2f}')

        if events:
            self.write_to_db(
                step_tablename=TABLE,
                insert_schema=('cycle_id', 'data'),
                records=events,
                to_tuple=lambda rec: (self.cycle_id, json.dumps(rec))
            )
        self.log.info(f'Finished parsing events, total={self.events_parsed}. Exec time {self.exec_time():.2f}')

    def parse_event(self, event):

        try:
            resp = requests.get(event['url'])
        except requests.exceptions.TooManyRedirects:
            return None

        if resp.status_code != 200:
            return None
        else:
            obj = bs(resp.text, 'lxml')
            if obj.select_one('#eventpassform'):
                self.log.info(f'EVENTS: Private event {event["url"]}')
                return event

            # extract offers & performers from API
            try:
                offers_resp = requests.get(TICKETS_URL_TEMPLATE.format(event['id']))
                event['offers'] = offers_resp.json()
            except:
                self.log.error(f"EVENTS: error pulling offers for event={event}")

            try:
                performers_resp = requests.get(PERFORMERS_URL_TEMPLATE.format(event['id']))
                event['performers'] = performers_resp.json()
            except:
                self.log.error(f"EVENTS: error pulling performers for event={event}")

            # extract data from ld+json
            script = obj.select_one('script[type="application/ld+json"]')
            if script is not None:
                try:
                    result = json.loads(script.string)
                    if '@type' in result.keys():
                        event['@type'] = result['@type']
                    if 'location' in result.keys():
                        event['location'] = result['location']
                except json.JSONDecodeError:
                    self.log.error(f'EVENTS: JSONDecodeError for {event["url"]}')
                    return event
            else:
                self.log.warning(f'EVENTS: No ld+json for {event["url"]}')

        self.events_parsed += 1
        return event

    def exec_time(self):
        return (dt.utcnow() - self.starting_time).total_seconds()


class Del:

    def __init__(self, keep=[*string.digits] + ['.']):
        self.comp = dict((ord(c),c) for c in keep)

    def __getitem__(self, k):
        return self.comp.get(k)
