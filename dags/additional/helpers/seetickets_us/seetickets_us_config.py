from airflow.models.variable import Variable
from additional.tools.config_helper import ConfigReader

try:
    config_name = Variable.get('SEETICKETS_US_CONFIG_NAME')
except KeyError:
    config_name = 'SEETICKETS_US'

URL_TEMPLATE = 'https://www.seetickets.us/wafform.aspx?_act=Search&_tab=Event&_sea=EventSearchV3&_sft=0&CurrentTab=all&_lfv={}'
TICKETS_URL_TEMPLATE = 'https://www.seetickets.us/wafform.aspx?_act=gettabgroups&_Event={}'
PERFORMERS_URL_TEMPLATE = 'https://www.seetickets.us/wafform.aspx?_act=geteventtalents&_pky={}'


with ConfigReader(config_name) as cr:

    CONFIG_NAME = config_name
    MANUAL_CYCLE_ID = cr.read_or_default('manual_cycle_id', None)
    REQUEST_RETRIES = cr.read_or_default('request_retries', 3)
    TABLE = cr.read_or_default('table', 'seetickets_us')
    SQL_BATCH_SIZE = cr.read_or_default('sql_batch_size', 100)

    EXPORT_TO_CSV = cr.read_or_default('export_to_csv', True)
    EXPORT_RECORDS_THRESHOLD = cr.read_or_default('export_records_threshold', 10)
