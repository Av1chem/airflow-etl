from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.exceptions import AirflowException
from additional.tools import sql_util


class CreateNewCyclePostgresOperator(BaseOperator):
    """
    Creates new cycle in the Postgres extended db.

    :param postgres_conn_id: reference to a specific postgres database
    :type postgres_conn_id: str
    :param source_id: source_id
    :type source_id: int
    :param pipeline_id: pipeline_id
    :type pipeline_id: int
    :param note: cycle note
    :type note: str
    :param cycle_categories: ["medicare", "medicaid", "commercial"... etc]
    :type cycle_categories: list
    :param cycle_states: list of states for cycle run ["CA", "NY"... etc]
    :type cycle_states: list
    :param cycle_plans: list of plans for cycle run
    :type cycle_plans: list
    :param addl_cycle_config_dict: dict of any additional cycle config information for cycles table (json format)
    :type addl_cycle_config_dict: dict
    :param is_run_last_cycle: Reuse last cycle and existing step tables
    :type is_run_last_cycle: bool
    :param is_test: Is test mode?
    :type is_test: bool
    :param step_tables: dict which has names and sql for step tables
    :type step_tables: dict
    """

    template_fields = ()
    template_ext = ()

    ui_color = '#92b8f0'

    @apply_defaults
    def __init__(
            self,
            step_tables,
            postgres_conn_id="crawlers_debug_postgres",
            is_test=False,
            is_run_last_cycle=False,
            manual_cycle_id=None,
            get_last_cycle_id_query=None,
            get_max_cycle_id_query=None,
            *args, **kwargs):
        super(CreateNewCyclePostgresOperator, self).__init__(*args, **kwargs)
        self.postgres_conn_id = postgres_conn_id
        self.is_test = is_test
        self.is_run_last_cycle = is_run_last_cycle
        self.manual_cycle_id = manual_cycle_id
        self.step_tables = step_tables
        self.cycles_hook = PostgresHook(postgres_conn_id=self.postgres_conn_id)
        self.get_last_cycle_id_query = get_last_cycle_id_query
        self.get_max_cycle_id_query = get_max_cycle_id_query

    def execute(self, context):

        if self.manual_cycle_id is not None:
            self.log.info(f"Using existing cycle ID. Cycle ID: {self.manual_cycle_id}")
            context['task_instance'].xcom_push(key="cycle_id", value=self.manual_cycle_id)
            self.__init_step_tables(self.manual_cycle_id, context)
            return

        last_cycle_id = None
        if self.is_run_last_cycle and not self.is_test:
            # hardcoded
            last_cycle_id = 0

            # CHANGES HERE, NOW IT PULLS ACTUAL CYCLE_ID FROM DB
            if self.get_last_cycle_id_query:
                last_cycle_id_row = sql_util.select_records(self.cycles_hook, self.log, self.get_last_cycle_id_query)
                if last_cycle_id_row:
                    last_cycle_id = last_cycle_id_row[0][0]

        if last_cycle_id is not None:
            cycle_id = last_cycle_id
            self.log.info(
                f'Reusing EXISTING cycle {last_cycle_id} '
            )
        else:
            self.log.info(f'Creating new cycle')
            # CHANGES HERE, NOW IT PULLS ACTUAL CYCLE_ID FROM DB
            cycle_id = 0
            if self.get_max_cycle_id_query:
                tables_exist = True
                for step_table in self.step_tables:
                    if not sql_util.does_table_exist(self.cycles_hook, step_table):
                        tables_exist = False
                        self.log.info(f"This is the very first cycle, cycle_id = 0.")
                        cycle_id = 0
                        break
                if tables_exist:
                    max_cycle_id_row = sql_util.select_records(self.cycles_hook, self.log, self.get_max_cycle_id_query)
                    if max_cycle_id_row and max_cycle_id_row[0][0] is not None:
                        cycle_id = max_cycle_id_row[0][0] + 1

            if self.is_test:
                self.log.info(f"New prod run. cycle_id: {cycle_id}")
            else:
                self.log.info(f"Skipped cycle creation. Running in Test Mode cycle_id: {cycle_id}")

        self.log.info(f'Using cycle_id={cycle_id}')
        context['task_instance'].xcom_push(key="cycle_id", value=cycle_id)

        self.__init_step_tables(cycle_id, context)

    def __init_step_tables(self, cycle_id, context):
        hook = self.cycles_hook # PostgresHook()

        for step_table_name, step_table_dict in self.step_tables.items():
            print(f'step_table_name={step_table_name} step_table_dict={step_table_dict}')
            step_table_schema = step_table_dict['schema']
            step_table_indices = step_table_dict['indices']
            insert_schema = step_table_dict['insert_schema']

            if not sql_util.does_table_exist(hook, step_table_name):
                self.log.info(f"Creating step table {step_table_name}")
                if 'cycle_id' not in step_table_schema:
                    raise AirflowException(
                        f'Step table sql should contain cycle_id field! step_table_name={step_table_name} '
                        f'step_table_sql={step_table_dict}')
                sql_util.run_and_commit_query(hook, self.log,
                                              f'create table if not exists {step_table_name} ({step_table_schema})')
                for index_field in step_table_indices:
                    sql_util.run_and_commit_query(hook, self.log,
                                                  f'create index {step_table_name}_{index_field}_index '
                                                  f'on {step_table_name} ({index_field})'
                                                  )

            else:
                self.log.info(f"Reusing existing table {step_table_name}")
                pass

            context['task_instance'].xcom_push(key=f"{step_table_name}_insert_schema", value=insert_schema)

            if self.is_test and not self.is_run_last_cycle:
                self.log.info(f"Given it's a test run, we are removing existing data for current cycle_id = {cycle_id}")
                sql_util.run_and_commit_query(hook, self.log,
                                              f'delete from {step_table_name} where cycle_id={cycle_id}')
