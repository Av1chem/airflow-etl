import json
from importlib import import_module

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults


class StartOperator(BaseOperator):
    """

    """

    template_fields = ()
    template_ext = ()

    ui_color = '#92b8f0'

    @apply_defaults
    def __init__(
            self,
            config_module_path,
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.config_module_path = config_module_path

    def execute(self, context):

        cfg_mod = import_module(self.config_module_path)
        config = json.dumps({v: str(getattr(cfg_mod, v)) for v in dir(cfg_mod) if v.isupper()}, indent=4)

        self.log.info("____________________ Begin config ____________________")
        self.log.info(config)
        self.log.info("____________________  End config  ____________________")
