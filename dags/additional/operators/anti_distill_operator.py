import os
import json
import shutil
import logging
import zipfile
import requests
import subprocess
from time import sleep
from datetime import datetime as dt
from datetime import timedelta
# from seleniumwire import webdriver as sw_webdriver
from selenium.common.exceptions import TimeoutException
from selenium import webdriver as webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pyvirtualdisplay import Display
from bs4 import BeautifulSoup as Bs
from airflow.models import BaseOperator
from airflow.exceptions import AirflowException
from playwright.sync_api import sync_playwright


class AntiDistillBrowser(BaseOperator):

    def __init__(
            self,
            *args, **kwargs):
        if 'browser_config' not in kwargs:
            raise AirflowException('Error: "browser_config" parameter is not provided.')
        else:
            self._browser_config = kwargs['browser_config']
            if self._browser_config.proxy is not None:
                conn_string = f"http://{self._browser_config.proxy.usr}:{self._browser_config.proxy.pwd}@" \
                              f"{self._browser_config.proxy.hst}:{self._browser_config.proxy.prt}"
                self._proxy_config = {
                    'http': conn_string,
                    'https': conn_string
                }
            else:
                self._proxy_config = None
        if 'is_test' not in kwargs:
            self._is_test = False
        else:
            self._is_test = kwargs['is_test']
        if 'prefix' not in kwargs:
            self.prefix = 'default'
        else:
            self.prefix = kwargs['prefix']
        if 'delay_between_pages' not in kwargs:
            self._delay_between_pages = 30
        else:
            self._delay_between_pages = kwargs['delay_between_pages']
        if 'clear_user_data' not in kwargs:
            self._clear_user_data = True
        else:
            self._clear_user_data = kwargs['clear_user_data']
        if 'user_data_dir' not in kwargs:
            self._user_data_dir = None
        else:
            self._user_data_dir = kwargs['user_data_dir']
        if 'pplugin' not in kwargs:
            self._pplugin = 'proxyplugin.zip'
        else:
            self._pplugin = kwargs['pplugin']
        if 'no_images' in kwargs:
            self._no_images = kwargs['no_images']
        else:
            self._no_images = False
        if 'playwright_proxy' not in kwargs:
            self._playwright_proxy = None
        else:
            self._playwright_proxy = kwargs['playwright_proxy']

        super().__init__(*args, **kwargs)

    def get_browser(self):

        self.shutdown_all()

        # playwright+firefox
        self._playwright = sync_playwright().start()
        self._browser = self._playwright.firefox.launch(headless=not self._is_test, proxy=self._playwright_proxy)
        self._context = self._browser.new_context(ignore_https_errors=True)
        self._page = self._context.new_page()
        self._page.set_default_timeout(180000)

        self._page._next_page_available = dt(1, 1, 1)
        self._page._get_orig = self._page.goto

        def _get(*args, **kwargs):
            sleep_for = (self._page._next_page_available - dt.utcnow()).total_seconds()
            if sleep_for > 0:
                if self._is_test:
                    print(f"AntiDistill requests throttling: sleep for {sleep_for:.2f} sec.")
                sleep(sleep_for)
            self._page._next_page_available = dt.utcnow() + timedelta(seconds=self._delay_between_pages)
            self._page._get_orig(*args, **kwargs)

        self._page.get = _get

        self.log.info(f"AntiDistill init: browser ready. Config: {json.dumps(self._browser_config)}")

        return

        # init Chrome

        chrome_releases = os.path.join(os.getcwd(), 'dags', 'additional', 'chrome_releases', self.prefix)
        dir_name = next(dir for dir in os.listdir(chrome_releases) if f"_{self._browser_config.major_version}" in dir)
        self._chromedriver_path = os.path.join(
            chrome_releases, dir_name, 'chromedriver'
        )

        if self._user_data_dir is None:
            self._user_data_dir = os.path.join(chrome_releases, dir_name, 'user-data')
        if os.path.isdir(self._user_data_dir) and self._clear_user_data:
            self.log.info(f"AntiDistill init: cleaning user_data_dir={self._user_data_dir}")

            bash_cmd = [
                'sudo', 'chmod', '-R', '777', self._user_data_dir
            ]
            bash_cmd_str = ' '.join(s for s in bash_cmd)
            self.log.info(f"Bash command: {bash_cmd}")
            ret = os.system(bash_cmd_str)

            shutil.rmtree(self._user_data_dir)
            os.mkdir(self._user_data_dir)

        self._browser_options = Options()

        self._browser_options.add_experimental_option("excludeSwitches", ["enable-automation"])
        self._browser_options.add_argument("--disable-blink-features=AutomationControlled")
        self._browser_options.add_argument("start-maximized")

        self._browser_options.binary_location = os.path.join(chrome_releases, dir_name, 'data', 'chrome')
        self._browser_options.add_argument(f"--user-data-dir={self._user_data_dir}")
        self._browser_options.add_argument('--no-first-run')
        self._browser_options.add_argument('--no-default-browser-check')

        self._browser_options.add_argument("--disable-gpu")
        self._browser_options.add_argument("--disable-dev-shm-usage")
        self._browser_options.add_argument("--no-sandbox")

        if self._browser_config.disable_webgl:
            self._browser_options.add_argument('--disable-webgl')

        prefs = {'intl.accept_languages': self._browser_config.lang}
        if self._no_images:
            prefs["profile.default_content_settings"] = {"images": 2}
            prefs["profile.managed_default_content_settings"] = {"images": 2}
        self._browser_options.add_experimental_option('prefs', prefs)

        if self._pplugin is not None:
            self._browser_options.add_extension(
                os.path.join(os.getcwd(), 'dags', 'additional', 'proxy_plugins', self._pplugin))
        # self._browser_options.add_argument(f'--proxy-server=http://localhost:8080')

        seleniumwire_logger = logging.getLogger('seleniumwire')
        seleniumwire_logger.setLevel(logging.CRITICAL)
        self.log.info(f"AntiDistill init: user-data-dir={self._user_data_dir}")
        self.log.info(f"AntiDistill init: chromedriver executable_path={self._chromedriver_path}")
        self.log.info(f"AntiDistill init: chrome binary_location={self._browser_options.binary_location}")

        dr = webdriver.Chrome(
            options=self._browser_options, executable_path=self._chromedriver_path
        )

        # if self._proxy_config is not None:
        #     dr = sw_webdriver.Chrome(
        #         options=self._browser_options, executable_path=self._chromedriver_path,
        #         # seleniumwire_options={
        #         #     # 'backend': 'mitmproxy',
        #         #     'proxy': self._proxy_config,
        #         #     # 'proxy': {
        #         #     #     'http': 'http://lum-customer-c_154c57cc-zone-ticketek_au-ip-185.212.60.227:qbod1kbqv9i8@zproxy.lum-superproxy.io:22225',
        #         #     #     'https': 'http://lum-customer-c_154c57cc-zone-ticketek_au-ip-185.212.60.227:qbod1kbqv9i8@zproxy.lum-superproxy.io:22225'
        #         #     # },
        #         #     'connection_timeout': 30
        #         # }
        #     )
        # else:
        #     dr = webdriver.Chrome(
        #         options=self._browser_options, executable_path=self._chromedriver_path
        #     )

        dr.delete_all_cookies()
        for url in self._browser_config.init_pages:
            dr.get(url)
        dr.get('chrome://settings')

        dr._next_page_available = dt(1, 1, 1)
        dr._get_orig = dr.get
        def _get(*args, **kwargs):
            sleep_for = (dr._next_page_available - dt.utcnow()).total_seconds()
            if sleep_for > 0:
                if self._is_test:
                    print(f"AntiDistill requests throttling: sleep for {sleep_for:.2f} sec.")
                sleep(sleep_for)
            dr._next_page_available = dt.utcnow() + timedelta(seconds=self._delay_between_pages)
            dr._get_orig(*args, **kwargs)
        dr.get = _get

        self.log.info(f"AntiDistill init: browser ready. Config: {json.dumps(self._browser_config)}")
        self.dr = dr

    def shutdown_all(self):

        # playwright
        if hasattr(self, '_browser'):
            self._browser.close()
            del self._browser

        if hasattr(self, '_playwright'):
            self._playwright.stop()
            del self._playwright
            self.log.info(f"AntiDistill shutdown: browser closed.")

        # selenium
        if hasattr(self, 'dr'):
            self.dr.close()
            del self.dr
            self.log.info(f"AntiDistill shutdown: browser closed.")

        if hasattr(self, '_display'):
            self._display.stop()
            del self._display
            self.log.info(f"AntiDistill shutdown: virtual display stopped.")

    def post_execute(self, context, result=None):
        self.shutdown_all()

    def on_kill(self):
        self.shutdown_all()

    def __del__(self):
        self.shutdown_all()

    def __exit__(self, *args, **kwargs):
        self.shutdown_all()

    def parse_page(self, url=None, as_json=False):

        if hasattr(self, '_playwright'):
            if url is not None:
                self._page.get(url, wait_until='domcontentloaded')
            page_source = self._page.content()
        else:
            if url is not None:
                self.dr.get(url)
            page_source = self.dr.page_source

        if as_json:
            obj = json.loads(Bs(page_source, 'lxml').text)
        else:
            obj = Bs(page_source, 'lxml')

            def parse_value(name, selector, attr=None):
                el = obj.select_one(selector)
                if el is None:
                    return {name: None}
                else:
                    if attr is not None:
                        val = el.attrs.get(attr)
                    else:
                        val = ' '.join(s for s in el.stripped_strings)
                    return {name: val}
            obj.parse_value = parse_value

            obj._select_old = obj.select
            def _select_wrapped(*args, **kwargs):
                selected = obj._select_old(*args, **kwargs)
                for s_el in selected:
                    def parse_value(name, selector, attr=None):
                        el = s_el.select_one(selector)
                        if el is None:
                            return {name: None}
                        else:
                            if attr is not None:
                                val = el.attrs.get(attr)
                            else:
                                val = ' '.join(s for s in el.stripped_strings)
                            return {name: val}
                    s_el.parse_value = parse_value
                return selected
            obj.select = _select_wrapped

        return obj

    def dr_find_element(self, selector, timeout=30, silent=False):
        try:
            return WebDriverWait(
                self.dr, timeout
            ).until(
                EC.element_to_be_clickable((
                    By.CSS_SELECTOR,
                    selector
                ))
            )
        except Exception as ex:
            if silent:
                return None
            else:
                raise

    @staticmethod
    def do_action(action, retries=5, *args, **kwargs):
        for i in range(retries):
            try:
                action(*args, **kwargs)
                return None
            except Exception as ex:
                if i == retries - 1:
                    raise ex
                sleep(0.5)


class AntiDistillInstall(BaseOperator):

    def __init__(
            self,
            browser_configs,
            rewrite=False,
            prefix='default',
            *args, **kwargs):
        self.rewrite=rewrite
        self.browser_configs = browser_configs
        self.prefix = prefix
        super().__init__(*args, **kwargs)

    def execute(self, context):

        # # create folders, delete old builds
        root_dir = os.path.join(os.getcwd(), 'dags', 'additional', 'chrome_releases', self.prefix)
        gcpm_path = os.path.join(os.getcwd(), 'dags', 'additional', 'tools', 'GCPM')

        if not os.path.isdir(os.path.join(os.getcwd(), 'dags', 'additional', 'chrome_releases')):
            self.log.info(f"Creating root folder: {os.path.join(os.getcwd(), 'dags', 'additional', 'chrome_releases')}")
            os.mkdir(os.path.join(os.getcwd(), 'dags', 'additional', 'chrome_releases'))


        if not os.path.isdir(root_dir):
            self.log.info(f"Creating root folder: {root_dir}")
            os.mkdir(root_dir)

        else:
            # cleanup
            if os.path.isdir(os.path.join(root_dir, 'ChromePortableGCPM')):
                self.log.info(f"Removing old: {os.path.join(root_dir, 'ChromePortableGCPM')}")
                shutil.rmtree(os.path.join(root_dir, 'ChromePortableGCPM'))

            chd_arch_name = f"chd.zip"
            chd_path = os.path.join(root_dir, chd_arch_name)
            if os.path.isfile(chd_path):
                self.log.info(f"Removing old: {chd_path}")
                os.remove(chd_path)


            if self.rewrite:
                self.log.info(f"Rewriting old configs:")
                for dir in os.walk(root_dir):
                    self.log.info(dir)
                shutil.rmtree(root_dir)
                os.mkdir(root_dir)
                shutil.copyfile(gcpm_path, os.path.join(root_dir, 'GCPM'))

        if not os.path.isfile(os.path.join(root_dir, 'GCPM')):
            new_path = os.path.join(root_dir, 'GCPM')
            self.log.info(f"Copying GCPM script to: \"{os.path.join(root_dir, 'GCPM')}\"")
            shutil.copyfile(gcpm_path, new_path)

        # # download & install Chrome
        os.walk(root_dir)
        chrome_dl_page = requests.get('https://www.slimjet.com/chrome/google-chrome-old-version.php')
        chrome_parsed = Bs(chrome_dl_page.content, 'lxml')
        for b_config in self.browser_configs.configs:

            # check if already exists
            try:
                dir = next(d[0] for d in os.walk(root_dir) if f"_{b_config.major_version}." in d[0])
                self.log.info(f"Version already exists: {dir}")
                continue
            except StopIteration:
                self.log.info(f"Downloading version: {b_config.major_version}")

            # download package
            try:
                chrome_dl_link = next(a for a in chrome_parsed.select('[href*="google-chrome-stable_current_amd64.deb"]') if
                                      f"{b_config.major_version}.0." in a.attrs.get('href'))
            except StopIteration as ex:
                self.log.error(f"No download link for release: {b_config.major_version}")
                raise ex

            self.log.info(f"Downloading from: https://www.slimjet.com/chrome/{chrome_dl_link.attrs['href']}")

            dl_stream = requests.get(f"https://www.slimjet.com/chrome/{chrome_dl_link.attrs['href']}")
            deb_rel_name = f"pac_{b_config.major_version}.deb"
            deb_fname = os.path.join(root_dir, deb_rel_name)
            with open(deb_fname, 'wb+') as f:
                f.write(dl_stream.content)
            self.log.info(f"Package downloaded: {deb_fname}")

            # make it executable
            default_folder = 'ChromePortableGCPM'
            bash_cmd = [
                'cd', root_dir,
                '&&',
                'bash', 'GCPM', deb_rel_name,
                '&&',
                'chmod', '-R', '777', os.path.join(root_dir, default_folder)
            ]
            bash_cmd_str = ' '.join(s for s in bash_cmd)
            self.log.info(f"Bash command: {bash_cmd}")
            ret = os.system(bash_cmd_str)
            self.log.info(f"Exit code: {ret}")

            # remove package file
            os.remove(deb_fname)

            # download chromedriver
            chd_ver = f"?path={b_config.major_version}.0."
            dl_page = requests.get('https://chromedriver.storage.googleapis.com/?delimiter=/&prefix=')
            parsed = Bs(dl_page.content, 'lxml')
            a = [a.text for a in parsed.select(f'CommonPrefixes Prefix') if f'{b_config.major_version}.0' in a.text]
            if a:
                a = a[-1]
                dl_page = requests.get(f'https://chromedriver.storage.googleapis.com/?delimiter=/&prefix={a}')
                parsed = Bs(dl_page.content, 'lxml')
                a = [a.text for a in parsed.select(f'Contents Key') if 'chromedriver_linux64.zip' in a.text]
                if a:
                    a = a[-1]
                    dl_stream = requests.get(
                        'https://chromedriver.storage.googleapis.com/' + a)
                    chd_arch_name = f"chd.zip"
                    chd_path = os.path.join(root_dir, chd_arch_name)
                    with open(chd_path, 'wb+') as f:
                        f.write(dl_stream.content)
                    self.log.info(f"Chromedriver downloaded: {chd_path}")
                    with zipfile.ZipFile(chd_path, 'r') as zip_ref:
                        zip_ref.extractall(os.path.join(root_dir, default_folder))
                    os.chmod(os.path.join(root_dir, default_folder, 'chromedriver'), 0o755)

                    self.log.info(f"Chromedriver installed!")

                    os.remove(chd_path)

                    # rename folder
                    final_dir = os.path.join(root_dir, f"{b_config.config_id}_{b_config.major_version}.0.something")
                    os.rename(
                        os.path.join(root_dir, default_folder),
                        final_dir
                    )
                    self.log.info(f"Browser version installed to: {final_dir}")

                else:
                    raise AirflowException(
                        f"Can't find appropriate linux chromedriver for release: {b_config.major_version}.")
            else:
                raise AirflowException(f"Can't find appropriate chromedriver for release: {b_config.major_version}.")
