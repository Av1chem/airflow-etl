from psycopg2 import connect
from contextlib import closing

from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from additional.tools import sql_util
from additional.tools.database_mixin import DatabaseMixin


class DedupDataOperator(BaseOperator, DatabaseMixin):
    """
    Select unique rows from one table and insert it into another table

    :param cycle_id: current cycle_id
    :type cycle_id: str
    :param from_table: name of table to select data from
    :type from_table: str
    :param into_table: name of table to insert deduped data into
    :type into_table: str
    :param insert_schema: columns to insert
    :type insert_schema: list
    :param select_schema: columns to select. The same as insert_schema by default
    :type select_schema: list
    :param group_by: group rows by this columns.
    :type group_by: list
    :param where: WHERE clause in select part of query
    :type where: str
    :param pk: name of primary_key (should be unique!) column in select table
    :type pk: str
    :param postgres_conn_id: reference to a specific Aurora database
    :type postgres_conn_id: str
    """

    template_fields = ('cycle_id', 'from_table', 'into_table', 'insert_schema',)
    template_ext = ()

    ui_color = '#b1deaf'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            from_table,
            into_table,
            insert_schema,
            group_by,
            where=None,
            select_schema=None,
            pk='id',
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cycle_id = cycle_id
        self.from_table = from_table
        self.into_table = into_table
        self.insert_schema = insert_schema
        self.group_by = group_by
        self.where = where
        if select_schema is not None:
            self.select_schema = select_schema
        else:
            self.select_schema = self.insert_schema
        self.pk = pk
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)
        self.postgres_credentials = PostgresHook.get_connection(postgres_conn_id)

    def execute(self, context):

        self.cleanup_results(self.into_table, self.hook)
        self.log.info("Deduplicating records...")
        # build query
        inner_query = \
        f'''SELECT
            max({self.pk}) as pk
        FROM
            {self.from_table}
        WHERE
            (cycle_id = {self.cycle_id})
        '''
        if self.where:
            inner_query += \
            f''' AND 
            ({self.where})
            '''
        inner_query += \
        f'''GROUP BY 
            {', '.join(gk for gk in self.group_by)}'''
        full_query = \
        f'''INSERT INTO {self.into_table} ({', '.join(self.insert_schema)})
        SELECT {', '.join(self.select_schema)} FROM {self.from_table} WHERE {self.pk} IN
        ({inner_query})
        '''

        # custom connection with extended timeout
        with connect(dbname=self.postgres_credentials.schema, user=self.postgres_credentials.login,
                     password=self.postgres_credentials.password,
                     host=self.postgres_credentials.host,
                     port=self.postgres_credentials.port,
                     options='-c statement_timeout=99999s') as conn, closing(conn.cursor()) as cursor:
            # run query
            self.log.info("Running query:")
            self.log.info(f"{full_query}")
            cursor.execute(full_query)
            conn.commit()
            self.log.info("Rows inserted.")

        # check stats
        raw_count = self.hook.get_first(
            f'select count(*) from {self.from_table} where cycle_id = {self.cycle_id}')[0]
        dedup_count = self.hook.get_first(
            f'select count(*) from {self.into_table} where cycle_id = {self.cycle_id}')[0]
        self.log.info(f"Rows count: before={raw_count}, after={dedup_count}.")
#
