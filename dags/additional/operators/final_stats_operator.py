from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from additional.tools.sql_util import select_records


class LogStatOperator(BaseOperator):
    """
    """

    template_fields = ('cycle_id',)
    template_ext = ()

    ui_color = '#92b8f0'

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            tables,
            depth=10,
            column_width=40,
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cycle_id = cycle_id
        self.tables = tables
        self.depth = depth
        self._selection_depth = depth + 1
        self.column_width = column_width
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)

    def execute(self, context):

        # select stats per table
        stats = []
        for i, table in enumerate(self.tables):
            query_total = \
                f"select count(*) from {table}"
            total_count = select_records(
                self.hook, self.log, query_total
            )
            if not total_count or not total_count[0][0]:
                self.log.warning(f"\n\n\n Table=\"{table}\" ({i + 1} of {len(self.tables)}): seems it's empty.\n\n")
                continue
            else:
                total_count = int(total_count[0][0])
            query = \
                f"select count(*), cycle_id, min(created_at) as sd, max(created_at) as ed " \
                f"from {table} group by cycle_id order by sd desc limit {self._selection_depth}"
            records = select_records(
                self.hook, self.log, query
            )
            if records:
                stats += [(table, total_count, *r) for r in records]
            self.log.info(f"\n\n\n Table=\"{table}\" ({i + 1} of {len(self.tables)}): found {len(records)} cycles.\n\n")

            if len(records) >= self._selection_depth:
                self.log.info(f"\n\n\n Table=\"{table}\" ({i + 1} of {len(self.tables)}): "
                              f"hit the limit - cycles count is higher than needed here.\n\n")

        # group stats into 1 table
        unique_cycle_ids = set(s[3] for s in stats)
        stats_final = [(
            c,
            min(r[4] for r in stats if r[3] == c),
            max(r[5] for r in stats if r[3] == c),
            sum(r[2] for r in stats if r[3] == c)
        ) for c in unique_cycle_ids]
        stats_final.sort(key=lambda o: o[0], reverse=True)
        selected_cycle_ids = [stats_final[i][0] for i in range(min(self._selection_depth, len(stats_final)))]
        self.log.info(f"\n\n\n Going to show these cycles: {selected_cycle_ids[:-1]}\n\n")

        # compile views
        views = {}
        for r_idx, cyc_id in enumerate(selected_cycle_ids[:-1]):
            view = {}
            for tb in self.tables:
                cur_cycle = next((r for r in stats if r[0] == tb and r[3] == cyc_id), None)
                if cur_cycle is None:
                    view[tb] = {
                        'first_row_added': 'No data.',
                        'last_row_added': 'No data.',
                        'duration': 'No data.',
                        'records_added': 'No data.',
                        'duration_alt_from_previous': 'No data.',
                        'records_added_alt_from_previous': 'No data.'
                    }
                else:
                    prev_cycle = next((r for r in stats if r[0] == tb and r[3] == selected_cycle_ids[r_idx + 1]), None)
                    view[tb] = {
                        'first_row_added': cur_cycle[4].strftime("%Y-%m-%d %H:%M:%S"),
                        'last_row_added': cur_cycle[5].strftime("%Y-%m-%d %H:%M:%S"),
                        'duration': self.strfdelta((cur_cycle[5] - cur_cycle[4]).total_seconds(),
                                                   "{sign}{days}{hours}:{minutes}:{seconds}s"),
                        'records_added': cur_cycle[2],
                    }
                    if prev_cycle is not None:
                        view[tb].update({
                            'duration_alt_from_previous':
                                self.strfdelta(
                                    ((cur_cycle[5] - cur_cycle[4]) - (prev_cycle[5] - prev_cycle[4])).total_seconds(),
                                    "{sign}{days}{hours}:{minutes}:{seconds}s"),
                            'records_added_alt_from_previous': cur_cycle[2] - prev_cycle[2]
                        })
                    else:
                        view[tb].update({
                            'duration_alt_from_previous': 'No data.',
                            'records_added_alt_from_previous': 'No data.'
                        })
            views[cyc_id] = view

        # print result
        rows, columns = None, None
        for t_idx, cyc_id in enumerate(selected_cycle_ids[:-1]):
            view = views[cyc_id]
            if rows is None:
                rows = list(view.keys())
            if columns is None:
                columns = list(view[rows[0]].keys())

            subheader = \
                "|| " + "TABLE_NAME".ljust(self.column_width) + "|| " + \
                "|| ".join(c.upper().ljust(self.column_width) for c in columns) + "||"

            padding_part = ''.join(
                c if self.in_whitelist(c) else "=" for c in subheader[len("|| ") + self.column_width:])
            cyc_definition = str(cyc_id) + (" (latest)" if t_idx == 0 else " (previous)")
            start_line = f"||==== Cycle # {cyc_definition} =="
            start_line = start_line + "=" * (len("|| ") + self.column_width - len(start_line)) + padding_part
            end_line = f"||==== End of Cycle # {cyc_definition} =="
            end_line = end_line + "=" * (len("|| ") + self.column_width - len(end_line)) + padding_part

            filled_line = ''.join(c if self.in_whitelist(c) else "=" for c in subheader)
            empty_line = ''.join(c if self.in_whitelist(c) else " " for c in subheader)

            if t_idx > 0:
                self.log.info("\n")
            self.log.info(start_line)
            self.log.info(empty_line)
            self.log.info(subheader)
            self.log.info(empty_line)
            self.log.info(filled_line)
            self.log.info(empty_line)
            for row in rows:
                self.log.info(
                    "|| " + row.ljust(self.column_width) + "|| " +
                    "|| ".join(str(view[row][k]).ljust(self.column_width) for k in columns) + "||")
            self.log.info(empty_line)
            self.log.info(end_line)
        self.log.info("\n\n")

    @staticmethod
    def in_whitelist(character):
        return character in ['\\', '/', '|']

    @staticmethod
    def strfdelta(t_sec, fmt):
        if t_sec < 0:
            d = {"sign": "-"}
        else:
            d = {"sign": ""}
        d["days"], rem = divmod(abs(t_sec), 86400)
        d["days"] = f"{int(d['days'])}d " if int(d['days']) > 0 else " "
        d["hours"], rem = divmod(rem, 3600)
        d["hours"] = f"{int(d['hours']):02d}"
        d["minutes"], d["seconds"] = divmod(rem, 60)
        d["minutes"] = f"{int(d['minutes']):02d}"
        d["seconds"] = f"{d['seconds']:.1f}"
        return fmt.format(**d)
