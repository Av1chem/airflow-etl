import os
import csv
import boto3
from datetime import datetime

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.hooks.base_hook import BaseHook
from airflow.hooks.S3_hook import S3Hook
from airflow.hooks.postgres_hook import PostgresHook

from additional.tools import sql_util


class ExportAndUpload(BaseOperator):
    """

    """

    template_fields = ('cycle_id',)
    template_ext = ()

    @apply_defaults
    def __init__(
            self,
            cycle_id,
            table_name,
            filename_template,
            records_threshold=0,
            chunk_size=10000,
            s3_conn_id='aws_export_bucket',
            s3_bucket='ticket-crawlers-export',
            postgres_conn_id='crawlers_debug_postgres',
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cycle_id = cycle_id
        self.table_name = table_name
        self.filename_template = filename_template
        self.records_threshold = records_threshold
        self.s3_conn_id = s3_conn_id
        self.s3_hook = S3Hook(self.s3_conn_id)
        self.s3_bucket = s3_bucket
        self.chunk_size = chunk_size
        self.hook = PostgresHook(postgres_conn_id=postgres_conn_id)

    def execute(self, context):

        # export to csv
        self.log.info(f"Exporting to csv...")
        now = datetime.utcnow()

        records_count = sql_util.select_records(
            self.hook, self.log,
            f"SELECT count(*) from {self.table_name} where cycle_id = {self.cycle_id}"
        )
        if records_count:
            records_count = records_count[0][0]
        chunk_count = records_count // self.chunk_size + (1 if records_count % self.chunk_size > 0 else 0)
        self.log.info(f"Got {records_count} records total for table={self.table_name} and cycle_id = {self.cycle_id}.")
        self.log.info(f"{chunk_count} chunks of size <= {self.chunk_size}")

        # extract columns
        columns = []
        for c_idx, s_idx in enumerate(range(0, records_count, self.chunk_size)):
            self.log.info(f"Extracting columns, chunk # {c_idx+1} of {chunk_count}")
            records = sql_util.select_records(
                self.hook, self.log,
                f"SELECT cycle_id, created_at, data from {self.table_name} where cycle_id = {self.cycle_id} "
                f"order by id LIMIT {self.chunk_size} OFFSET {s_idx}"
            )
            for r in records:
                event = r[2]
                cur_columns = self.get_columns(event, [])
                for cc in cur_columns:
                    if cc not in columns:
                        columns.append(cc)

        columns.sort(key=lambda k: k[1])

        # prepare file
        header_row = ['cycle_id', 'parsing_date'] + [
            c[1] for c in columns
        ]
        filename = f"{self.filename_template}_{now.strftime('%Y-%m-%d_%H_%M')}.csv"
        with open(filename, "w+") as f:
            writer = csv.writer(f)
            writer.writerow(header_row)

        # write data
        for c_idx, s_idx in enumerate(range(0, records_count, self.chunk_size)):
            self.log.info(f"Writing to file, chunk # {c_idx + 1} of {chunk_count}")

            if chunk_count > 1:
                records = sql_util.select_records(
                    self.hook, self.log,
                    f"SELECT cycle_id, created_at, data from {self.table_name} where cycle_id = {self.cycle_id} "
                    f"order by id LIMIT {self.chunk_size} OFFSET {s_idx}"
                )

            with open(filename, "a+") as f:
                writer = csv.writer(f)
                for r in records:
                    event = r[2]
                    row = [
                        self.cycle_id, now.strftime('%Y-%m-%d %H:%M:%S')
                    ]
                    for col in columns:
                        row.append(self.get_dict_attr(event, col[0]))
                    writer.writerow(row)

        self.log.info("Export finished.")

        # upload to s3
        s3_key = f"{now.strftime('%Y-%m-%d')}/{filename}"
        self.s3_hook.load_file(
            filename=filename,
            key=s3_key,
            bucket_name=self.s3_bucket,
            replace=False
        )
        s3_full_path = f"s3://{self.s3_bucket}/{s3_key}"
        self.log.info(f"Data has been uploaded to S3. S3 path: \"{s3_full_path}\".")

        if records_count < self.records_threshold:
            self.log.warning(f"Got only {records_count} records! Threshold={self.records_threshold}. "
                             f"Maybe something went wrong?")
        else:
            s3_key = f"{self.filename_template}_latest.csv"
            self.s3_hook.load_file(
                filename=filename,
                key=s3_key,
                bucket_name=self.s3_bucket,
                replace=True
            )
            s3_full_path = f"s3://{self.s3_bucket}/{s3_key}"
            self.log.info(f"Data has been uploaded to S3. S3 path: \"{s3_full_path}\".")

            # add permission
            s3_conn = BaseHook.get_connection('aws_export_bucket')
            session = boto3.Session(
                aws_access_key_id=s3_conn.login,
                aws_secret_access_key=s3_conn.password,
            )
            s3 = session.resource('s3')
            object_acl = s3.ObjectAcl(self.s3_bucket, s3_key)
            response = object_acl.put(ACL='public-read')

        # delete local file
        os.remove(filename)

    @staticmethod
    def get_dict_attr(obj, path):
        cur_o = obj
        for attr in path:
            if isinstance(cur_o, list):
                if len(cur_o) > attr:
                    cur_o = cur_o[attr]
                else:
                    cur_o = None
            elif isinstance(cur_o, dict):
                cur_o = cur_o.get(attr)
            elif hasattr(cur_o, attr):
                return cur_o.attr
            else:
                return cur_o
            if cur_o is None:
                return ''
        return cur_o

    @staticmethod
    def get_columns(obj, path):
        columns = []
        for col in sorted(obj.keys(), key=lambda k: k):
            val = obj.get(col)
            if isinstance(val, dict):
                columns += ExportAndUpload.get_columns(val, path + [col])
            elif isinstance(val, list):
                for idx in range(len(val)):
                    if isinstance(val[idx], dict):
                        columns += ExportAndUpload.get_columns(val[idx], path + [col, idx])
                    else:
                        columns.append((path + [col, idx], '_'.join(str(p) for p in (path + [col])) + f"_#{idx}"))
            else:
                columns.append((
                    path + [col],
                    '_'.join(f"#{p+1:02d}" if isinstance(p, int) else p for p in (path + [col]))
                ))

        return columns
