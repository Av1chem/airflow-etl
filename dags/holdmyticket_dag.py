from datetime import datetime, timedelta

from airflow import DAG

from additional.helpers.holdmyticket.holdmyticket_config import *
from additional.helpers.holdmyticket.crawl_events import PullEvents

from additional.operators.start_operator \
    import StartOperator
from additional.operators.create_new_cycle_postgres_operator \
    import CreateNewCyclePostgresOperator
from additional.operators.export_to_csv_operator \
    import ExportAndUpload

# Default settings applied to all tasks
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'provide_context': True,
    'execution_timeout': None
}

with DAG('crawlers.holdmyticket.main',
         start_date=datetime(2020, 6, 1),
         max_active_runs=1,
         concurrency=64,
         schedule_interval='0 3,15 * * *',
         default_args=default_args,
         catchup=False
         ) as dag:

    oStart = StartOperator(
        task_id='start',
        config_module_path='additional.helpers.holdmyticket.holdmyticket_config'
    )

    # step 1 - start new cycle, init tables
    operator_0_init_tables = CreateNewCyclePostgresOperator(
        task_id='create_cycle',
        is_test=False,
        is_run_last_cycle=False,
        manual_cycle_id=MANUAL_CYCLE_ID,
        postgres_conn_id='crawlers_debug_postgres',
        step_tables={
            TABLE: {
                'schema':
                    f'id bigserial not null constraint {TABLE}_pk primary key, '
                    f'cycle_id integer, '
                    f'data jsonb, '
                    f'created_at timestamp default now()',
                'insert_schema': [
                    'cycle_id', 'data'
                ],
                'indices': ['cycle_id']},
        },
        get_last_cycle_id_query=f'SELECT cycle_id from {TABLE} order by created_at desc limit 1',
        get_max_cycle_id_query=f'SELECT MAX(cycle_id) from {TABLE}',
    )

    operator_1_pull_events = PullEvents(
        task_id="pull_events",
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}"
    )

    oStart >> operator_0_init_tables >> operator_1_pull_events

    if EXPORT_TO_CSV:
        operator_3_export_and_upload = ExportAndUpload(
            task_id="export_to_csv",
            cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
            table_name=TABLE,
            filename_template='holdmyticket',
            records_threshold=EXPORT_RECORDS_THRESHOLD
        )
        operator_1_pull_events >> operator_3_export_and_upload
