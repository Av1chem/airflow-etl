from datetime import datetime, timedelta

from airflow import DAG

from additional.helpers.maintenance.custom_export_config import *

from additional.operators.start_operator \
    import StartOperator
from additional.operators.export_to_csv_operator \
    import ExportAndUpload

# Default settings applied to all tasks
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'provide_context': True,
    'execution_timeout': None
}

with DAG('auxiliary.custom_csv_export',
         start_date=datetime(2020, 6, 1),
         max_active_runs=1,
         concurrency=64,
         schedule_interval=None,
         default_args=default_args,
         catchup=False
         ) as dag:

    oStart = StartOperator(
        task_id='start',
        config_module_path='additional.helpers.maintenance.custom_export_config'
    )

    export_and_upload = ExportAndUpload(
        task_id="export_to_csv",
        cycle_id=f"cycle_id and ({CLAUSE})",
        table_name=TABLE,
        filename_template=FILENAME,
        records_threshold=1
    )
    oStart >> export_and_upload
