from datetime import datetime, timedelta

from airflow import DAG

from additional.helpers.seetickets_us.seetickets_us_config import *
from additional.helpers.seetickets_us.crawl_events import PullEvents

from additional.operators.start_operator \
    import StartOperator
from additional.operators.final_stats_operator \
    import LogStatOperator
from additional.operators.create_new_cycle_postgres_operator \
    import CreateNewCyclePostgresOperator
from additional.operators.export_to_csv_operator \
    import ExportAndUpload

# Default settings applied to all tasks
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'provide_context': True,
    'execution_timeout': None
}

with DAG('crawlers.seetickets.us',
         start_date=datetime(2020, 6, 1),
         max_active_runs=1,
         concurrency=64,
         schedule_interval='0 4,16 * * *',
         default_args=default_args,
         catchup=False
         ) as dag:

    oStart = StartOperator(
        task_id='start',
        config_module_path='additional.helpers.seetickets_us.seetickets_us_config'
    )

    oEnd = LogStatOperator(
        task_id='end',
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
        tables=[TABLE, ]
    )

    # step 1 - start new cycle, init tables
    operator_0_init_tables = CreateNewCyclePostgresOperator(
        task_id='create_cycle',
        is_test=False,
        is_run_last_cycle=False,
        manual_cycle_id=MANUAL_CYCLE_ID,
        postgres_conn_id='crawlers_debug_postgres',
        step_tables={
            TABLE: {
                'schema':
                    f'id bigserial not null constraint {TABLE}_pk primary key, '
                    f'cycle_id integer, '
                    f'data jsonb, '
                    f'created_at timestamp default now()',
                'insert_schema': [
                    'cycle_id', 'data'
                ],
                'indices': ['cycle_id']},
        },
        get_last_cycle_id_query=f'SELECT cycle_id from {TABLE} order by created_at desc limit 1',
        get_max_cycle_id_query=f'SELECT MAX(cycle_id) from {TABLE}',
    )

    operator_1_pull_events = PullEvents(
        task_id="pull_events",
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}"
    )

    oStart >> operator_0_init_tables >> operator_1_pull_events

    if EXPORT_TO_CSV:
        operator_3_export_and_upload = ExportAndUpload(
            task_id="export_to_csv",
            cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
            table_name=TABLE,
            filename_template='seetickets_us',
            records_threshold=EXPORT_RECORDS_THRESHOLD
        )
        operator_1_pull_events >> operator_3_export_and_upload >> oEnd
    else:
        operator_1_pull_events >> oEnd
