from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator

from additional.helpers.axs.axs_config import *
from additional.helpers.axs.get_venues import GetVenues
from additional.helpers.axs.pull_venues import PullVenues
from additional.helpers.axs.merge_data import MergeData

from additional.operators.start_operator import StartOperator
from additional.operators.create_new_cycle_postgres_operator \
    import CreateNewCyclePostgresOperator
from additional.operators.export_to_csv_operator \
    import ExportAndUpload
from additional.operators.anti_distill_operator \
    import AntiDistillInstall


# Default settings applied to all tasks
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'retries': RETRIES,
    'retry_delay': timedelta(minutes=1),
    'provide_context': True,
    'execution_timeout': None
}

# Using a DAG context manager, you don't have to specify the dag property of each task
with DAG('crawlers.axs.main',
         start_date=datetime(2020, 6, 1),
         max_active_runs=1,
         concurrency=64,
         schedule_interval='0 11,23 * * *',
         default_args=default_args,
         catchup=False  # enable if you don't want historical dag runs to run
         ) as dag:

    oStart = StartOperator(
        task_id='start',
        config_module_path='additional.helpers.axs.axs_config'
    )

    # step 1 - start new cycle, init tables
    operator_0_init_tables = CreateNewCyclePostgresOperator(
        task_id='create_cycle',
        is_test=IS_TEST_MODE,
        is_run_last_cycle=False,
        manual_cycle_id=MANUAL_CYCLE_ID,
        postgres_conn_id='crawlers_debug_postgres',
        step_tables={
            VENUES_TABLE: {
                'schema':
                    f'id bigserial not null constraint {VENUES_TABLE}_pk primary key, '
                    f'cycle_id integer, '
                    f'data jsonb, '
                    f'created_at timestamp default now()',
                'insert_schema': [
                    'cycle_id', 'data'
                ],
                'indices': ['cycle_id', ]},
            EVENTS_TABLE: {
                'schema':
                    f'id bigserial not null constraint {EVENTS_TABLE}_pk primary key, '
                    f'cycle_id integer, '
                    f'data jsonb, '
                    f'created_at timestamp default now()',
                'insert_schema': [
                    'cycle_id', 'data'
                ],
                'indices': ['cycle_id', ]}
        },
        get_last_cycle_id_query=f'SELECT cycle_id from {EVENTS_TABLE} order by created_at desc limit 1',
        get_max_cycle_id_query=f'SELECT MAX(cycle_id) from {EVENTS_TABLE}',
    )

    operator_0_1_install_distill = AntiDistillInstall(
        task_id='prepare_browser',
        browser_configs=B_CONFIGS,
        prefix=PREFIX,
        rewrite=False
    )

    operator_1_get_venues = GetVenues(
        task_id="get_venues",
        browser_config=B_CONFIGS.get_by_id(0),
        playwright_proxy=PROXY,
        is_test=IS_TEST_MODE,
        delay_between_pages=DELAY_BETWEEN_REQUESTS,
        prefix=PREFIX
    )
    oStart >> operator_0_init_tables >> operator_0_1_install_distill >> operator_1_get_venues

    operator_3_merge_data = MergeData(
        task_id="merge_data",
        cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}"
    )

    for task in range(NUM_WORKERS_STEP_1):
        operator_2_pull_venues = PullVenues(
            pool=AIRFLOW_POOL_NAME,
            task_id=f"pull_venues_{task}",
            task_idx=task,
            browser_config=B_CONFIGS.get_by_id(task),
            is_test=IS_TEST_MODE,
            delay_between_pages=DELAY_BETWEEN_REQUESTS,
            prefix=PREFIX
        )
        operator_1_get_venues >> operator_2_pull_venues >> operator_3_merge_data

    # Exporting
    export_tasks = []
    last_task = operator_3_merge_data
    if VENUES_EXPORT_TO_CSV:
        export_tasks.append(ExportAndUpload(
            task_id="export_venues",
            cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
            table_name=VENUES_TABLE,
            filename_template='axs_venues',
            records_threshold=VENUE_RECORDS_THRESHOLD
        ))
        last_task >> export_tasks[-1]
        last_task = export_tasks[-1]

    if EVENTS_EXPORT_TO_CSV:
        export_tasks.append(ExportAndUpload(
            task_id="export_events",
            cycle_id=f"{{{{ ti.xcom_pull(key='cycle_id') }}}}",
            table_name=EVENTS_TABLE,
            filename_template='axs_events',
            records_threshold=EVENT_RECORDS_THRESHOLD
        ))
        last_task >> export_tasks[-1]
        last_task = export_tasks[-1]

    oEnd = DummyOperator(
        task_id='end'
    )
    last_task >> oEnd
