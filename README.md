# Tickets crawling framework

## High-level overview

In general, the whole system consists of several basic parts, let's look through them:
* [Apache Airflow](https://airflow.apache.org/): the heart of the system, it deploys, schedules & runs all crawling code. 
  Airflow is a very _rich & huge_ workflow management framework. 
  It inсludes an enormous amount of high-quality tools for pretty much any ETL task out there. 
  No need to reinvent the wheel.
  But the obvious drawback of this "all-in-one" approach is complexity. 
  Most folks had to spend a noticeable amount of time even to made _hello-world-like_ things (deploy it locally, kick-off workflow, etc).
* [AWS Managed Apache Airflow](https://docs.aws.amazon.com/mwaa/latest/userguide/what-is-mwaa.html): deployment service.
  Although it consists of EC2 instances, managed by Celery under the hood, it still gives us a pure serverless feeling.
  Instant auto-scaling, pay-as-you-go, no fees on idle.
* [A bit of CI/CD](https://github.com/marketplace/actions/s3-sync): the code in the cloud is updated on every merge/push to the ```main``` branch via GitHub Action.
* Storage: AWS Aurora stores collected data for debugging purposes. 
  Also data is exported to *.csv file & uploaded to public S3 bucket after each run (temporary way to expose results until API will be released).
  Logs are stored in CloudWatch.

## Airflow local deployment

### 1. Installation

Let's try to deploy Airflow locally. Assuming you're on Debian-based Linux (like Ubuntu), and you have python>=3.6 installed.
* First of all, we need to install some OS-level packages (psql-related):
  
  ```sudo apt install postgresql postgresql-contrib libpq-dev -y```
* Then clone repo somewhere.
* ```cd``` to repo's root folder, install requirements via ```pip install -r requirements.txt```
* Besides requirements in the file, you need to install Airflow itself via 
  
  ```pip install apache-airflow==1.10.12  --constraint "https://raw.githubusercontent.com/apache/airflow/constraints-1.10.12/constraints-3.7.txt"``` 
  
  The particular version _is_ important, and these crawlers were built for ```1.10.12``` [Complete installation guide](https://pypi.org/project/apache-airflow/1.10.12/)
* If you were lucky enough, you would have already installed Airflow. Check it with the command:
  
  ```airflow version```
  
  It should return something like:

  ```1.10.12```

  If ```airflow``` command is not recognized, you'll probably need to add __~/.local/bin__ to __PATH__:

  ```PATH=$PATH:~/.local/bin```

  To be honest, installation could go wrong in thousands of different ways, so don't worry if it is not working right from the start - it's quite an ordinary situation. :) 
  Don't hesitate pinging me and we'll sort it out!
  
### 2. Configs

If you already made it through the installation procedure, it's time to configure our Airflow instance:

* Airflow reads all of its settings from ```airflow.cfg``` file, which is usually located in your ```home``` folder, inside ```airflow``` directory:

  ```nano ~/airflow/airflow.cfg```

* You should point Airflow to repo's __DAG__ folder by updating __dags_folder__ value in the file. 
  In my case this line looks like:
  
  ```dags_folder = /home/avl/PycharmProjects/ticket-crawlers-for-aws-mwaa-main/dags```

* Important part of Airflow is [executors](https://airflow.apache.org/docs/apache-airflow/stable/executor/index.html). 
  The point is that Airflow is intended to schedule workflows in a multithreaded, distributed way, and Airflow supports several different implementations for this. 
  The default value is __SequentialExecutor__, which is completely useless outside testing. 
  We aren't gonna change it right now, for simplicity, but keep in mind that for any _real work_ you have to adjust it (and some dependant values as well).  

* I also recommend disabling Airflow built-in DAGs. Airflow is shipped with a lot of _how-to-workflows_ included by default, but you likely don't need them:

  ```load_examples = False```

### 3. Launch
  
_Almost done!_

* Before the very first run, you need to initialize Airflow's internal database:

  ```airflow initdb```

  _You don't need to run this command anywhen again._

* Let's start Airflow webserver (UI component):

  ```airflow webserver --port 8080```

* And then start Airflow scheduler in another terminal:

  ```airflow scheduler```

* Open [http://localhost:8080](http://127.0.0.1:8080) in browser. You should see Airflow UI:

  ![Airflow UI](https://github.com/Av1chem/ticket-crawlers-for-aws-mwaa/raw/main/.airflow_ui.png?raw=true)
  
### 4. What's next?

To run our crawlers locally, you still need to configure some aspects:

* As mentioned before, crawlers write data to database & S3 storage, so you have to define [_connections_](https://airflow.apache.org/docs/apache-airflow/stable/howto/connection.html) to these sources.

* Alternatively, you can configure crawlers for "test run", without writing any data _(yes, crawlers are also configurable)_.
  It can be done by adjusting special [_variables_.](https://airflow.apache.org/docs/apache-airflow/stable/howto/variable.html)
  
### 5. Bootstrapping Airflow

Although deploying Airflow manually can be good for understanding the basics, in the production environment _you don't need to do anything on your own._
There are a lot of products ([MWAA](https://docs.aws.amazon.com/mwaa/latest/userguide/what-is-mwaa.html), [Astronomer](https://www.astronomer.io/), [Cloud Composer](https://cloud.google.com/composer/docs/concepts/overview), etc.) that take care of the deployment mess.

I've chosen [MWAA](https://docs.aws.amazon.com/mwaa/latest/userguide/what-is-mwaa.html), and right now "deployment" takes me literally one click: _push local to main_. Updates will be deployed to __PROD__ in under a minute. That's simple. 


*****
