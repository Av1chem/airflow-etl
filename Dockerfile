FROM apache/airflow:1.10.12-python3.8
USER root
RUN echo deb http://ftp.us.debian.org/debian testing main contrib non-free >> /etc/apt/sources.list
RUN apt update && apt install libc6 libdbus-glib-1-2 build-essential libpq-dev -y

# install browser & related
RUN curl -LO https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt install ./google-chrome-stable_current_amd64.deb xvfb libxss1 -y
RUN rm google-chrome-stable_current_amd64.deb

USER airflow

COPY requirements.txt .
RUN pip install requests --user
RUN pip install -r requirements.txt --user
RUN python -m playwright install